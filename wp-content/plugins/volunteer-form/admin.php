<?php
global $wpdb;

include('classes.php');

$action = $_GET['action'];

date_default_timezone_set('America/New_York');
$current_time = date('m-d-Y, h:i:s A');

switch($action) {

case "apply":
function form_insert() {
date_default_timezone_set('America/New_York');
$current_time = date('m-d-Y, h:i A');

		$MyVolunteer = new Volunteer();
		$MyVolunteer->SubmitTime = $current_time;
		$MyVolunteer->UserId = $_POST['user_id'];
		$MyVolunteer->FirstName = $_POST['first_name'];
		$MyVolunteer->MiddleName = $_POST['middle_name'];
		$MyVolunteer->LastName = $_POST['last_name'];
		$MyVolunteer->DOBMonth = $_POST['dob_month'];
		$MyVolunteer->DOBDay = $_POST['dob_day'];
		$MyVolunteer->DOBYear = $_POST['dob_year'];
		$MyVolunteer->Gender = $_POST['gender'];
		$MyVolunteer->HomeAddress = $_POST['home_address1'];
		$MyVolunteer->HomeAddress2 = $_POST['home_address2'];
		$MyVolunteer->HomeCity = $_POST['home_city'];
		$MyVolunteer->HomeState = $_POST['home_state'];
		$MyVolunteer->HomeZip = $_POST['home_zip'];
		$MyVolunteer->HomeCountry = $_POST['home_country'];
		$MyVolunteer->Phone1 = $_POST['phone1'];
		$MyVolunteer->Phone2 = $_POST['phone2'];
		$MyVolunteer->Email1 = $_POST['email1'];
		$MyVolunteer->Email2 = $_POST['email2'];
		$MyVolunteer->Branch = $_POST['branch'];
		$MyVolunteer->ValueCenter = $_POST['value_center'];
		$MyVolunteer->WorkAddress = $_POST['work_address1'];
		$MyVolunteer->WorkAddress2 = $_POST['work_address2'];
		$MyVolunteer->WorkCity = $_POST['work_city'];
		$MyVolunteer->WorkState = $_POST['work_state'];
		$MyVolunteer->WorkZip = $_POST['work_zip'];
		$MyVolunteer->WorkCountry = $_POST['work_country'];
		$MyVolunteer->AmbassadorChampion = $_POST['ambassador_champion'];
		$MyVolunteer->KnowAmbassadorChampion = $_POST['know_ambassador_champion'];
		$MyVolunteer->AmbassadorChampionNames = $_POST['ambassador_champion_names'];
		$MyVolunteer->Participated = $_POST['participated'];
		$MyVolunteer->Which = $_POST['which'];
		$MyVolunteer->Hear = $_POST['hear'];
		$MyVolunteer->PrimaryLanguage = $_POST['primary_language'];
		$MyVolunteer->PrimaryLanguageProficiency = $_POST['primary_language_proficiency'];
		$MyVolunteer->SecondaryLanguage = $_POST['secondary_language'];
		$MyVolunteer->SecondaryLanguageProficiency = $_POST['secondary_language_proficiency'];
		$MyVolunteer->OtherLanguages = $_POST['other_languages'];
		$MyVolunteer->AdvancedDegree = $_POST['advanced_degree'];
		$MyVolunteer->AdvancedDegreeYN = $_POST['advanced_degree_yn'];
		$MyVolunteer->AdvancedDegree_list = $_POST['advanced_degree_list'];
		$MyVolunteer->Certification = $_POST['certification'];
		$MyVolunteer->Experience = implode(', ', $_POST['experience']);
		$MyVolunteer->OtherList = $_POST['other_list'];
		$MyVolunteer->CarryLoads = $_POST['carry_loads'];
		$MyVolunteer->Feet = $_POST['feet'];
		$MyVolunteer->SmartPhone = $_POST['smart_phone'];
		$MyVolunteer->Travel = $_POST['travel'];
		$MyVolunteer->SolveWater = $_POST['solve_water'];
		$MyVolunteer->WhyTrip = $_POST['why_trip'];
		$MyVolunteer->Learn = $_POST['learn'];
		$MyVolunteer->Liability = $_POST['liability'];
		$MyVolunteer->Responsibilities = $_POST['responsibilities'];
		$MyVolunteer->Policies = $_POST['policies'];
		$MyVolunteer->BlogPermission = $_POST['blog_permission'];
		$MyVolunteer->PersonalInfo = $_POST['personal_info'];

		$MyVolunteer->Save();
	}

form_insert();

/* Start Email Confirmation */
$toaddress = $_POST['email1'];
$name = $_POST['first_name'] ." ".$_POST['last_name'];
$subject = "Thank you for applying to volunteer with Xylem Watermark";
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= "From: Michael Fields <michaelc.fields@xyleminc.com>" . "\r\n";

if (!$toaddress) {
echo '<div id="text"><h2>You have not entered all the necessary information. Please go back.</h2>';
}

else {

$content = '
<html>
<head>
  <title>Thank you for applying to Volunteer with Xylem Watermark</title>
</head>
<body>
  <p><strong>Thank You for Applying!</strong></p>
  <p>You have successfully submitted your application for a Xylem Employee Volunteer Trip.</p>

<p>If your application is accepted, you will be contacted with further details regarding your trip, as well as instructions for travel preparation.</p>

<p>Thank you for your commitment to solving water!</p>

<p>If you have any questions about your submission or the application process, please contact Michael Fields, Director, Xylem Watermark at <a href="mailto:michaelc.fields@xyleminc.com">michaelc.fields@xyleminc.com</a>.</p>

</body>
</html>
';

mail($toaddress, $subject, $content, $headers);
header('Location: http://staging.xylemwatermark.com/thank-you/');
}
/* End Email Confirmation */
	
break;

case "save":
function form_insert() {
		$MyVolunteer = new Volunteer();
		$MyVolunteer->UserId = $_POST['user_id'];
		$MyVolunteer->FirstName = $_POST['first_name'];
		$MyVolunteer->MiddleName = $_POST['middle_name'];
		$MyVolunteer->LastName = $_POST['last_name'];
		$MyVolunteer->DOBMonth = $_POST['dob_month'];
		$MyVolunteer->DOBDay = $_POST['dob_day'];
		$MyVolunteer->DOBYear = $_POST['dob_year'];
		$MyVolunteer->Gender = $_POST['gender'];
		$MyVolunteer->HomeAddress = $_POST['home_address1'];
		$MyVolunteer->HomeAddress2 = $_POST['home_address2'];
		$MyVolunteer->HomeCity = $_POST['home_city'];
		$MyVolunteer->HomeState = $_POST['home_state'];
		$MyVolunteer->HomeZip = $_POST['home_zip'];
		$MyVolunteer->HomeCountry = $_POST['home_country'];
		$MyVolunteer->Phone1 = $_POST['phone1'];
		$MyVolunteer->Phone2 = $_POST['phone2'];
		$MyVolunteer->Email1 = $_POST['email1'];
		$MyVolunteer->Email2 = $_POST['email2'];
		$MyVolunteer->Branch = $_POST['branch'];
		$MyVolunteer->ValueCenter = $_POST['value_center'];
		$MyVolunteer->WorkAddress = $_POST['work_address1'];
		$MyVolunteer->WorkAddress2 = $_POST['work_address2'];
		$MyVolunteer->WorkCity = $_POST['work_city'];
		$MyVolunteer->WorkState = $_POST['work_state'];
		$MyVolunteer->WorkZip = $_POST['work_zip'];
		$MyVolunteer->WorkCountry = $_POST['work_country'];
		$MyVolunteer->AmbassadorChampion = $_POST['ambassador_champion'];
		$MyVolunteer->KnowAmbassadorChampion = $_POST['know_ambassador_champion'];
		$MyVolunteer->AmbassadorChampionNames = $_POST['ambassador_champion_names'];
		$MyVolunteer->Participated = $_POST['participated'];
		$MyVolunteer->Which = $_POST['which'];
		$MyVolunteer->Hear = $_POST['hear'];
		$MyVolunteer->PrimaryLanguage = $_POST['primary_language'];
		$MyVolunteer->PrimaryLanguageProficiency = $_POST['primary_language_proficiency'];
		$MyVolunteer->SecondaryLanguage = $_POST['secondary_language'];
		$MyVolunteer->SecondaryLanguageProficiency = $_POST['secondary_language_proficiency'];
		$MyVolunteer->OtherLanguages = $_POST['other_languages'];
		$MyVolunteer->AdvancedDegree = $_POST['advanced_degree'];
		$MyVolunteer->AdvancedDegreeYN = $_POST['advanced_degree_yn'];
		$MyVolunteer->AdvancedDegree_list = $_POST['advanced_degree_list'];
		$MyVolunteer->Certification = $_POST['certification'];
		$MyVolunteer->Experience = implode(', ', $_POST['experience']);
		$MyVolunteer->OtherList = $_POST['other_list'];
		$MyVolunteer->CarryLoads = $_POST['carry_loads'];
		$MyVolunteer->Feet = $_POST['feet'];
		$MyVolunteer->SmartPhone = $_POST['smart_phone'];
		$MyVolunteer->Travel = $_POST['travel'];
		$MyVolunteer->SolveWater = $_POST['solve_water'];
		$MyVolunteer->WhyTrip = $_POST['why_trip'];
		$MyVolunteer->Learn = $_POST['learn'];
		$MyVolunteer->Liability = $_POST['liability'];
		$MyVolunteer->Responsibilities = $_POST['responsibilities'];
		$MyVolunteer->Policies = $_POST['policies'];
		$MyVolunteer->BlogPermission = $_POST['blog_permission'];
		$MyVolunteer->PersonalInfo = $_POST['personal_info'];

		$MyVolunteer->Save();
	}

form_insert();
break;
}

?>