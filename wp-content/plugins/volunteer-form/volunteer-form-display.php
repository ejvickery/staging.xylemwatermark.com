<?php
global $wpdb;
global $current_user;
get_currentuserinfo();
include('classes.php');

$MyVolunteers = new Volunteers();
$MyVolunteers = $MyVolunteers->GetByUser($current_user->ID);
	
	foreach($MyVolunteers as $MyVolunteer) {
	}
?>
<div id="volunteer_app">
<div id="autosave">
<p><strong>Auto-save complete</strong></p>
</div>
<?php //echo bloginfo('wpurl'); ?>
<?php //var_dump($wpdb); ?>

<p><em>* Denotes required field.</em></p>
<br/><br/>
<!--<p><a href="#" id="expand">Expand All</a> | <a href="#" id="collapse">Collapse All</a></p>-->
<form action="<?php echo plugins_url( 'admin.php?action=apply' , __FILE__ ) ?>" method="POST" name="volunteer_form" id="volunteer_form">
<h3>PART I: CONTACT AND DEMOGRAPHIC INFORMATION</h3>
<fieldset>
<legend id="personal_information_toggle" class="expanded">Personal Information</legend>
<ol id="personal_information" class="show">
	<li><label for="first_name">First Name: *</label><input type="text" name="first_name" id="first_name" value="<?= $MyVolunteer->FirstName ?>"></li>
	<li><label for="middle_name">Middle Name:</label><input type="text" name="middle_name" id="middle_name" value="<?= $MyVolunteer->MiddleName ?>"></li>
	<li><label for="last_name">Last Name: *</label><input type="text" name="last_name" id="last_name" value="<?= $MyVolunteer->LastName ?>"></li>
	<li><label for="dob_month">Date of Birth: *</label><label for="dob_month" class="dob2">Month:</label><input type="text" name="dob_month" id="dob_month" class="dob2" value="<?= $MyVolunteer->DOBMonth ?>"> <label for="dob_day" class="dob2">Day:</label><input type="text" name="dob_day" id="dob_day" class="dob2" value="<?= $MyVolunteer->DOBDay ?>"> <label for="dob_year" class="dob4">Year:</label><input type="text" name="dob_year" id="dob_year" class="dob4" value="<?= $MyVolunteer->DOBYear ?>"></li>
	<li><label for="gender">Gender: *</label><select id="gender" name="gender">
	<option value="">---</option>
	<option value="Male" id="male" <?php if($MyVolunteer->Gender == "Male") { echo 'selected="selected"'; } ?>>Male</option>
	<option value="Female" id="female" <?php if($MyVolunteer->Gender == "Female") { echo 'selected="selected"'; } ?>>Female</option>
	</select></li>
	<li><label for="home_address1">Home Address: *</label><input type="text" name="home_address1" id="home_address1" value="<?= $MyVolunteer->HomeAddress ?>"></li>
	<li><label for="home_address2">Home Address 2:</label><input type="text" name="home_address2" id="home_address2" value="<?= $MyVolunteer->HomeAddress2 ?>"></li>
	<li><label for="home_city">City: *</label><input type="text" name="home_city" id="home_city" value="<?= $MyVolunteer->HomeCity ?>"></li>
	<li><label for="home_state">State/Region: *</label><input type="text" name="home_state" id="home_state" value="<?= $MyVolunteer->HomeState ?>"></li>
	<li><label for="home_zip">Postal Code: *</label><input type="text" name="home_zip" id="home_zip" value="<?= $MyVolunteer->HomeZip ?>"></li>
	<li><label for="home_country">Country: *</label><input type="text" name="home_country" id="home_country" value="<?= $MyVolunteer->HomeCountry ?>"></li>
	<li><label for="phone1">Phone: *</label><input type="text" name="phone1" id="phone1" value="<?= $MyVolunteer->Phone1 ?>"></li>
	<!-- <li><label for="phone2">Phone 2:</label><input type="text" name="phone2" id="phone2" value="<?= $MyVolunteer->Phone2 ?>"></li> -->
	<li><label for="email1">Email: *</label><input type="text" name="email1" id="email1" value="<?= $MyVolunteer->Email1 ?>"></li>

	<li><a href="#professional_information_toggle" class="personal_information">Next Section &gt;</a></li>
</ol>
</fieldset>
<fieldset>
<legend id="professional_information_toggle" class="collapsed">Professional Information</legend>
<ol id="professional_information" class="hide">
	<li><label for="branch">Xylem Location: *</label><input type="text" name="branch" id="branch" value="<?= $MyVolunteer->Branch ?>"></li>
	<li><label for="value_center">Growth Center: *</label><select id="value_center" name="value_center">
	<option value="">---</option>	
	<option value="Global Dewatering" id="global_dewatering" <?php if($MyVolunteer->ValueCenter == "Global Dewatering") { echo 'selected="selected"'; } ?>>Global Dewatering</option>
	<option value="Global Transport" id="global_transport" <?php if($MyVolunteer->ValueCenter == "Global Transport") { echo 'selected="selected"'; } ?>>Global Transport</option>
	<option value="Global Applied Water Systems" id="global_applied_water_systems" <?php if($MyVolunteer->ValueCenter == "Global Applied Water Systems") { echo 'selected="selected"'; } ?>>Global Applied Water Systems</option>
	<option value="Global Analytics and Treatment" id="global_analytics_and_treatment" <?php if($MyVolunteer->ValueCenter == "Global Analytics and Treatment") { echo 'selected="selected"'; } ?>>Global Analytics and Treatment</option>
	<option value="Americas" id="americas" <?php if($MyVolunteer->ValueCenter == "Americas") { echo 'selected="selected"'; } ?>>Americas</option>
	<option value="EMEA" id="emea" <?php if($MyVolunteer->ValueCenter == "EMEA") { echo 'selected="selected"'; } ?>>EMEA</option>
	<option value="China" id="china" <?php if($MyVolunteer->ValueCenter == "China") { echo 'selected="selected"'; } ?>>China</option>
	<option value="Human Resources" id="human_resources" <?php if($MyVolunteer->ValueCenter == "Human Resources") { echo 'selected="selected"'; } ?>>Human Resources</option>
	<option value="Finance" id="finance" <?php if($MyVolunteer->ValueCenter == "Finance") { echo 'selected="selected"'; } ?>>Finance</option>
	<option value="Legal" id="legal" <?php if($MyVolunteer->ValueCenter == "Legal") { echo 'selected="selected"'; } ?>>Legal</option>
	<option value="Communications" id="communications" <?php if($MyVolunteer->ValueCenter == "Communications") { echo 'selected="selected"'; } ?>>Communications</option>
	<option value="IT" id="it" <?php if($MyVolunteer->ValueCenter == "IT") { echo 'selected="selected"'; } ?>>IT</option>
	
	
	</select></li>
	<li><label for="work_address1">Address: *</label><input type="text" name="work_address1" id="work_address1" value="<?= $MyVolunteer->WorkAddress ?>"></li>
	<li><label for="work_address2">Address 2:</label><input type="text" name="work_address2" id="work_address2" value="<?= $MyVolunteer->WorkAddress2 ?>"></li>
	<li><label for="work_city">City: *</label><input type="text" name="work_city" id="work_city" value="<?= $MyVolunteer->WorkCity ?>"></li>
	<li><label for="work_state">State/Region: *</label><input type="text" name="work_state" id="work_state" value="<?= $MyVolunteer->WorkState ?>"></li>
	<li><label for="work_zip">Postal Code: *</label><input type="text" name="work_zip" id="work_zip" value="<?= $MyVolunteer->WorkZip ?>"></li>
	<li><label for="work_country">Country: *</label><input type="text" name="work_country" id="work_country" value="<?= $MyVolunteer->WorkCountry ?>"></li>
	<li><a href="#short_answer_toggle" class="professional_information">Next Section &gt;</a></li>
</ol>
</fieldset>
<h3>PART II: CONNECTION TO XYLEM WATERMARK</h3>
<fieldset>
<legend id="short_answer_toggle" class="collapsed">Short Answer Questions</legend>
<ol id="short_answer" class="hide">
<li>
<label for="ambassador_champion" class="textarea">Are you a Xylem Watermark Ambassador or Champion? *</label><br />
<input type="radio" name="ambassador_champion" value="ambassador_champion_yes" id="ambassador_champion_yes" class="radio" checked<?php if($MyVolunteer->AmbassadorChampion == "ambassador_champion_yes") { echo 'checked="checked"'; } ?>> <label for="ambassador_champion_yes" class="radio">Yes</label> <input type="radio" name="ambassador_champion" value="ambassador_champion_no" id="ambassador_champion_no" class="radio" <?php if($MyVolunteer->AmbassadorChampion == "ambassador_champion_no") { echo 'checked="checked"'; } ?> ><label for="ambassador_champion_no" class="radio">No</label>
</li>
<!--<li id="know_ambassador_champion_wrap">
<label for="know_ambassador_champion" class="textarea">Do you know the Watermark Ambassador or Champion at your site?</label><br />
<input type="radio" name="know_ambassador_champion" value="know_ambassador_champion_yes" id="know_ambassador_champion_yes" class="radio" <?php if($MyVolunteer->KnowAmbassadorChampion == "know_ambassador_champion_yes") { echo 'checked="checked"'; } ?>> <label for="know_ambassador_champion_yes" class="radio">Yes</label> <input type="radio" name="know_ambassador_champion" value="know_ambassador_champion_no" id="know_ambassador_champion_no" class="radio" <?php if($MyVolunteer->KnowAmbassadorChampion == "know_ambassador_champion_no") { echo 'checked="checked"'; } ?>> <label for="know_ambassador_champion_no" class="radio">No</label>
</li> -->
<li id="ambassador_champion_names_wrap">
<label for="ambassador_champion_names" class="textarea">Please provide the name(s) of the Ambassador or Champion at your site:</label>
<textarea id="ambassador_champion_names" name="ambassador_champion_names">
<?= $MyVolunteer->AmbassadorChampionNames ?>
</textarea>
</li>
<li>
<label for="participated" class="textarea">Have you previously participated in any Xylem Watermark programs or activities?</label><br />
<input type="radio" name="participated" value="participated_yes" id="participated_yes" class="radio" <?php if($MyVolunteer->Participated == "participated_yes") { echo 'checked="checked"'; } ?>> <label for="participated_yes" class="radio">Yes</label> <input type="radio" name="participated" value="participated_no" id="participated_no" class="radio" <?php if($MyVolunteer->Participated == "participated_no") { echo 'checked="checked"'; } ?>> <label for="participated_no" class="radio">No</label>
</li>
<li id="which_wrap">
<label for="which" class="textarea">If so, please list:</label>
<textarea id="which" name="which">
<?= $MyVolunteer->Which ?>
</textarea>
</li>
<!-- <li>
<label for="hear" class="textarea">How did you hear about this opportunity? *</label>
<textarea id="hear" name="hear">
<?= $MyVolunteer->Hear ?>
</textarea>
</li> -->
	<li><a href="#skills_certifications_toggle" class="short_answer">Next Section &gt;</a></li>
</ol>
</fieldset>
<h3>PART III: EXPERIENCE AND SKILLS</h3>
<fieldset>
<legend id="skills_certifications_toggle" class="collapsed">Languages</legend>
<ol id="skills_certifications" class="hide">
	<li><label for="primary_language">Primary Language: *</label><input type="text" name="primary_language" id="primary_language" value="<?= $MyVolunteer->PrimaryLanguage ?>"></li>
	<!-- <li>
	<label for="primary_language_proficiency">Primary Language Proficiency: *</label>
	<select id="primary_language_proficiency" name="primary_language_proficiency">
	<option value="">---</option>
	<option value="Very Poorly" <?php if($MyVolunteer->PrimaryLanguageProficiency == "Very Poorly") { echo 'selected="selected"'; } ?>>Very Poorly</option>
	<option value="Poorly" <?php if($MyVolunteer->PrimaryLanguageProficiency == "Poorly") { echo 'selected="selected"'; } ?>>Poorly</option>
	<option value="Moderate" <?php if($MyVolunteer->PrimaryLanguageProficiency == "Moderate") { echo 'selected="selected"'; } ?>>Moderate</option>
	<option value="Proficient" <?php if($MyVolunteer->PrimaryLanguageProficiency == "Proficient") { echo 'selected="selected"'; } ?>>Proficient</option>
	<option value="Very Proficient" <?php if($MyVolunteer->PrimaryLanguageProficiency == "Very Proficient") { echo 'selected="selected"'; } ?>>Very Proficient</option>
	</select>
	</li> -->
	<li><label for="secondary_language">Secondary Language:</label><input type="text" name="secondary_language" id="secondary_language" value="<?= $MyVolunteer->SecondaryLanguage ?>"></li>
	<li>
	<label for="secondary_language_proficiency">Secondary Language Proficiency:</label>
	<select id="secondary_language_proficiency" name="secondary_language_proficiency">
	<option value="">---</option>
	<option value="Very Poorly" <?php if($MyVolunteer->SecondaryLanguageProficiency == "Very Poorly") { echo 'selected="selected"'; } ?>>Very Poorly</option>
	<option value="Poorly" <?php if($MyVolunteer->SecondaryLanguageProficiency == "Poorly") { echo 'selected="selected"'; } ?>>Poorly</option>
	<option value="Moderate" <?php if($MyVolunteer->SecondaryLanguageProficiency == "Moderate") { echo 'selected="selected"'; } ?>>Moderate</option>
	<option value="Proficient" <?php if($MyVolunteer->SecondaryLanguageProficiency == "Proficient") { echo 'selected="selected"'; } ?>>Proficient</option>
	<option value="Very Proficient" <?php if($MyVolunteer->SecondaryLanguageProficiency == "Very Proficient") { echo 'selected="selected"'; } ?>>Very Proficient</option>
	</select>
	</li>
	<!-- <li><label for="other_languages">Other Languages:</label><input type="text" name="other_languages" id="other_languages" value="<?= $MyVolunteer->OtherLanguages ?>"></li>
	<li><label for="advanced_degree">Advanced Degree:</label><input type="text" name="advanced_degree" id="advanced_degree" value="<?= $MyVolunteer->AdvancedDegree ?>"></li>
	<li><label for="certification">Certification:</label><input type="text" name="certification" id="certification" value="<?= $MyVolunteer->Certification ?>"></li>-->
	<li><a href="#professional_experience_toggle" class="skills_certifications">Next Section &gt;</a></li>
</ol>

</fieldset>
<fieldset>
<legend id="professional_experience_toggle" class="collapsed">Professional Experience</legend>
<ol id="professional_experience" class="hide">
<li>
<label for="degreeCertification" class="textarea">Advanced Degree or Certification:</label><br />
<input type="radio" name="advanced_degree_yn" value="advanceddegree_yes" id="advanceddegree_yes" class="radio" <?php if($MyVolunteer->AdvancedDegreeYN == "advanceddegree_yes") { echo 'checked="checked"'; } ?>> <label for="advanceddegree_yes" class="radio">Yes</label>
<input type="radio" name="advanced_degree_yn" value="advanceddegree_no" id="advanceddegree_no" class="radio" <?php if($MyVolunteer->AdvancedDegreeYN == "advanceddegree_no") { echo 'checked="checked"'; } ?>> <label for="participated_no" class="radio">No</label>
</li>
<li id="advanceddegree_wrap">
<label for="advanced_degree_list" class="textarea">If so, please list:</label>
<textarea id="advanced_degree_list" name="advanced_degree_list">
<?= $MyVolunteer->AdvancedDegree_list ?>
</textarea>
</li>


	<li><label class="textarea">Do you have experience in:</label></li>
<?php
$aExperiences = array("Industrial Engineering", "Water Technology", "Computer Engineering", "Other");
$experiences = explode(', ', $MyVolunteer->Experience); 
foreach($aExperiences as $experience) {

if($experience == "Other") { $otherToggle = " id=\"other\""; }

	if(in_array($experience,$experiences)) {
		echo '<li><input name="experience[]" type="checkbox" value="'.$experience.'" class="ckbx" checked="checked"'.$otherToggle.'><label class="ckbx">'.$experience.'</label></li>';
	}
	else {
		echo '<li><input name="experience[]" type="checkbox" value="'.$experience.'" class="ckbx"'.$otherToggle.'><label class="ckbx">'.$experience.'</label></li>';
	}
}
?>
<li id="other_list"><label for="other_list" class="textarea">If other, please list:</label>
	<textarea name="other_list">
	<?= $OtherList ?>
	</textarea>
	</li>
	<li><a href="#individual_capabilities_toggle" class="professional_experience">Next Section &gt;</a></li>
</ol>
</fieldset>
<fieldset>
<legend id="individual_capabilities_toggle" class="collapsed">Travel Experience</legend>
<ol id="individual_capabilities" class="hide">
	<li><label class="textarea">Are you able to carry loads of 15-25lbs? *</label><br /><input type="radio" name="carry_loads" value="carry_loads_yes" id="carry_loads_yes" class="radio" <?php if($MyVolunteer->CarryLoads == "carry_loads_yes") { echo 'checked="checked"'; } ?>> <label for="carry_loads_yes" class="radio">Yes</label> <input type="radio" name="carry_loads" value="carry_loads_no" id="carry_loads_no" class="radio" <?php if($MyVolunteer->CarryLoads == "carry_loads_no") { echo 'checked="checked"'; } ?>> <label for="carry_loads_no" class="radio">No</label></li>
	<li><label class="textarea">Are you able to be on your feet and active for 1-2 hours at a time? *</label> <br /><input type="radio" name="feet" value="feet_yes" id="feet_yes" class="radio" <?php if($MyVolunteer->Feet == "feet_yes") { echo 'checked="checked"'; } ?>> <label for="feet_yes" class="radio">Yes</label> <input type="radio" name="feet" value="feet_no" id="feet_no" class="radio" <?php if($MyVolunteer->Feet == "feet_no") { echo 'checked="checked"'; } ?>> <label for="feet_no" class="radio">No</label></li>
	<li><label class="textarea">Are you comfortable using smart phone technology? *</label> <br /><input type="radio" name="smart_phone" value="smart_phone_yes" id="smart_phone_yes" class="radio" <?php if($MyVolunteer->SmartPhone == "smart_phone_yes") { echo 'checked="checked"'; } ?>> <label for="smart_phone_yes" class="radio">Yes</label> <input type="radio" name="smart_phone" value="smart_phone_no" id="smart_phone_no" class="radio" <?php if($MyVolunteer->SmartPhone == "smart_phone_no") { echo 'checked="checked"'; } ?>> <label for="smart_phone_no" class="radio">No</label></li>
	<li><label class="textarea">Have you ever traveled outside your current country of residence? *</label> <br /><input type="radio" name="travel" value="travel_yes" id="travel_yes" class="radio" <?php if($MyVolunteer->Travel == "travel_yes") { echo 'checked="checked"'; } ?>> <label for="travel_yes" class="radio">Yes</label> <input type="radio" name="travel" value="travel_no" id="travel_no" class="radio" <?php if($MyVolunteer->Travel == "travel_no") { echo 'checked="checked"'; } ?>> <label for="travel_no" class="radio">No</label></li>
	<li><p><em>Note: The items above are for informational uses only and will not be used as selection criteria for trip participants.</em></p></li>
	<li><a href="#essay_questions_toggle" class="individual_capabilities">Next Section &gt;</a></li>
</ol>
</fieldset>
<h3>PART IV: ESSAY SECTION</h3>
<fieldset>
<legend id="essay_questions_toggle" class="collapsed">Essay Questions</legend>
<ol id="essay_questions" class="hide">
	<!-- <li><label for="solve_water" class="textarea">How do you solve water? (up to 300 words) *</label>
	<textarea id="solve_water" name="solve_water"><?= $MyVolunteer->SolveWater ?></textarea>
	<script type="text/javascript">
	$("#solve_water").textareaCounter({ limit: 300 });
	</script>
	</li> -->
	<li><label for="why_trip" class="textarea">Why do you want to participate in a Xylem Watermark trip? (up to 150 words) *</label>
	<textarea id="why_trip" name="why_trip"><?= $MyVolunteer->WhyTrip ?></textarea>
	<script type="text/javascript">
	$("#why_trip").textareaCounter({ limit: 150 });
	</script>
	</li>
	<li><label for="learn" class="textarea">What do you hope to learn on the trip and how will you apply it to your position at Xylem? (up to 150 words) *</label>
	<textarea id="learn" name="learn"><?= $MyVolunteer->Learn ?></textarea>
	<script type="text/javascript">
	$("#learn").textareaCounter({ limit: 150 });
	</script>
	</li>
	<li><a href="#liability" class="essay_questions">Next Section &gt;</a></li>
</ol>
</fieldset>
<h3>PART V: TERMS AND CONDITIONS</h3>
<fieldset>
<legend id="liability_toggle" class="collapsed">Liability Agreements</legend>
<ol id="liability" class="hide">
	<li><input type="checkbox" name="liability" value="1" id="liability" class="ckbx" <?php if($MyVolunteer->Liability == "1") { echo 'checked="checked"'; } ?>><label for="liability" class="long_ckbx">I agree to comply with Xylem Inc. company policies and code of conduct while participating in a World Water Corps volunteer trip. *</label></li>
	<li><input type="checkbox" name="responsibilities" value="1" id="responsibilities" class="ckbx" <?php if($MyVolunteer->Responsibilities == "1") { echo 'checked="checked"'; } ?>><label for="responsibilities" class="long_ckbx">I agree to comply with Xylem Inc. volunteer policy while participating in a World Water Corps volunteer trip. *</label></li>
	<li><input type="checkbox" name="policies" value="1" id="policies" class="ckbx" <?php if($MyVolunteer->Policies == "1") { echo 'checked="checked"'; } ?>><label for="policies" class="long_ckbx">I grant permission for Xylem Inc. and partner agencies to use any photos, film, or videos of me, or my likeness, and any quotes or statements given by me, orally or in writing, without restrictions, in any media, in legitimate promotions and accounts for social media and other established communications means of Xylem Inc. and/or Xylem Watermark. *</label></li>
	<li><input type="checkbox" name="blog_permission" value="1" id="blog_permission" class="ckbx" <?php if($MyVolunteer->BlogPermission == "1") { echo 'checked="checked"'; } ?>><label for="blog_permission" class="long_ckbx">I hereby grant permission to use any blog entries created by me relating to any Volunteer trip in which I participate, in any way otherwise approved in this document for other of my personal information. *</label></li>
	<li><input type="checkbox" name="personal_info" value="1" id="personal_info" class="ckbx" <?php if($MyVolunteer->PersonalInfo == "1") { echo 'checked="checked"'; } ?>><label for="personal_info" class="long_ckbx">By submitting this application, in addition to other consents contained in this document, I consent to the provision of my personal information for purposes of determining eligibility for a Volunteer trip, participation in such trip and subsequent promotion of such trip. *</label></li>
</ol>

</fieldset>
<div class="savesubmit">
<input type="hidden" name="user_id" value="<?= $current_user->ID ?>" id="user_id" />
<input type="button" name="save" value="Save" id="save" class="button">
<input type="submit" name="submit" value="Submit" id="submit" class="button">
</div>

</form>
<div class="clear"></div>
</div>