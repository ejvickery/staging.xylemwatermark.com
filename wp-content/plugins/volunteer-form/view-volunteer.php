<?php
$volunteer = $_GET['volunteer'];

include('classes.php');

$MyVolunteers = new Volunteers();
$MyVolunteers = $MyVolunteers->GetById($volunteer);
	
	foreach($MyVolunteers as $MyVolunteer) {
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Volunteer Application for <?= $MyVolunteer->FirstName ?> <?= $MyVolunteer->LastName ?></title>
	<meta name="generator" content="BBEdit 10.1" />
	<?php
	if(@file_exists(TEMPLATEPATH.'/volunteer-app.css')) {
		echo '<link rel="stylesheet" href="'.get_stylesheet_directory_uri().'/volunteer-app.css" type="text/css" media="screen" />'."\n";	
	} else {
		echo '<link rel="stylesheet" href="'.WP_PLUGIN_URL.'/volunteer-form/volunteer-app.css" type="text/css" media="screen" />'."\n";
	}
	?>
</head>
<body>
<div id="volunteer_view">
<p style="text-transform: uppercase; font-size: 14px; color: #666;"><strong>Submission Time:</strong> <?= $MyVolunteer->SubmitTime ?></p>
<p style="text-transform: uppercase; font-size: 14px; color: #666;"><strong>Last Save:</strong> <?php echo date("m-d-Y, h:i A", strtotime($MyVolunteer->Timestamp)); ?></p>
<h3>PART I: CONTACT AND DEMOGRAPHIC INFORMATION</h3>
<fieldset>
<legend id="personal_information_toggle">Personal Information</legend>
<ol id="personal_information">
	<li><strong>First Name:</strong> <?= $MyVolunteer->FirstName ?></li>
	<li><strong>Middle Name:</strong> <?= $MyVolunteer->MiddleName ?></li>
	<li><strong>Last Name:</strong> <?= $MyVolunteer->LastName ?></li>
	<li><strong>Date of Birth:</strong> <?= $MyVolunteer->DOBMonth ?>/<?= $MyVolunteer->DOBDay ?>/<?= $MyVolunteer->DOBYear ?></li>
	<li><strong>Gender:</strong> <?= $MyVolunteer->Gender ?></li>
	<li><strong>Home Address:</strong> <?= $MyVolunteer->HomeAddress ?></li>
	<li><strong>Home Address 2:</strong> <?= $MyVolunteer->HomeAddress2 ?></li>
	<li><strong>City:</strong> <?= $MyVolunteer->HomeCity ?></li>
	<li><strong>State/Region:</strong> <?= $MyVolunteer->HomeState ?></li>
	<li><strong>Postal Code:</strong> <?= $MyVolunteer->HomeZip ?></li>
	<li><strong>Country:</strong> <?= $MyVolunteer->HomeCountry ?></li>
	<li><strong>Phone:</strong> <?= $MyVolunteer->Phone1 ?></li>
	<!-- <li><strong>Phone 2:</strong> <?= $MyVolunteer->Phone2 ?></li> -->
	<li><strong>Email:</strong> <a href="mailto:<?= $MyVolunteer->Email1 ?>"><?= $MyVolunteer->Email1 ?></a></li>
	<!-- <li><strong>Email 2:</strong> <a href="mailto:<?= $MyVolunteer->Email2 ?>"><?= $MyVolunteer->Email2 ?></a></li> -->
</ol>
</fieldset>
<fieldset>
<legend id="professional_information_toggle" class="collapsed">Professional Information</legend>
<ol id="professional_information">
	<li><strong>Xylem Location:</strong> <?= $MyVolunteer->Branch ?></li>
	<li><strong>Growth Center:</strong> <?= $MyVolunteer->ValueCenter ?></li>
	<li><strong>Address:</strong> <?= $MyVolunteer->WorkAddress ?></li>
	<li><strong>Address 2:</strong> <?= $MyVolunteer->WorkAddress2 ?></li>
	<li><strong>City:</strong> <?= $MyVolunteer->WorkCity ?></li>
	<li><strong>State/Region:</strong> <?= $MyVolunteer->WorkState ?></li>
	<li><strong>Postal Code:</strong> <?= $MyVolunteer->WorkZip ?></li>
	<li><strong>Country:</strong> <?= $MyVolunteer->WorkCountry ?></li>
</ol>
</fieldset>
<h3>PART II: CONNECTION TO XYLEM WATERMARK</h3>
<fieldset>
<legend id="short_answer_toggle" class="collapsed">Short Answer Questions</legend>
<ol id="short_answer" class="hide">
	<li><strong>Are you a Watermark Ambassador or Champion?</strong> <?php if($MyVolunteer->AmbassadorChampion == "ambassador_champion_yes") { echo 'Yes'; } else { echo 'No'; } ?></li>
	<!-- <li id="know_ambassador_champion_wrap"><strong>Do you know the Watermark Ambassador or Champion at your site?</strong> <?php if($MyVolunteer->KnowAmbassadorChampion == "know_ambassador_champion_yes") { echo 'Yes'; } else { echo 'No'; } ?></li>
	<li id="ambassador_champion_names_wrap"><strong>Please provide the name(s) of the Ambassador or Champion at your site:</strong> <?= $MyVolunteer->AmbassadorChampionNames ?></li> -->
	<li><strong>Have you previously participated in any Watermark programs or activities?</strong> <?php if($MyVolunteer->Participated == "participated_yes") { echo 'Yes'; } else { echo 'No'; } ?></li>
	<li id="which_wrap"><strong>If so, please describe:</strong> <?= $MyVolunteer->Which ?></li>
	<!-- <li><strong>How did you hear about this opportunity?</strong> <?= $MyVolunteer->Hear ?></li> -->
</ol>
</fieldset>
<h3>PART III: EXPERIENCE AND SKILLS</h3>
<fieldset>
<legend id="skills_certifications_toggle" class="collapsed">Languages</legend>
<ol id="skills_certifications" class="hide">
	<li><strong>Primary Language:</strong> <?= $MyVolunteer->PrimaryLanguage ?></li>
	<!-- <li><strong>Primary Language Proficiency:</strong> <?= $MyVolunteer->PrimaryLanguageProficiency ?></li> -->
	<li><strong>Secondary Language:</strong> <?= $MyVolunteer->SecondaryLanguage ?></li>
	<li><strong>Secondary Language Proficiency:</strong> <?= $MyVolunteer->SecondaryLanguageProficiency ?></li>
	<!-- <li><strong>Other Languages:</strong> <?= $MyVolunteer->OtherLanguages ?></li>
	<li><strong>Advanced Degree:</strong> <?= $MyVolunteer->AdvancedDegree ?></li>
	<li><strong>Certification:</strong> <?= $MyVolunteer->Certification ?></li> -->
</ol>

</fieldset>
<fieldset>
<legend id="professional_experience_toggle" class="collapsed">Professional Experience</legend>
<ol id="professional_experience" class="hide">
<li><strong>Advanced Degree or certification?</strong> <?php if($MyVolunteer->AdvancedDegreeYN == "advanceddegree_yes") { echo 'Yes'; } else { echo 'No'; } ?></li>
<li id="degree_certification_names_wrap"><strong>If so, please list:</strong> <?= $MyVolunteer->AdvancedDegree_list ?></li>
	<li><strong>Do you have experience in:</strong> <?= $MyVolunteer->Experience; ?></li>
	<li id="other_list"><strong>If other, please list:</strong> <?= $OtherList ?></li>
</ol>
</fieldset>
<fieldset>
<legend id="individual_capabilities_toggle" class="collapsed">Travel Experience</legend>
<ol id="individual_capabilities" class="hide">
	<li><strong>Are you able to carry loads of 15-25lbs?</strong> <?php if($MyVolunteer->CarryLoads == "carry_loads_yes") { echo 'Yes'; } else { echo 'No'; } ?></li>
	<li><strong>Are you able to be on your feet for 1-2 hours at a time?</strong> <?php if($MyVolunteer->Feet == "feet_yes") { echo 'Yes'; } else { echo 'No'; } ?></li>
	<li><strong>Are you comfortable using smart phone technology?</strong> <?php if($MyVolunteer->SmartPhone == "smart_phone_yes") { echo 'Yes'; } else { echo 'No'; } ?></li>
	<li><strong>Have you ever traveled outside your current country of residence?</strong> <?php if($MyVolunteer->Travel == "travel_yes") { echo 'Yes'; } else { echo 'No'; } ?></li>
</ol>
</fieldset>
<h3>PART IV: ESSAY SECTION</h3>
<fieldset>
<legend id="essay_questions_toggle" class="collapsed">Essay Questions</legend>
<ol id="essay_questions" class="hide">
	<!-- <li><strong>How would you like to play a role in helping Xylem &ldquo;solve water&rdquo;? (up to 300 words)</strong>
	<p><?= $MyVolunteer->SolveWater ?></p> -->
	</li>
	<li><strong>Why do you want to participate in a Xylem Watermark trip? (up to 150 words)</strong>
	<p><?= $MyVolunteer->WhyTrip ?></p>
	</li>
	<li><strong>What do you hope to learn on the trip and how will you apply it to your position at Xylem? (up to 150 words)</strong>
	<p><?= $MyVolunteer->Learn ?></p>
	</li>
</ol>
</fieldset>
<!-- <h3>PART V: TERMS AND CONDITIONS</h3>
<fieldset>
<legend id="liability_toggle" class="collapsed">Liability Agreements</legend>
<ol id="liability" class="hide">
	<li><input type="checkbox" name="liability" value="1" id="liability" class="ckbx" <?php if($MyVolunteer->Liability == "1") { echo 'checked="checked"'; } ?>><label for="liability" class="long_ckbx">I agree to comply with Xylem Inc. company policies and code of conduct while participating in a World Water Corps volunteer trip. *</label></li>
	<li><input type="checkbox" name="responsibilities" value="1" id="responsibilities" class="ckbx" <?php if($MyVolunteer->Responsibilities == "1") { echo 'checked="checked"'; } ?>><label for="responsibilities" class="long_ckbx">I agree to comply with Xylem Inc. volunteer policy while participating in a World Water Corps volunteer trip. *</label></li>
	<li><input type="checkbox" name="policies" value="1" id="policies" class="ckbx" <?php if($MyVolunteer->Policies == "1") { echo 'checked="checked"'; } ?>><label for="policies" class="long_ckbx">I grant permission for Xylem Inc. and partner agencies to use any photos, film, or videos of me, or my likeness, and any quotes or statements given by me, orally or in writing, without restrictions, in any media, in legitimate promotions and accounts for social media and other established communications means of Xylem Inc. and/or Xylem Watermark. *</label></li>
	<li><input type="checkbox" name="blog_permission" value="1" id="blog_permission" class="ckbx" <?php if($MyVolunteer->BlogPermission == "1") { echo 'checked="checked"'; } ?>><label for="blog_permission" class="long_ckbx">I hereby grant permission to use any blog entries created by me relating to any Volunteer trip in which I participate, in any way otherwise approved in this document for other of my personal information. *</label></li>
	<li><input type="checkbox" name="personal_info" value="1" id="personal_info" class="ckbx" <?php if($MyVolunteer->PersonalInfo == "1") { echo 'checked="checked"'; } ?>><label for="personal_info" class="long_ckbx">By submitting this application, in addition to other consents contained in this document, I consent to the provision of my personal information for purposes of determining eligibility for a Volunteer trip, participation in such trip and subsequent promotion of such trip. *</label></li>
</ol>

</fieldset> -->
<div class="clear"></div>
</div>
</body>
</html>