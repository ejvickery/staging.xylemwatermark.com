<?php

/*
Plugin Name: Volunteer Form
Plugin URI: http://visionmix.com
Description: Volunteer form application and management for Xylem Watermark
Author: Alexander Tague
Version: 1.0
*/
global $wpdb;

// Create Volunteer Application table
function volunteer_app_install()
{
    global $wpdb;
    $table = $wpdb->prefix."volunteer_app";
    $structure = "CREATE TABLE $table (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  submit_time varchar(25) DEFAULT NULL,
  user_id varchar(25) DEFAULT NULL,
  first_name varchar(25) DEFAULT NULL,
  middle_name varchar(25) DEFAULT NULL,
  last_name varchar(25) DEFAULT NULL,
  dob_month int(2) DEFAULT NULL,
  dob_day int(2) DEFAULT NULL,
  dob_year int(4) DEFAULT NULL,
  gender varchar(10) DEFAULT NULL,
  home_address1 varchar(100) DEFAULT NULL,
  home_address2 varchar(100) DEFAULT NULL,
  home_city varchar(100) DEFAULT NULL,
  home_state varchar(50) DEFAULT NULL,
  home_zip int(6) DEFAULT NULL,
  home_country varchar(50) DEFAULT NULL,
  phone1 varchar(20) DEFAULT NULL,
  phone2 varchar(20) DEFAULT NULL,
  email1 varchar(50) DEFAULT NULL,
  email2 varchar(50) DEFAULT NULL,
  branch varchar(50) DEFAULT NULL,
  value_center varchar(50) DEFAULT NULL,
  work_address1 varchar(100) DEFAULT NULL,
  work_address2 varchar(100) DEFAULT NULL,
  work_city varchar(100) DEFAULT NULL,
  work_state varchar(100) DEFAULT NULL,
  work_zip int(6) DEFAULT NULL,
  work_country varchar(100) DEFAULT NULL,
  ambassador_champion varchar(25) DEFAULT NULL,
  know_ambassador_champion varchar(30) DEFAULT NULL,
  ambassador_champion_names varchar(200) DEFAULT NULL,
  participated varchar(20) DEFAULT NULL,
  which varchar(200) DEFAULT NULL,
  hear varchar(200) DEFAULT NULL,
  primary_language varchar(100) DEFAULT NULL,
  primary_language_proficiency varchar(20) DEFAULT NULL,
  secondary_language varchar(100) DEFAULT NULL,
  secondary_language_proficiency varchar(20) DEFAULT NULL,
  other_languages varchar(100) DEFAULT NULL,
  advanced_degree varchar(100) DEFAULT NULL,
  advanced_degree_yn varchar(25) DEFAULT NULL,
  advanced_degree_list text,
  certification varchar(100) DEFAULT NULL,
  experience varchar(100) DEFAULT NULL,
  carry_loads varchar(100) DEFAULT NULL,
  feet varchar(100) DEFAULT NULL,
  smart_phone varchar(100) DEFAULT NULL,
  travel varchar(100) DEFAULT NULL,
  solve_water text,
  why_trip text,
  learn text,
  liability int(1) DEFAULT NULL,
  responsibilities int(1) DEFAULT NULL,
  policies int(1) DEFAULT NULL,
  blog_permission int(1) DEFAULT NULL,
  personal_info int(1) DEFAULT NULL,
  PRIMARY KEY (id)
);";
    $wpdb->query($structure);

require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
dbDelta( $structure );

}

add_action('activate_volunteer-form/volunteer-form.php', 'volunteer_app_install');

function init_sessions() {
    if (!session_id()) {
        session_start();
    }
}
add_action('init', 'init_sessions');

// Volunteer Header
add_action('wp_head', 'the_form_header');
function the_form_header() {
$file = volunteer-form.php;
if(@file_exists(TEMPLATEPATH.'/volunteer-app.css')) {
		echo '<link rel="stylesheet" href="'.get_stylesheet_directory_uri().'/volunteer-app.css" type="text/css" media="screen" />'."\n";	
	} else {
		echo '<link rel="stylesheet" href="'.WP_PLUGIN_URL.'/volunteer-form/volunteer-app.css" type="text/css" media="screen" />'."\n";
	}
echo '<script type="text/javascript" src="'.plugins_url( 'js/jquery.validate.min.js' , __FILE__ ).'"></script>
<script type="text/javascript" src="'.plugins_url( 'js/volunteer-app.js' , __FILE__ ).'"></script>';
}

function admin_header() {
$file = volunteer-form.php;
if(@file_exists(TEMPLATEPATH.'/volunteer-app.css')) {
		echo '<link rel="stylesheet" href="'.get_stylesheet_directory_uri().'/volunteer-app.css" type="text/css" media="screen" />'."\n";	
	} else {
		echo '<link rel="stylesheet" href="'.WP_PLUGIN_URL.'/volunteer-form/volunteer-app.css" type="text/css" media="screen" />'."\n";
	}
}
add_action('admin_head', 'admin_header');

function create_volunteer_form() {
ob_start();
	include ('volunteer-form-display.php');
	$inquiry = ob_get_clean();
	return $inquiry;
}

add_shortcode("volunteer", "create_volunteer_form");

add_action('admin_menu', 'volunteer_app_menu');

function volunteer_app_menu() {

$icon = plugins_url( 'images/watermark_icon.png' , __FILE__ );

	add_menu_page( 'Manage Volunteer Applications', 'Volunteers', 'manage_options', 'volunteers', 'volunteer_applications', $icon );
}

function volunteer_applications() {
	if (!current_user_can('manage_options'))  {
		wp_die( __('You do not have sufficient permissions to access this page.') );
	}
	include('volunteer-admin-display.php');
}
?>