<?php
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

Class Volunteer {

   public $Id = 0;
   public $Timestamp = 0;
   public $SubmitTime = "Not Submitted";
   public $UserId;
   public $FirstName;
   public $MiddleName;
   public $LastName;
   public $DOBMonth;
   public $DOBDay;
   public $DOBYear;
   public $Gender;
   public $HomeAddress;
   public $HomeAddress2;
   public $HomeCity;
   public $HomeState;
   public $HomeCountry;
   public $Phone1;
   public $Phone2;
   public $Email1;
   public $Email2;
   public $Branch;
   public $ValueCenter;
   public $WorkAddress;
   public $WorkAddress2;
   public $WorkCity;
   public $WorkState;
   public $WorkZip;
   public $WorkCountry;
   public $AmbassadorChampion;
   public $KnowAmbassadorChampion;
   public $AmbassadorChampionNames;
   public $Participated;
   public $Which;
   public $Hear;
   public $PrimaryLanguage;
   public $PrimaryLanguageProficiency;
   public $SecondaryLanguage;
   public $SecondaryLanguageProficiency;
   public $OtherLanguages;
   public $AdvancedDegree;
   public $AdvancedDegreeYN;
   public $AdvancedDegree_list;
   public $Certification;
   public $Experience;
   public $OtherList;
   public $CarryLoads;
   public $Feet;
   public $SmartPhone;
   public $Travel;
   public $SolveWater;
   public $WhyTrip;
   public $Learn;
   public $Liability;
   public $Responsibilities;
   public $Policies;
   public $BlogPermission;
   public $PersonalInfo;

   public function GetByUser($UserId) {
      global $wpdb;
      $dbResult = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'volunteer_app WHERE user_id = \'$UserId\' LIMIT 1;');
      $this->DBToParams($dbResult);

      return $this; 
   }

   public function Save() {
      global $wpdb;
      
	  $check = intval($wpdb->get_var("SELECT count(user_id) FROM ".$wpdb->prefix."volunteer_app WHERE user_id = ".$this->UserId.";"));
	  if($check > 0) {      
         $wpdb->Sql = 'UPDATE '.$wpdb->prefix.'volunteer_app SET 
				  submit_time               = \'' . $this->SubmitTime . '\',
				  user_id               = \'' . $this->UserId . '\',
				  first_name               = \'' . $this->FirstName . '\',
				  middle_name           = \'' . $this->MiddleName .'\',
				  last_name            = \'' . $this->LastName .'\',
				  dob_month            = \'' . $this->DOBMonth .'\',
				  dob_day             = \'' . $this->DOBDay .'\',
				  dob_year             = \'' . $this->DOBYear .'\',
				  gender                = \'' . $this->Gender .'\',
				  home_address1       = \'' . $this->HomeAddress .'\',
				  home_address2      = \'' . $this->HomeAddress2 .'\',
				  home_city      = \'' . $this->HomeCity .'\',
				  home_state      = \'' . $this->HomeState .'\',
				  home_zip      = \'' . $this->HomeZip .'\',
				  home_country      = \'' . $this->HomeCountry .'\',
				  phone1      = \'' . $this->Phone1 .'\',
				  phone2      = \'' . $this->Phone2 .'\',
				  email1      = \'' . $this->Email1 .'\',
				  email2      = \'' . $this->Email2 .'\',
				  branch      = \'' . $this->Branch .'\',
				  value_center      = \'' . $this->ValueCenter .'\',
				  work_address1      = \'' . $this->WorkAddress .'\',
				  work_address2      = \'' . $this->WorkAddress2 .'\',
				  work_city      = \'' . $this->WorkCity .'\',
				  work_state      = \'' . $this->WorkState .'\',
				  work_zip      = \'' . $this->WorkZip .'\',
				  work_country      = \'' . $this->WorkCountry .'\',
				  ambassador_champion = \'' . $this->AmbassadorChampion .'\',
				  know_ambassador_champion = \'' . $this->KnowAmbassadorChampion .'\',
				  ambassador_champion_names = \'' . $this->AmbassadorChampionNames .'\',
				  participated = \'' . $this->Participated .'\',
				  which = \'' . $this->Which .'\',
				  hear = \'' . $this->Hear .'\',
				  primary_language = \'' . $this->PrimaryLanguage .'\',
				  primary_language_proficiency = \'' . $this->PrimaryLanguageProficiency .'\',
				  secondary_language = \'' . $this->SecondaryLanguage .'\',
				  secondary_language_proficiency = \'' . $this->SecondaryLanguageProficiency .'\',
				  other_languages = \'' . $this->OtherLanguages .'\',
				  advanced_degree = \'' . $this->AdvancedDegree .'\',
				  advanced_degree_yn = \'' . $this->AdvancedDegreeYN .'\',
				  advanced_degree_list = \'' . $this->AdvancedDegree_list .'\',
				  certification = \'' . $this->Certification .'\',
				  experience = \'' . $this->Experience .'\',
				  other_list = \'' . $this->OtherList .'\',
				  carry_loads = \'' . $this->CarryLoads .'\',
				  feet = \'' . $this->Feet .'\',
				  smart_phone = \'' . $this->SmartPhone .'\',
				  travel = \'' . $this->Travel .'\',
				  solve_water = \'' . $this->SolveWater .'\',
				  why_trip = \'' . $this->WhyTrip .'\',
				  learn = \'' . $this->Learn .'\',
				  liability = \'' . $this->Liability .'\',
				  responsibilities = \'' . $this->Responsibilities .'\',
				  policies                = \'' . $this->Policies .'\',
				  blog_permission = \'' . $this->BlogPermission .'\',
				  personal_info                = \'' . $this->PersonalInfo .'\'
				  WHERE user_id = \'' . $this->UserId  . '\';';
							  
      }
      else {
         $wpdb->Sql = 'INSERT INTO '.$wpdb->prefix.'volunteer_app 
                                  (submit_time, user_id, first_name, middle_name, last_name, dob_month, dob_day, dob_year, gender, home_address1, home_address2, home_city, home_state, home_zip, home_country, phone1, phone2, email1, email2, branch, value_center, work_address1, work_address2, work_city, work_state, work_zip, work_country, ambassador_champion, know_ambassador_champion, ambassador_champion_names, participated, which, hear, primary_language, primary_language_proficiency, secondary_language, secondary_language_proficiency, other_languages, advanced_degree, advanced_degree_yn, advanced_degree_list, certification, experience, other_list, carry_loads, feet, smart_phone, travel, solve_water, why_trip, learn, liability, responsibilities, policies, blog_permission, personal_info) 
                                  VALUES
                                  (
                                  	\'' . $this->SubmitTime . '\',
                                  	\'' . $this->UserId . '\',
                                  	\'' . $this->FirstName . '\',
                                  	\'' . $this->MiddleName .'\',
                                  	\'' . $this->LastName .'\',
                                  	\'' . $this->DOBMonth .'\',
                                  	\'' . $this->DOBDay .'\',
                                  	\'' . $this->DOBYear .'\',
                                  	\'' . $this->Gender . '\',
                                  	\'' . $this->HomeAddress .'\',
                                  	\'' . $this->HomeAddress2 .'\',
                                  	\'' . $this->HomeCity .'\',
                                  	\'' . $this->HomeState .'\',
                                  	\'' . $this->HomeZip .'\',
                                  	\'' . $this->HomeCountry .'\',
                                  	\'' . $this->Phone1 . '\',
                                  	\'' . $this->Phone2 .'\',
                                  	\'' . $this->Email1 .'\',
                                  	\'' . $this->Email2 .'\',
                                  	\'' . $this->Branch .'\',
                                  	\'' . $this->ValueCenter .'\',
                                  	\'' . $this->WorkAddress .'\',
                                  	\'' . $this->WorkAddress2 . '\',
                                  	\'' . $this->WorkCity .'\',
                                  	\'' . $this->WorkState .'\',
                                  	\'' . $this->WorkZip .'\',
                                  	\'' . $this->WorkCountry .'\',
                                  	\'' . $this->AmbassadorChampion .'\',
                                  	\'' . $this->KnowAmbassadorChampion .'\',
                                  	\'' . $this->AmbassadorChampionNames . '\',
                                  	\'' . $this->Participated .'\',
                                  	\'' . $this->Which .'\',
                                  	\'' . $this->Hear .'\',
                                  	\'' . $this->PrimaryLanguage .'\',
                                  	\'' . $this->PrimaryLanguageProficiency .'\',
                                  	\'' . $this->SecondaryLanguage . '\',
                                  	\'' . $this->SecondaryLanguageProficiency .'\',
                                  	\'' . $this->OtherLanguages . '\',
                                  	\'' . $this->AdvancedDegree .'\',
                                  	\'' . $this->AdvancedDegreeYN .'\',
                                  	\'' . $this->AdvancedDegree_list .'\',
                                  	\'' . $this->Certification .'\',
                                  	\'' . $this->Experience .'\',
                                  	\'' . $this->OtherList .'\',
                                  	\'' . $this->CarryLoads .'\',
                                  	\'' . $this->Feet . '\',
                                  	\'' . $this->SmartPhone .'\',
                                  	\'' . $this->Travel .'\',
                                  	\'' . $this->SolveWater .'\',
                                  	\'' . $this->WhyTrip .'\',
                                  	\'' . $this->Learn .'\',
                                  	\'' . $this->Liability .'\',
                                  	\'' . $this->Responsibilities .'\',
                                  	\'' . $this->Policies .'\',
                                  	\'' . $this->BlogPermission .'\',
                                  	\'' . $this->PersonalInfo .'\');';                 	
      }
      $wpdb->query($wpdb->Sql);
      
      return $this;
   }

   private function DBToParams($objInstance) {
      $this->Id             = $objInstance[0]->id;
      $this->Timestamp        = $objInstance[0]->timestamp;
      $this->SubmitTime        = $objInstance[0]->submit_time;
      $this->UserId        = $objInstance[0]->user_id;
      $this->FirstName    = $objInstance[0]->first_name;
      $this->MiddleName     = $objInstance[0]->middle_name;
      $this->LastName      = $objInstance[0]->last_name;
      $this->DOBMonth       = $objInstance[0]->dob_month;
      $this->DOBDay       = $objInstance[0]->dob_day;
      $this->DOBYear         = $objInstance[0]->dob_year;
      $this->Gender        = $objInstance[0]->gender;
      $this->HomeAddress        = $objInstance[0]->home_address1;
      $this->HomeAddress2    = $objInstance[0]->home_address2;
      $this->HomeCity     = $objInstance[0]->home_city;
      $this->HomeZip      = $objInstance[0]->home_zip;
      $this->HomeCountry       = $objInstance[0]->home_country;
      $this->Phone1       = $objInstance[0]->phone1;
      $this->Phone2         = $objInstance[0]->phone2;
      $this->Email1        = $objInstance[0]->email1;
      $this->Email2        = $objInstance[0]->email2;
      $this->Branch    = $objInstance[0]->branch;
      $this->ValueCenter    = $objInstance[0]->value_center;
      $this->WorkAddress     = $objInstance[0]->work_address1;
      $this->WorkAddress2      = $objInstance[0]->work_address2;
      $this->WorkCity       = $objInstance[0]->work_city;
      $this->WorkState       = $objInstance[0]->work_state;
      $this->WorkZip       = $objInstance[0]->work_zip;
      $this->WorkCountry         = $objInstance[0]->work_country;
      $this->AmbassadorChampion        = $objInstance[0]->ambassador_champion;
      $this->KnowAmbassadorChampion        = $objInstance[0]->know_ambassador_champion;
      $this->AmbassadorChampionNames    = $objInstance[0]->ambassador_champion_names;
      $this->Participated     = $objInstance[0]->participated;
      $this->Which      = $objInstance[0]->which;
      $this->Hear       = $objInstance[0]->hear;
      $this->PrimaryLanguage       = $objInstance[0]->primary_language;
      $this->PrimaryLanguageProficiency         = $objInstance[0]->primary_language_proficiency;
      $this->SecondaryLanguage        = $objInstance[0]->secondary_language;
      $this->SecondaryLanguageProficiency        = $objInstance[0]->secondary_language_proficiency;
      $this->OtherLanguages        = $objInstance[0]->other_languages;
      $this->AdvancedDegree    = $objInstance[0]->advanced_degree;
      $this->AdvancedDegreeYN    = $objInstance[0]->advanced_degree_yn;
      $this->AdvancedDegree_list    = $objInstance[0]->advanced_degree_list;
      $this->Certification     = $objInstance[0]->certification;
      $this->Experience      = $objInstance[0]->experience;
      $this->OtherList      = $objInstance[0]->other_list;
      $this->CarryLoads       = $objInstance[0]->carry_loads;
      $this->Feet       = $objInstance[0]->feet;
      $this->SmartPhone         = $objInstance[0]->smart_phone;
      $this->Travel        = $objInstance[0]->travel;
      $this->SolveWater     = $objInstance[0]->solve_water;
      $this->WhyTrip      = $objInstance[0]->why_trip;
      $this->Learn       = $objInstance[0]->learn;
      $this->Liability       = $objInstance[0]->liability;
      $this->Responsibilities         = $objInstance[0]->responsibilities;
      $this->Policies        = $objInstance[0]->policies;
      $this->BlogPermission         = $objInstance[0]->blog_permission;
      $this->PersonalInfo        = $objInstance[0]->personal_info;
   }
}

Class Volunteers {
	private $outputArray = array();
	
	public function GetByUser($UserId) {
      global $wpdb;
      $dbResult = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."volunteer_app WHERE user_id = '$UserId';");
      $this->DBToObjectArray($dbResult);

      return $this->outputArray; 
    }
	
	public function GetById($Id) {
      global $wpdb;
      $dbResult = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."volunteer_app WHERE id = '$Id';");
      $this->DBToObjectArray($dbResult);

      return $this->outputArray; 
    }
    
    public function GetAll() {
      global $wpdb;
      $dbResult = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."volunteer_app;");
      $this->DBToObjectArray($dbResult);

      return $this->outputArray; 
    }
	
	private function DBToObjectArray($objInstance) {
		foreach($objInstance as $index => $instance) {
			$MyVolunteer = new Volunteer();
			$MyVolunteer->Id             = $instance->id;
      		$MyVolunteer->Timestamp        = $instance->timestamp;
      		$MyVolunteer->SubmitTime        = $instance->submit_time;
      		$MyVolunteer->UserId        = $instance->user_id;
      		$MyVolunteer->FirstName    = $instance->first_name;
      		$MyVolunteer->MiddleName     = $instance->middle_name;
      		$MyVolunteer->LastName      = $instance->last_name;
      		$MyVolunteer->DOBMonth       = $instance->dob_month;
      		$MyVolunteer->DOBDay       = $instance->dob_day;
     		$MyVolunteer->DOBYear         = $instance->dob_year;
     		$MyVolunteer->Gender        = $instance->gender;
      		$MyVolunteer->HomeAddress       = $instance->home_address1;
     		$MyVolunteer->HomeAddress2         = $instance->home_address2;
     		$MyVolunteer->HomeCity        = $instance->home_city;
     		$MyVolunteer->HomeState        = $instance->home_state;
      		$MyVolunteer->HomeZip    = $instance->home_zip;
      		$MyVolunteer->HomeCountry     = $instance->home_country;
      		$MyVolunteer->Phone1      = $instance->phone1;
      		$MyVolunteer->Phone2       = $instance->phone2;
      		$MyVolunteer->Email1       = $instance->email1;
     		$MyVolunteer->Email2         = $instance->email2;
     		$MyVolunteer->Branch        = $instance->branch;
     		$MyVolunteer->ValueCenter        = $instance->value_center;
      		$MyVolunteer->WorkAddress       = $instance->work_address1;
     		$MyVolunteer->WorkAddress2         = $instance->work_address2;
     		$MyVolunteer->WorkCity        = $instance->work_city;
      		$MyVolunteer->WorkState    = $instance->work_state;
      		$MyVolunteer->WorkZip    = $instance->work_zip;
      		$MyVolunteer->WorkCountry     = $instance->work_country;
      		$MyVolunteer->AmbassadorChampion      = $instance->ambassador_champion;
      		$MyVolunteer->KnowAmbassadorChampion       = $instance->know_ambassador_champion;
      		$MyVolunteer->AmbassadorChampionNames       = $instance->ambassador_champion_names;
     		$MyVolunteer->Participated         = $instance->participated;
     		$MyVolunteer->Which        = $instance->which;
      		$MyVolunteer->Hear       = $instance->hear;
     		$MyVolunteer->PrimaryLanguage         = $instance->primary_language;
     		$MyVolunteer->PrimaryLanguageProficiency        = $instance->primary_language_proficiency;
      		$MyVolunteer->SecondaryLanguage    = $instance->secondary_language;
      		$MyVolunteer->SecondaryLanguageProficiency     = $instance->secondary_language_proficiency;
      		$MyVolunteer->OtherLanguages    = $instance->other_languages;
      		$MyVolunteer->AdvancedDegree      = $instance->advanced_degree;
      		$MyVolunteer->AdvancedDegreeYN      = $instance->advanced_degree_yn;
      		$MyVolunteer->AdvancedDegree_list      = $instance->advanced_degree_list;
      		$MyVolunteer->Certification       = $instance->certification;
      		$MyVolunteer->Experience       = $instance->experience;
      		$MyVolunteer->OtherList       = $instance->other_list;
     		$MyVolunteer->CarryLoads         = $instance->carry_loads;
     		$MyVolunteer->Feet        = $instance->feet;
      		$MyVolunteer->SmartPhone       = $instance->smart_phone;
     		$MyVolunteer->Travel         = $instance->travel;
     		$MyVolunteer->SolveWater        = $instance->solve_water;
      		$MyVolunteer->WhyTrip    = $instance->why_trip;
      		$MyVolunteer->Learn     = $instance->learn;
      		$MyVolunteer->Liability      = $instance->liability;
      		$MyVolunteer->Responsibilities       = $instance->responsibilities;
      		$MyVolunteer->Policies       = $instance->policies;
      		$MyVolunteer->BlogPermission       = $instance->blog_permission;
      		$MyVolunteer->PersonalInfo       = $instance->personal_info;
     		$this->outputArray[$index] = $MyVolunteer;
		}
	}
}
?>