
jQuery(document).ready(function() {

$.validator.addMethod("wordCount",
   function(value, element, params) {
      var typedWords = jQuery.trim(value).split(' ').length;
 
      if(typedWords <= params[0]) {
         return true;
      }
   },
   jQuery.format("Only {0} words allowed.")
);

$.validator.addMethod('cb_selectone', function(value,element){
    if(element.length>0){
        for(var i=0;i<element.length;i++){
            if($(element[i]).val('checked')) return true;
        }
        return false;
    }
    return false;
}, 'Please select at least one option');

$("#volunteer_form").validate({
        rules: {
          first_name: "required",// simple rule, converted to {required:true}
          email1: {// compound rule
          required: true,
          email: true
        },
          email2: {// compound rule
          required: false,
          email: true
        },
          last_name: "required",
          dob_month: "required",
          dob_day: "required",
          dob_year: "required",
          gender: "required",
          home_address1: "required",
          home_city: "required",
          home_state: "required",
          home_zip: "required",
          home_country: "required",
          phone1: "required",
          branch: "required",
          value_center: "required",
          work_address1: "required",
          work_city: "required",
          work_state: "required",
          work_zip: "required",
          work_country: "required",
          hear: "required",
          primary_language: "required",
          primary_language_proficiency: "required",
          carry_loads: "required",
          feet: "required",
		  solve_water: {
			 required: true,
			 wordCount: ['300']
		  },
		  why_trip: {
			 required: true,
			 wordCount: ['150']
		  },
		  learn: {
			 required: true,
			 wordCount: ['150']
		  },
          liability: "required",
          responsibilities: "required",
          policies: "required",
          blog_permission: "required",
          personal_info: "required"
        },
        messages: {
		  solve_water: "Your essay must be 300 words or less.",
		  why_trip: "Your essay must be 200 words or less.",
		  learn: "Your essay must be 150 words or less."
        }
      });

setInterval(function(){
  var form = $('#volunteer_form');
  var method = form.attr('method').toLowerCase();      // "get" or "post"

$("#autosave").css("display","none");  
  $[method]("http://staging.xylemwatermark.com/wp-content/plugins/volunteer-form/admin.php?action=save", form.serialize(), function(data){
  $('#autosave').slideDown();
  $('#autosave').delay(800).slideUp();
  });
},60000);                                      // do it every minute

$("#save").click(function(){
  var form = $('#volunteer_form');
  var method = form.attr('method').toLowerCase();      // "get" or "post"
  $[method]("http://staging.xylemwatermark.com/wp-content/plugins/volunteer-form/admin.php?action=save", form.serialize(), function(data){
  alert("Form saved.");
  });
});

	  // $("select#primary_language_proficiency").css("display","none");
	  // $("select#secondary_language_proficiency").css("display","none");
	   
	  // $("span.ui-slider-label").css("margin-left","-10");
	   
		// Toggle textarea to list other experience
	   $("#other_list").css("display","none");

	   $("#other").click(function(){

		// If checked
		if ($("#other").is(":checked"))
		{
			//show the hidden question
			$("#other_list").show("fast");
		}
		else
		{
			//otherwise, hide it
			$("#other_list").hide("fast");
		}
	  });

// Decision tree toggling for the short answer section
        var newVal1 = $(":radio[name='ambassador_champion']:checked").val();

            if (newVal1 == "ambassador_champion_yes") {
           $("#know_ambassador_champion_wrap").css("display","none");
            }

	  $(":radio[name='ambassador_champion']").click(function(){
                        var newVal1 = $(":radio[name='ambassador_champion']:checked").val();
		  if (newVal1 == "ambassador_champion_no") {
			$("#know_ambassador_champion_wrap").show();
		  } else {
			$("#know_ambassador_champion_wrap").hide();
			$("#ambassador_champion_names_wrap").hide();
		  }
		});

	   $("#ambassador_champion_names_wrap").css("display","none");
	  
	  $(":radio[name='know_ambassador_champion']").click(function(){
		  var newVal = $(":radio[name='know_ambassador_champion']:checked").val();
		  if (newVal == "know_ambassador_champion_yes") {
			$("#ambassador_champion_names_wrap").show();
		  } else {
			$("#ambassador_champion_names_wrap").hide();
		  }
		});

var newVal2 = $(":radio[name='participated']:checked").val();
if (newVal2 == "ambassador_champion_no") {
	   $("#which_wrap").css("display","none");
	   }
	  
	  $(":radio[name='participated']").change(function(){
		  var newVal = $(":radio[name='participated']:checked").val();
		  if (newVal == "participated_yes") {
			$("#which_wrap").show();
		  } else {
			$("#which_wrap").hide();
		  }
		});
		
		var newVal3 = $(":radio[name='advanced_degree_yn']:checked").val();
		if (newVal2 == "advanceddegree_no") {
		$("#advanceddegree_wrap").css("display","none");
		}
		
		$(":radio[name='advanced_degree_yn']").change(function(){
		  var newVal = $(":radio[name='advanced_degree_yn']:checked").val();
		  if (newVal == "advanceddegree_yes") {
			$("#advanceddegree_wrap").show();
		  } else {
			$("#advanceddegree_wrap").hide();
		  }
		});
	  
	  
	  
	//  $("a[href^=#]").click(function(e) { e.preventDefault(); var dest = $(this).attr('href'); console.log(dest); $('html,body').animate({ scrollTop: $(dest).offset().top - 600}, 'slow'); });
	  
	  
	  // Toggle sections on the legend and on completion
		$(".hide").css("display","none");
		$(".show").css("display","block");
		
		$('#expand').click(function(event) {
			event.preventDefault();
			$(".hide").removeClass('hide').addClass('show');
			$("#personal_information").removeClass('hide').addClass('show');
			$("legend").addClass('expanded').removeClass('collapsed');
		});
		
		$('#collapse').click(function(event) {
			event.preventDefault();
			$(".show").removeClass('show').addClass('hide');
			$("#personal_information").addClass('hide').removeClass('show');
			$("legend").addClass('collapsed').removeClass('expanded');
		});
		
		$('#personal_information_toggle').click(function() {
			$('#personal_information').toggleClass('show hide');
			if($(this).hasClass('expanded'))
			{
				$(this).addClass('collapsed').removeClass('expanded');
			}
			else
			{
				$(this).addClass('expanded').removeClass('collapsed');
			}
		});
		
		$('.personal_information').click(function(event) {
			event.preventDefault();
			$("#personal_information").toggleClass('show hide');
			$("#professional_information").toggleClass('show hide');
			$("#personal_information_toggle").addClass('collapsed').removeClass('expanded');
			$("#professional_information_toggle").addClass('expanded').removeClass('collapsed');
		});
		
		$('#professional_information_toggle').click(function() {
			$('#professional_information').toggleClass('show hide');
			if($(this).hasClass('expanded'))
			{
				$(this).addClass('collapsed').removeClass('expanded');
			}
			else
			{
				$(this).addClass('expanded').removeClass('collapsed');
			}
		});
	
		
		$('.professional_information').click(function(event) {
			event.preventDefault();
			$("#professional_information").toggleClass('show hide');
			$("#short_answer").toggleClass('show hide');
			$("#professional_information_toggle").addClass('collapsed').removeClass('expanded');
			$("#short_answer_toggle").addClass('expanded').removeClass('collapsed');
		});
		
		$('#short_answer_toggle').click(function() {
			$('#short_answer').toggleClass('show hide');
			if($(this).hasClass('expanded'))
			{
				$(this).addClass('collapsed').removeClass('expanded');
			}
			else
			{
				$(this).addClass('expanded').removeClass('collapsed');
			}
		});
		
		$('#work_country').change(function() {
			$("#short_answer").toggleClass('show hide');
			$("#professional_information").toggleClass('show hide');
			$("#professional_information_toggle").addClass('collapsed').removeClass('expanded');
			$("#short_answer_toggle").addClass('expanded').removeClass('collapsed');
		});
		
		$('.short_answer').click(function(event) {
			event.preventDefault();
			$("#short_answer").toggleClass('show hide');
			$("#skills_certifications").toggleClass('show hide');
			$("#short_answer_toggle").addClass('collapsed').removeClass('expanded');
			$("#skills_certifications_toggle").addClass('expanded').removeClass('collapsed');
		});
		
		$('#skills_certifications_toggle').click(function() {
			$('#skills_certifications').toggleClass('show hide');
			if($(this).hasClass('expanded'))
			{
				$(this).addClass('collapsed').removeClass('expanded');
			}
			else
			{
				$(this).addClass('expanded').removeClass('collapsed');
			}
		});
		
		$('#hear').change(function() {
			$("#skills_certifications").toggleClass('show hide');
			$("#short_answer").toggleClass('show hide');
			$("#short_answer_toggle").addClass('collapsed').removeClass('expanded');
			$("#skills_certifications_toggle").addClass('expanded').removeClass('collapsed');
		});
		
		$('.skills_certifications').click(function(event) {
			event.preventDefault();
			$("#skills_certifications").toggleClass('show hide');
			$("#professional_experience").toggleClass('show hide');
			$("#skills_certifications_toggle").addClass('collapsed').removeClass('expanded');
			$("#professional_experience_toggle").addClass('expanded').removeClass('collapsed');
		});
		
		$('#professional_experience_toggle').click(function() {
			$('#professional_experience').toggleClass('show hide');
			if($(this).hasClass('expanded'))
			{
				$(this).addClass('collapsed').removeClass('expanded');
			}
			else
			{
				$(this).addClass('expanded').removeClass('collapsed');
			}
		});
		
		$('#certification').change(function() {
			$("#professional_experience").toggleClass('show hide');
			$("#skills_certifications").toggleClass('show hide');
			$("#skills_certifications_toggle").addClass('collapsed').removeClass('expanded');
			$("#professional_experience_toggle").addClass('expanded').removeClass('collapsed');
		});
		
		$('.professional_experience').click(function(event) {
			event.preventDefault();
			$("#professional_experience").toggleClass('show hide');
			$("#individual_capabilities").toggleClass('show hide');
			$("#professional_experience_toggle").addClass('collapsed').removeClass('expanded');
			$("#individual_capabilities_toggle").addClass('expanded').removeClass('collapsed');
		});
		
		$('#individual_capabilities_toggle').click(function() {
			$('#individual_capabilities').toggleClass('show hide');
			if($(this).hasClass('expanded'))
			{
				$(this).addClass('collapsed').removeClass('expanded');
			}
			else
			{
				$(this).addClass('expanded').removeClass('collapsed');
			}
		});
		
		$(":radio[name='travel']").change(function() {
			$("#essay_questions").toggleClass('show hide');
			$("#individual_capabilities").toggleClass('show hide');
			$("#individual_capabilities_toggle").addClass('collapsed').removeClass('expanded');
			$("#essay_questions_toggle").addClass('expanded').removeClass('collapsed');
		});
		
		$('.individual_capabilities').click(function(event) {
			event.preventDefault();
			$("#individual_capabilities").toggleClass('show hide');
			$("#essay_questions").toggleClass('show hide');
			$("#individual_capabilities_toggle").addClass('collapsed').removeClass('expanded');
			$("#essay_questions_toggle").addClass('expanded').removeClass('collapsed');
		});
		
		$('#essay_questions_toggle').click(function() {
			$('#essay_questions').toggleClass('show hide');
			if($(this).hasClass('expanded'))
			{
				$(this).addClass('collapsed').removeClass('expanded');
			}
			else
			{
				$(this).addClass('expanded').removeClass('collapsed');
			}
		});
		
		$('#learn').change(function() {
			$("#liability").toggleClass('show hide');
			$("#essay_questions").toggleClass('show hide');
			$("#essay_questions_toggle").addClass('collapsed').removeClass('expanded');
			$("#liability_toggle").addClass('expanded').removeClass('collapsed');
		});
		
		$('.essay_questions').click(function(event) {
			event.preventDefault();
			$("#essay_questions").toggleClass('show hide');
			$("#liability").toggleClass('show hide');
			$("#essay_questions_toggle").addClass('collapsed').removeClass('expanded');
			$("#liability_toggle").addClass('expanded').removeClass('collapsed');
		});
		
		$('#liability_toggle').click(function() {
			$('#liability').toggleClass('show hide');
			if($(this).hasClass('expanded'))
			{
				$(this).addClass('collapsed').removeClass('expanded');
			}
			else
			{
				$(this).addClass('expanded').removeClass('collapsed');
			}
		});

	});
	
(function($){
	$.fn.textareaCounter = function(options) {
		// setting the defaults
		// $("textarea").textareaCounter({ limit: 100 });
		var defaults = {
			limit: 100
		};	
		var options = $.extend(defaults, options);
 
		// and the plugin begins
		return this.each(function() {
			var obj, text, wordcount, limited;
 
			obj = $(this);
 
			obj.after('<span style="font-size: 11px; clear: both; margin-top: 3px; display: block;" class="counter-text">Max. '+options.limit+' words</span>');
 
			// function to check word count in field			
			var countcheck = function() {
		    text = obj.val();
		    if(text === "") {
		    	wordcount = 0;
		    } else {
			    wordcount = $.trim(text).split(" ").length;
				}
		    if(wordcount >= options.limit) {
		      obj.parent().find(".counter-text").html('<span style="color: #DD0000;">'+(options.limit - wordcount)+' words left</span>');
					limited = $.trim(text).split(" ", options.limit);
					limited = limited.join(" ");
					$(this).val(limited);
		    } else {
		      obj.parent().find(".counter-text").html((options.limit - wordcount)+' words left');
		    } 
			}
 
			// if field is not empty, count words
			if(obj.val() != '') {
				countcheck(); }
 
 			// if field changes, count words
			obj.keyup(function() {
				countcheck(); });
		});
	};
})(jQuery);

var Timer;
var TotalSeconds;


function CreateTimer(TimerID, Time) {
    Timer = document.getElementById(TimerID);
    TotalSeconds = Time;

    UpdateTimer()
    window.setTimeout("Tick()", 1000);
}

function Tick() {
    TotalSeconds -= 1;
    UpdateTimer()
    window.setTimeout("Tick()", 1000);
}

function UpdateTimer() {
    Timer.innerHTML = TotalSeconds;
}