<?php

require('pdf/fpdf.php');
require('classes.php');

class PDF extends FPDF
{
var $B;
var $I;
var $U;
var $HREF;

function PDF($orientation='P', $unit='mm', $size='Letter')
{
    // Call parent constructor
    $this->FPDF($orientation,$unit,$size);
    // Initialization
    $this->B = 0;
    $this->I = 0;
    $this->U = 0;
    $this->HREF = '';
}

function WriteHTML($html)
{
    // HTML parser
    $html = str_replace("\n",' ',$html);
    $html = iconv('UTF-8', 'ISO-8859-1', $html);
    $a = preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            // Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(5,$e);
        }
        else
        {
            // Tag
            if($e[0]=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                // Extract attributes
                $a2 = explode(' ',$e);
                $tag = strtoupper(array_shift($a2));
                $attr = array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])] = $a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

	function OpenTag($tag,$attr)
	{
    // Opening tag
    if($tag=='B' || $tag=='I' || $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF = $attr['HREF'];
    if($tag=='BR')
        $this->Ln(5);
    if($tag=='HR')
		$this->PutLine();
    if($tag=='H1'){
		$this->Ln(5);
		$this->SetTextColor(18,163,123);
		$this->SetFontSize(18);
		}
	if($tag=='H2'){
		$this->Ln(3);
		$this->SetTextColor(0,141,178);
		$this->SetFontSize(14);
		}
	if($tag=='H3'){
		$this->Ln(5);
		$this->SetTextColor(102,102,102);
		$this->SetFontSize(14);
		}
}
	function CloseTag($tag)
	{
		//Closing tag
		if ($tag=='H1' || $tag=='H3' || $tag=='H4'){
			$this->Ln(6);
			$this->SetFont('Helvetica','',12);
			$this->SetFontSize(12);
			$this->mySetTextColor(-1);
		}
		if ($tag=='H2'){
			$this->Ln(4);
			$this->SetFont('Helvetica','',12);
			$this->SetFontSize(12);
			$this->mySetTextColor(-1);
		}
		if($tag=='P')
			$this->Ln(6);
		if ($tag=='PRE'){
			$this->SetFont('Times','',12);
			$this->SetFontSize(12);
			$this->PRE=false;
		}
		if ($tag=='RED' || $tag=='BLUE')
			$this->mySetTextColor(-1);
		if ($tag=='BLOCKQUOTE'){
			$this->mySetTextColor(0,0,0);
			$this->Ln(3);
		}
		if($tag=='B' || $tag=='I' || $tag=='U')
        $this->SetStyle($tag,false);
		if($tag=='A')
			$this->HREF='';
	}

function SetStyle($tag, $enable)
{
    // Modify style and select corresponding font
    $this->$tag += ($enable ? 1 : -1);
    $style = '';
    foreach(array('B', 'I', 'U') as $s)
    {
        if($this->$s>0)
            $style .= $s;
    }
    $this->SetFont('',$style);
}

function PutLink($URL, $txt)
{
    // Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(5,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}

	function PutLine()
	{
		$this->Ln(2);
		$this->Line($this->GetX(),$this->GetY(),$this->GetX()+187,$this->GetY());
		$this->Ln(3);
	}

	function mySetTextColor($r,$g=0,$b=0){
		static $_r=0, $_g=0, $_b=0;

		if ($r==-1) 
			$this->SetTextColor($_r,$_g,$_b);
		else {
			$this->SetTextColor($r,$g,$b);
			$_r=$r;
			$_g=$g;
			$_b=$b;
		}
	}
}




	$MyVolunteers = new Volunteers();
	$MyVolunteers = $MyVolunteers->GetAll();
	
	foreach($MyVolunteers as $MyVolunteer) {
		if($MyVolunteer->SubmitTime !== "Not Submitted") {
	
if($MyVolunteer->AmbassadorChampion == "ambassador_champion_yes") { $AmbassadorChampion = "Yes"; } else { $AmbassadorChampion = "No"; }
	
	if($MyVolunteer->KnowAmbassadorChampion == "know_ambassador_champion_yes") { $KnowAmbassadorChampion = 'Yes'; } else { $KnowAmbassadorChampion = 'No'; }
	
	if($MyVolunteer->Participated == "participated_yes") { $Participated = 'Yes'; } else { $Participated = 'No'; }
	
	if($MyVolunteer->CarryLoads == "carry_loads_yes") { $CarryLoads = 'Yes'; } else { $CarryLoads = 'No'; }
	
	if($MyVolunteer->Feet == "feet_yes") { $Feet = 'Yes'; } else { $Feet = 'No'; }
	
	if($MyVolunteer->SmartPhone == "smart_phone_yes") { $SmartPhone = 'Yes'; } else { $SmartPhone = 'No'; }
	
	if($MyVolunteer->Travel == "travel_yes") { $Travel = 'Yes'; } else { $Travel =  'No'; }
	
$html = 
"<h3>Submission Time: " . $MyVolunteer->SubmitTime . "</h3>"
. "<h1>PART I: CONTACT AND DEMOGRAPHIC INFORMATION</h1>"
. "<h2>Personal Information</h2><hr>"
.  "<p><b>Name:</b> " . $MyVolunteer->FirstName . " "
. $MyVolunteer->MiddleName .' '
. $MyVolunteer->LastName ."</p>"
. '<p><b>Date of Birth:</b> ' . $MyVolunteer->DOBMonth . '/' . $MyVolunteer->DOBDay . '/' . $MyVolunteer->DOBYear . "</p>"
. '<p><b>Gender:</b> ' . $MyVolunteer->Gender . "</p>"
. '<p><b>Home Address:</b> ' . $MyVolunteer->HomeAddress ."</p>"
. '<p><b>Home Address 2:</b> ' . $MyVolunteer->HomeAddress2 ."</p>"
. '<p><b>Home City:</b> ' . $MyVolunteer->HomeCity ."</p>"
. '<p><b>Home State:</b> ' . $MyVolunteer->HomeState ."</p>"
. '<p><b>Home Zip:</b> ' . $MyVolunteer->HomeZip ."</p>"
. '<p><b>Home Country:</b> ' . $MyVolunteer->HomeCountry ."</p>"
. '<p><b>Phone:</b> ' . $MyVolunteer->Phone1 . "</p>"
. '<p><b>Email:</b> <a href="mailto:'.$MyVolunteer->Email1.'">' . $MyVolunteer->Email1 ."</a></p><br />
<h2>Professional Information</h2><hr>"
. '<p><b>Xylem Location:</b> ' . $MyVolunteer->Branch ."</p>"
. '<p><b>Growth Center:</b> ' . $MyVolunteer->ValueCenter ."</p>"
. '<p><b>Work Address:</b> ' . $MyVolunteer->WorkAddress ."</p>"
. '<p><b>Work Address 2:</b> ' . $MyVolunteer->WorkAddress2 . "</p>"
. '<p><b>Work City:</b> ' . $MyVolunteer->WorkCity ."</p>"
. '<p><b>Work State:</b> ' . $MyVolunteer->WorkState ."</p>"
. '<p><b>Work Zip:</b> ' . $MyVolunteer->WorkZip ."</p>"
. '<p><b>Work Country:</b> ' . $MyVolunteer->WorkCountry ."</p><br />"

. "<h1>PART II: CONNECTION TO XYLEM WATERMARK</h1>"
. "<h2>Short Answer Questions</h2><hr>"
. '<p><b>Are you a Watermark Ambassador or Champion?</b> ' . $AmbassadorChampion ."</p>"
//. '<p><b>Do you know the Watermark Ambassador or Champion at your site?</b> ' . $KnowAmbassadorChampion ."</p>"
//. '<p><b>Please provide the name(s) of the Ambassador or Champion at your site:</b> ' . $MyVolunteer->AmbassadorChampionNames . "</p>"
. '<p><b>Have you previously participated in any Watermark programs or activities?</b> ' . $Participated ."</p>"
. '<p><b>Which programs or activities?</b> ' . $MyVolunteer->Which ."</p>"

. "<h1>PART III: EXPERIENCE AND SKILLS</h1>"
. "<h2>Languages</h2><hr>"
. '<p><b>Primary Language:</b> ' . $MyVolunteer->PrimaryLanguage ."</p>"
. '<p><b>Secondary Language:</b> ' . $MyVolunteer->SecondaryLanguage . "</p>"
. '<p><b>Secondary Language Proficiency:</b> ' . $MyVolunteer->SecondaryLanguageProficiency ."</p>"
. "<h2>Professional Experience</h2><hr>"
. '<p><b>Advanced Degree:</b> ' . $MyVolunteer->AdvancedDegreeYN ."</p>"
. '<p><b>If so, please list:</b> ' . $MyVolunteer->AdvancedDegree_list ."</p>"
. '<p><b>Do you have experience in:</b> ' . $MyVolunteer->Experience ."</p>"
. '<p><b>Please list other experiences:</b> ' . $MyVolunteer->OtherList ."</p>"
. "<h2>Travel Experience</h2><hr>"
. '<p><b>Are you able to carry loads of 15-25lbs?</b> ' . $CarryLoads ."</p>"
. '<p><b>Are you able to be on your feet for 1-2 hours at a time?</b> ' . $Feet . "</p>"
. '<p><b>Are you comfortable using smart phone technology?</b> ' . $SmartPhone ."</p>"
. '<p><b>Have you ever traveled outside your current country of residence?</b> ' . $Travel ."</p><br />"

. "<h1>PART IV: ESSAY SECTION</h1>"
. "<h2>Essay Questions</h2><hr>"
. '<p><b>Why do you want to participate in a Xylem Watermark trip? (up to 150 words)</b></p>' . $MyVolunteer->WhyTrip ."<br /><br />"
. '<p><b>What do you hope to learn on the trip and how will you apply it to your position at Xylem? (up to 150 words)</b></p>' . $MyVolunteer->Learn . "<br />";
	
	$pdf = new PDF();
	$pdf->AddPage();
	$pdf->SetFont('Helvetica','',12);
	$pdf->WriteHTML($html);
	$pdf->Output('volunteers/'.$MyVolunteer->LastName.'_'.$MyVolunteer->FirstName.'.pdf','F');
	
		}
	}


// Create a zip of the volunteers directory and force download

$directoryToZip="volunteers"; // This will zip all the file(s) in this present working directory

$outputDir=""; //Replace "/" with the name of the desired output directory.
$zipName="volunteers.zip";

include_once("CreateZipFile.inc.php");
$createZipFile=new CreateZipFile;

//Code toZip a directory and all its files/subdirectories
$createZipFile->zipDirectory($directoryToZip,$outputDir);

$rand=md5(microtime().rand(0,999999));
$zipName=$rand."_".$zipName;
$fd=fopen($zipName, "wb");
$out=fwrite($fd,$createZipFile->getZippedfile());
fclose($fd);
$createZipFile->forceDownload($zipName);
@unlink($zipName);

function SureRemoveDir($dir, $DeleteMe) {
    if(!$dh = @opendir($dir)) return;
    while (false !== ($obj = readdir($dh))) {
        if($obj=='.' || $obj=='..') continue;
        if (!@unlink($dir.'/'.$obj)) SureRemoveDir($dir.'/'.$obj, true);
    }

    closedir($dh);
    if ($DeleteMe){
        @rmdir($dir);
    }
}

SureRemoveDir('volunteers',false);

echo '<script type="text/javascript"> window.close(); </script>';
?>