<?php
global $wpdb;
include('classes.php');
?>
<div id="volunteers" class="wrap"><h2>Volunteer Applicants</h2>
<table class="wp-list-table widefat fixed">
<thead>
<tr><th class="manage-column column-cb check-column"><input type="checkbox"></th><th class="manage-column sortable desc"><a href="#"><span>Name</span><span class="sorting-indicator"></span></a></th><th class="manage-column sortable desc"><a href="#"><span>Xylem Branch</span><span class="sorting-indicator"></span></a></th><th class="manage-column sortable desc"><a href="#"><span>Last Save</span><span class="sorting-indicator"></span></a></th><th class="manage-column sortable desc"><a href="#"><span>Date Submitted</span><span class="sorting-indicator"></span></a></th></tr>
</thead>
<tbody>
		<?php
			$MyVolunteers = new Volunteers();
			$MyVolunteers = $MyVolunteers->GetAll();
			
			$rowclass = 0;
			foreach($MyVolunteers as $MyVolunteer) { ?>
	<tr class="row<?= $rowclass ?>">
	<th class="check-column"><input type="checkbox"></th>
	<td><?= $MyVolunteer->FirstName ?> <?= $MyVolunteer->LastName ?></td>
	<td><?= $MyVolunteer->Branch ?></td>
	<td><?= date("F j, Y - g:ia", strtotime($MyVolunteer->Timestamp)) ?></td>
	<td><?= date("F j, Y - g:ia", strtotime($MyVolunteer->Timestamp)) ?></td>
	</tr>
<?php $rowclass = 1 - $rowclass; } ?>
</tbody>
<tfoot>
<tr><th class="manage-column column-cb check-column"><input type="checkbox"></th><th class="manage-column sortable desc"><a href="#"><span>Name</span><span class="sorting-indicator"></span></a></th><th class="manage-column sortable desc"><a href="#"><span>Xylem Branch</span><span class="sorting-indicator"></span></a></th><th class="manage-column sortable desc"><a href="#"><span>Last Save</span><span class="sorting-indicator"></span></a></th><th class="manage-column sortable desc"><a href="#"><span>Date Submitted</span><span class="sorting-indicator"></span></a></th></tr>
</tfoot>
</table>
</div>