<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>
<div id="content">

<div class="left_col">
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div class="post">
<h3><a href="/blog">&lt; RECENT POSTS</a></h3>
<div class="divider"></div>
					<div class="date-meta"><?php twentyten_posted_on(); ?></div>
					<h2><?php the_title(); ?></h2>
					<div class="author typeface-js"><div class="author_image"><?php the_author_image(); ?></div>
					<p><span class="author_name"><?php the_author(); ?></span><br />
					<span class="author_title"><?php the_author_meta('job_title'); ?>
					</p>
					<div class="clear"></div>
					</div>

						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'twentyten' ), 'after' => '' ) ); ?>

<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their description, show a bio on their entries  ?>
							<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'twentyten_author_bio_avatar_size', 60 ) ); ?>
							<h2><?php printf( esc_attr__( 'About %s', 'twentyten' ), get_the_author() ); ?></h2>
							<?php the_author_meta( 'description' ); ?>
							<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
								<?php printf( __( 'View all posts by %s &rarr;', 'twentyten' ), get_the_author() ); ?>
							</a>
<?php endif; ?>
</div>


<?php endwhile; // end of the loop. ?>
</div>

<?php get_sidebar(); ?>
</div>
<div class="connect_footer">
<div class="connect_tagline">
CONNECT WITH US
</div>
<ul>
	<li><a href="http://www.facebook.com/xylemwatermark" target="_blank"><img alt="facebook" src="<?php bloginfo('template_url'); ?>/images/fb.png" width="27" height="27"></a></li>
	<li><a href="http://twitter.com/#!/xylemwatermark" target="_blank"><img alt="twitter" src="<?php bloginfo('template_url'); ?>/images/twitter.png" width="27" height="27"></a></li>
</ul>
</div>
<?php get_footer(); ?>