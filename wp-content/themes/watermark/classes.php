<?php
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

Class Donation {

   public $Id = 0;
   public $Timestamp = 0;
   public $FirstName;
   public $LastName;
   public $Email;
   public $Location;
   public $ValueCenter;
   public $Cause;
   public $Currency;
   public $Amount;
   public $Approved;
   public $TransactionId;

   public function GetByUser($UserId) {
      global $wpdb;
      $dbResult = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'donation_app WHERE user_id = \'$UserId\' LIMIT 1;');
      $this->DBToParams($dbResult);

      return $this; 
   }

   public function Save() {
      global $wpdb;
      
	  $check = intval($wpdb->get_var("SELECT count(id) FROM ".$wpdb->prefix."donation_app WHERE id = ".$this->Id.";"));
	  if($check > 0) {      
         $wpdb->Sql = 'UPDATE '.$wpdb->prefix.'donation_app SET 
				  first_name               = \'' . $this->FirstName . '\',
				  last_name            = \'' . $this->LastName .'\',
				  email      = \'' . $this->Email .'\',
				  location      = \'' . $this->Location .'\',
				  value_center      = \'' . $this->ValueCenter .'\',
				  cause      = \'' . $this->Cause .'\',
				  currency      = \'' . $this->Currency .'\',
				  amount      = \'' . $this->Amount .'\',
				  approved      = \'' . $this->Approved .'\',
				  transaction_id      = \'' . $this->TransactionId .'\'
				  WHERE id = \'' . $this->Id  . '\';';
							  
      }
      else {
         $wpdb->Sql = 'INSERT INTO '.$wpdb->prefix.'donation_app 
                                  (first_name, last_name, email, location, value_center, cause, currency, amount, approved, transaction_id) 
                                  VALUES
                                  (
                                  	\'' . $this->FirstName . '\',
                                  	\'' . $this->LastName .'\',
                                  	\'' . $this->Email .'\',
                                  	\'' . $this->Location .'\',
                                  	\'' . $this->ValueCenter .'\',
                                  	\'' . $this->Cause .'\',
                                  	\'' . $this->Currency . '\',
                                  	\'' . $this->Amount . '\',
                                  	\'' . $this->Approved . '\',
                                  	\'' . $this->TransactionId .'\');';
      }
      $wpdb->query($wpdb->Sql);
      
      return $this;
   }

   private function DBToParams($objInstance) {
      $this->Id             = $objInstance[0]->id;
      $this->Timestamp        = $objInstance[0]->timestamp;
      $this->FirstName    = $objInstance[0]->first_name;
      $this->LastName      = $objInstance[0]->last_name;
      $this->Email        = $objInstance[0]->email;
      $this->Location    = $objInstance[0]->location;
      $this->ValueCenter    = $objInstance[0]->value_center;
      $this->Cause     = $objInstance[0]->cause;
      $this->Currency      = $objInstance[0]->currency;
      $this->Amount       = $objInstance[0]->amount;
      $this->Approved       = $objInstance[0]->approved;
      $this->TransactionId  = $objInstance[0]->transaction_id;
   }
}

Class Donations {
	private $outputArray = array();
	
	public function GetById($Id) {
      global $wpdb;
      $dbResult = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."donation_app WHERE id = '$Id';");
      $this->DBToObjectArray($dbResult);

      return $this->outputArray; 
    }
	
	public function GetByTransactionId($TransactionId) {
      global $wpdb;
      $dbResult = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."donation_app WHERE transaction_id = '$TransactionId';");
      $this->DBToObjectArray($dbResult);

      return $this->outputArray; 
    }
    
    public function GetAll() {
      global $wpdb;
      $dbResult = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."donation_app WHERE approved = '1';");
      $this->DBToObjectArray($dbResult);

      return $this->outputArray; 
    }
	
	private function DBToObjectArray($objInstance) {
		foreach($objInstance as $index => $instance) {
			$MyDonation = new Donation();
			$MyDonation->Id             = $instance->id;
      		$MyDonation->Timestamp        = $instance->timestamp;
      		$MyDonation->FirstName    = $instance->first_name;
      		$MyDonation->LastName      = $instance->last_name;
      		$MyDonation->Email       = $instance->email;
     		$MyDonation->Location        = $instance->location;
     		$MyDonation->ValueCenter        = $instance->value_center;
      		$MyDonation->Cause       = $instance->cause;
     		$MyDonation->Currency         = $instance->currency;
     		$MyDonation->Amount        = $instance->amount;
     		$MyDonation->Approved        = $instance->approved;
     		$MyDonation->TransactionId        = $instance->transaction_id;
     		$this->outputArray[$index] = $MyDonation;
		}
	}
}
?>