<?php
/**
 * Template Name: One column, no sidebar
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<div id="content">
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<!-- <h1><?php the_title(); ?></h1> -->
				<?php the_content(); ?>
				<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'twentyten' ), 'after' => '' ) ); ?>

<?php endwhile; ?>
</div>
<div class="connect_footer">
<div class="connect_tagline">
CONNECT WITH US
</div>
<ul>
	<li><a href="http://www.facebook.com/xylemwatermark" target="_blank"><img alt="facebook" src="<?php bloginfo('template_url'); ?>/images/fb.png" width="27" height="27"></a></li>
	<li><a href="http://twitter.com/#!/xylemwatermark" target="_blank"><img alt="twitter" src="<?php bloginfo('template_url'); ?>/images/twitter.png" width="27" height="27"></a></li>
</ul>
</div>
<?php get_footer(); ?>