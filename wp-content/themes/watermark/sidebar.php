<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */
?>

			<ul class="right_col blog">

<?php
	/* When we call the dynamic_sidebar() function, it'll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn't exist, so we'll hard-code in
	 * some default sidebar stuff just in case.
	 */
if(is_home()) {
	if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>

			<li>
				<h3><?php _e( 'Archives', 'twentyten' ); ?></h3>
				<ul>
					<?php wp_get_archives( 'type=monthly' ); ?>
				</ul>
			</li>

			<li>
				<h3><?php _e( 'Meta', 'twentyten' ); ?></h3>
				<ul>
					<?php wp_register(); ?>
					<li><?php wp_loginout(); ?></li>
					<?php wp_meta(); ?>
				</ul>
			</li>

		<?php endif; // end primary widget area ?>
		<li class="widget-container twitter2">
				<h3>Our Tweets</h3>
		<?php twitter_messages('@xylemwatermark', 1, true, true, '#', true, true, false); ?></li>
<?php } ?>
			</ul>
			
			<ul class="right_col blog">

<?php
	/* When we call the dynamic_sidebar() function, it'll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn't exist, so we'll hard-code in
	 * some default sidebar stuff just in case.
	 */
if(is_single() || is_category()) {
	if ( ! dynamic_sidebar( 'blog-post-sidebar' ) ) : ?>

			<li>
				<h3><?php _e( 'Archives', 'twentyten' ); ?></h3>
				<ul>
					<?php wp_get_archives( 'type=monthly' ); ?>
				</ul>
			</li>

			<li>
				<h3><?php _e( 'Meta', 'twentyten' ); ?></h3>
				<ul>
					<?php wp_register(); ?>
					<li><?php wp_loginout(); ?></li>
					<?php wp_meta(); ?>
				</ul>
			</li>

		<?php endif; } // end primary widget area ?>
		
			</ul>

<?php
	// A second sidebar for widgets, just because.
if(is_page('the-challenge') || is_page('protectwater') || is_page('providewater')) {
	if ( is_active_sidebar( 'secondary-widget-area' ) ) : ?>

			<ul class="right_col challenge">
				<?php dynamic_sidebar( 'secondary-widget-area' ); ?>
			</ul>

<?php endif; } ?>

<?php
if(is_page('our-commitment') || is_subpage() == "27") {
	if ( is_active_sidebar( 'commitment-sidebar' ) ) : ?>

			<ul class="right_col commit">
				<?php dynamic_sidebar( 'commitment-sidebar' ); ?>
				<?php if(is_page('collaboration-is-key') || is_page('partner-video-mercy-corps') || is_page('partner-videos-water-for-people')) { ?>
				<li class="widget-container partner-videos">
				<h3 class="widget-title">Partner Videos</h3>
				<ul>
					<li><a href="/our-commitment/partner-video-mercy-corps/"><img alt="vidthumb_mercy" src="<?php bloginfo('template_url'); ?>/images/vidthumb_mercy.jpg" width="63" height="66"><span>Neal Keny-Guyer, CEO, Mercy Corps</span></a></li>
					<li><a href="/our-commitment/partner-videos-water-for-people/"><img alt="vidthumb_wfp" src="<?php bloginfo('template_url'); ?>/images/vidthumb_wfp.jpg" width="63" height="66"><span>Ned Breslin, CEO, Water For People</span></a></li>
					<div class="clear"></div>
				</ul>
				</li>
				<?php } ?>
				
				<?php if(is_page('its-our-responsibility')) { ?>
				<!-- li class="widget-container partner-videos">
				<h3 class="widget-title">In Her Words</h3>
				<ul>
					<li><a href="/our-commitment/in-her-words/"><img alt="vidthumb_gretchen" src="<?php bloginfo('template_url'); ?>/images/vidthumb_gretchen.jpg" width="63" height="66"><span>Hear Gretchen McClain discuss Xylem's commitment to Watermark</span></a></li>
					<div class="clear"></div>
				</ul>
				</li -->
				<?php } ?>
			</ul>

<?php endif; } ?>

<?php
if(is_page('make-your-mark') || is_subpage() == "30" || is_page('contact') || is_page('champions')) {
	if ( is_active_sidebar( 'mark-sidebar' ) ) : ?>

			<ul class="right_col mark">
				<?php dynamic_sidebar( 'mark-sidebar' ); ?>
			</ul>

<?php endif; } ?>


