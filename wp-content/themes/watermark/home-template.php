<?php
/*
Template Name: Home Page
*/

get_header(); ?>
<!-- <div id="carousel">
<span id="carousel_overlay">
Watermark in Action
</span>
<div class="banners">
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<?php the_content(); ?>
<?php endwhile; ?>
</div>
</div> -->

<div id="video">

<span id="video_overlay">
Watermark in Action
</span>
<div class="videos">
<div id="myElement">Loading the player...</div>
<script type="text/javascript">
					jwplayer("myElement").setup({
					
                    	width: 1000,
                    	height:560,
                        aspectratio: "16:9",
                        autostart: "true",
                        controls: "false",
                        repeat: "true",
                        preload: "auto",
                        playlist: [{
                        image: "/wp-content/uploads/2014/02/video-tile.jpg",
                        sources: [
							{ file: "/wp-content/uploads/2014/02/XYLEM_temp_3_3.webm" },
							{ file: "/wp-content/uploads/2014/02/XYLEM_temp_3_3.mp4" },
							{ file: "/wp-content/uploads/2014/02/XYLEM_temp_3_3.flv" }
								]
                      }]
					});
				</script>


<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<?php the_content(); ?>
<?php endwhile; ?>
</div>
</div>

<?php
switch(ICL_LANGUAGE_CODE) {
	case "zh-hans":
		$lang = "_CHI";
		break;
	case "fr":
		$lang = "_FRA";
		break;
	case "es":
		$lang = "_SPA";
		break;
	case "sv":
		$lang = "_SWE";
		break;
	case "de":
		$lang = "_GER";
		break;
	case "it":
		$lang = "_ITA";
		break;
	default:
		break;
}
?>
<div id="boxes">
<a href="the-challenge"><img alt="The Challenge" src="<?php bloginfo('template_url'); ?>/images/challenge_box<?= $lang ?>.jpg"></a><img src="<?php bloginfo('template_url'); ?>/images/gradient_bar.jpg"><a href="our-commitment"><img alt="Our Commitment" src="<?php bloginfo('template_url'); ?>/images/commitment_box<?= $lang ?>.jpg"></a><img src="<?php bloginfo('template_url'); ?>/images/gradient_bar.jpg"><a href="blog"><img alt="Running Water" src="<?php bloginfo('template_url'); ?>/images/water_box<?= $lang ?>.jpg"></a><img src="<?php bloginfo('template_url'); ?>/images/gradient_bar.jpg"><a href="make-your-mark"><img alt="Make Your Mark" src="<?php bloginfo('template_url'); ?>/images/mark_box<?= $lang ?>.jpg"></a>
<div class="clear"></div>
</div>
<div id="content">
<div id="social_home">
<img alt="more from Watermark" src="<?php bloginfo('template_url'); ?>/images/more_from_watermark.png" width="312" height="24">
<div class="connect">
CONNECT WITH US <a href="http://www.facebook.com/xylemwatermark" target="_blank"><img alt="fb" src="<?php bloginfo('template_url'); ?>/images/fb.png" width="27" height="27"></a> <a href="http://twitter.com/#!/xylemwatermark" target="_blank"><img alt="twitter" src="<?php bloginfo('template_url'); ?>/images/twitter.png" width="27" height="27"></a>
</div>
</div>
<?php
	/* A sidebar in the footer? Yep. You can can customize
	 * your footer with four columns of widgets.
	 */
	get_sidebar( 'footer' );
?>
</div>

<?php get_footer(); ?>