<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */
?>

<div class="clear"></div>
<div id="footer">
<p><a href="http://www.xyleminc.com/" target="_blank">&copy; <?php echo date('Y');?> Xylem Inc.</a></p>

</div>
</div>

<div id="footer_nav_container">
<?php wp_nav_menu( array( 'container_class' => 'footer_nav', 'theme_location' => 'primary' ) ); ?>

<p><a href="http://www.xyleminc.com/en-us/pages/privacy-policy.aspx" target="_blank">Privacy Policy</a>  |  <a href="http://www.xyleminc.com/en-us/pages/terms-and-conditions.aspx" target="_blank">Terms and Conditions</a></p>
<div class="clear"></div>
</div>
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-42508124-1', 'xylemwatermark.com');
  ga('send', 'pageview');
</script>
</body>
</html>