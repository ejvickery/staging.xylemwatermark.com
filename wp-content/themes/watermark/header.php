<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 * We filter the output of wp_title() a bit -- see
	 * twentyten_filter_wp_title() in functions.php.
	 */
	wp_title( '|', true, 'right' );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script> -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url'); ?>/js/modernizr.custom.15590.js" type="text/javascript"></script>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<?php if (ICL_LANGUAGE_CODE == 'zh-hans') { ?>
<?php }
else {	
?>
<link href="<?php bloginfo('template_url'); ?>/css/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
<script src="<?php bloginfo('template_url'); ?>/js/jquery.vmap.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
	<script type="text/JavaScript" src="<?php bloginfo('template_url'); ?>/js/typeface-0.15.js"></script>
	<script type="text/JavaScript" src="<?php bloginfo('template_url'); ?>/js/avenir_book.typeface.js"></script>
	<script type="text/JavaScript" src="<?php bloginfo('template_url'); ?>/js/avenir_heavy.typeface.js"></script>
	
	<!-- Homepage video -->
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jwplayer.js"></script>
	<script type="text/javascript">jwplayer.key="nSzIUWYJK6EN6G/5ofMmAB1BEEL+44I2jjOTRltXWC0=";</script>
	
<?php } ?>	

<script src="<?php bloginfo('template_url'); ?>/js/jquery.carousel.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(function(){
			$("div.banners").carousel( {
			effect: "slide",
			animSpeed: 1000,
			pagination: true,
			nextBtnInsert: "insertBefore",
			autoSlide: true,
			autoSlideInterval: 5000,
			delayAutoSlide: 2000,
			loop: true
			} );
		});
	</script>
<?php $path = get_bloginfo('template_url'); ?>
<?php if(is_page('global-challenge-local-solutions')) : ?>
<!-- <script type="text/javascript">
$.fn.preload = function() {
    this.each(function(){
        $('<img/>')[0].src = this;
    });
}

$(['brazil.jpg','cambodia.jpg','china.jpg','colombia.jpg','congo.jpg','ethiopia.jpg','guatemala.jpg','haiti.jpg','honduras.jpg','india.jpg','indonesia.jpg','japan.jpg','jordan.jpg','kenya.jpg','nepal.jpg','niger.jpg','pakistan.jpg','peru.jpg','phillipines.jpg','sri-lanka.jpg','tajikstan.jpg','yemen.jpg']).preload();
</script> -->	

	<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('#vmap').vectorMap({
		    mmap: 'world_en',
			backgroundColor: null,
			color: '#5466a6',
			hoverColor: "#12A37A",
			selectedColor: '#12A37A',
		    values: sample_data,
		    scaleColors: ['#5466a6', '#9ea3cb'],
			enableZoom: true,
			showTooltip: true,
			normalizeFunction: 'polynomial',
			onRegionClick: function(element, code, region)
			{
				if (code != "br" && code != "co" && code != "in" && code != "gt" && code != "ht" && code != "hn" && code != "pe" && code != "kh" && code != "id" && code != "cn" && code != "jp" && code != "jo" && code != "np" && code != "pk" && code != "ph" && code != "lk" && code != "tj" && code != "ye" && code != "cd" && code != "et" && code != "ke" && code != "ne" && code != "us") {
				event.preventDefault();
				clearSelectedRegions();
				}
				else {
				var region = region.toLowerCase().replace(/ /g,"-");
				//$('.worldPop img').attr('src', '<?php echo $path; ?>/images/countries/'+region+'.png');
				//$('.worldPop img').attr('alt', region);
				$('#overlay').delay(800).fadeIn(200);
				$('.worldPop').fadeOut(200);
				$('.worldPop img').remove();
				$('.worldPop').append('<img width="850" height="369" alt="'+region+'" src="<?php echo $path; ?>/images/countries/'+region+'.png" id="country_info" />');
				switch(region)
				{
				case "brazil":
				  $('.worldPop a').attr('href', 'http://www.avina.net/eng/acciones-por-pais/brasil/');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "cambodia":
				  $('.worldPop a').attr('href', 'http://www.planet-water.org/project.php?id=341');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "china":
				  $('.worldPop a').attr('href', 'http://xylemwatermark.com/2013/02/01/cwdf-nanjing-project/');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "colombia":
				  $('.worldPop a').attr('href', 'http://xylemwatermark.com/2013/03/05/drr-colombia/');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "congo":
				  $('.worldPop a').attr('href', 'http://www.ecbproject.org/goma-joint-emergency-training/goma-joint-emergency-training');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "ethiopia":
				  $('.worldPop a').attr('href', 'http://xylemwatermark.com/2012/11/28/story-of-impact-ethiopia/');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "guatemala":
				  $('.worldPop a').attr('href', 'http://xylemwatermark.com/2013/03/05/bringing-positive-change-for-students-in-las-tunas/');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "haiti":
				  $('.worldPop a').attr('href', 'http://www.mercycorps.org/dansadowsky/blog/18988');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "honduras":
				  $('.worldPop a').attr('href', 'http://xylemwatermark.com/2013/03/05/wfp-vallecito-2010/');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "india":
				  $('.worldPop a').attr('href', 'http://xylemwatermark.com/2012/09/14/driving-real-change/');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "indonesia":
				  $('.worldPop a').attr('href', 'http://xylemwatermark.com/2013/03/05/stories-of-impact-neglected-springs-could-save-lives/');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "japan":
				  $('.worldPop a').attr('href', 'http://www.mercycorps.org/countries/japan/26556');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "jordan":
				  $('.worldPop a').attr('href', 'http://xylemwatermark.com/2013/01/02/refugees-in-jordan/');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "kenya":
				  $('.worldPop a').attr('href', 'http://www.mercycorps.org/rogerburks/blog/25564');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "nepal":
				  $('.worldPop a').attr('href', '•	http://xylemwatermark.com/2012/08/27/story-of-impact-community-womens-groups-in-nepal-champion-sanitation-and-hygiene-improvement/');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "niger":
				  $('.worldPop a').attr('href', 'http://xylemwatermark.com/2013/03/07/mercy-corps-niger-2009/');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "pakistan":
				  $('.worldPop a').attr('href', 'http://www.mercycorps.org/jeffreyshannon/blog/25996');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "peru":
				  $('.worldPop a').attr('href', 'http://www.youtube.com/watch?v=Ta8Ia1FoXrs&list=UUd7w_pTEorFS9aXvqxyG6KA&index=1');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "philippines":
				  $('.worldPop a').attr('href', 'http://www.planet-water.org/project.php?id=23');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "sri-lanka":
				  $('.worldPop a').attr('href', 'http://www.mercycorps.org/paularmour/blog/15841');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "tajikistan":
				  $('.worldPop a').attr('href', 'http://xylemwatermark.com/2013/03/07/mercy-corps-tajikistan/');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "yemen":
				  $('.worldPop a').attr('href', 'http://www.mercycorps.org/victoriastanski/blog/26314');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				case "united-states-of-america":
				  $('.worldPop a').attr('href', 'http://www.godwinpumps.com/index.php/about-us/press-releases/xylem-still-sending-relief-and-donations-to-areas-hit-by-hurricane-sandy');
				  $('.worldPop #map_donate').attr('href', 'make-your-mark/make-a-donation/');
				  break;
				default:
				  $('.worldPop a').attr('href', '#');
				}
				$('.worldPop').delay(800).fadeIn(400);		
				}
			},
			onRegionOver: function (event, code, region) {
			 if (code != "br" && code != "co" && code != "in" && code != "gt" && code != "ht" && code != "hn" && code != "pe" && code != "kh" && code != "id" && code != "cn" && code != "jp" && code != "jo" && code != "np" && code != "pk" && code != "ph" && code != "lk" && code != "tj" && code != "ye" && code != "cd" && code != "et" && code != "ke" && code != "ne" && code != "us") {
			  event.preventDefault();
			 }
			},
			onLabelShow: function(event, label, code) {
			 if (code != "br" && code != "co" && code != "in" && code != "gt" && code != "ht" && code != "hn" && code != "pe" && code != "kh" && code != "id" && code != "cn" && code != "jp" && code != "jo" && code != "np" && code != "pk" && code != "ph" && code != "lk" && code != "tj" && code != "ye" && code != "cd" && code != "et" && code != "ke" && code != "ne" && code != "us") {
			  event.preventDefault();
			  $('.jvectormap-region').css( 'cursor', 'default' );
			 }
			 else {
			  $('.jvectormap-region').css( 'cursor', 'pointer' );
			 }
			}
		});
		
			
			$('.worldPop .close').click(function(event){
				event.preventDefault();
				$('#overlay').fadeOut(200);
				$('.worldPop').fadeOut(200);
				});
	});
	</script>
	<?php endif; ?>
</head>

<body <?php body_class(); ?>>
<div id="container">
<div id="header">
	<h1>
		<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
	</h1>
	<p class="description"><?php bloginfo( 'description' ); ?></p>
	<div id="search">
	<?php get_search_form(); ?>
	</div>
	<?php wp_nav_menu( array( 'container_class' => 'nav typeface-js', 'theme_location' => 'primary' ) ); ?>
</div>
<div id="languages"><?php do_action('icl_language_selector'); ?></div>
	<div id="access" role="navigation">
	  <?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
		<a href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentyten' ); ?>"><?php _e( 'Skip to content', 'twentyten' ); ?></a>
		<?php /* Our navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
		<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
	</div><!-- #access -->
<?php	
if(!is_front_page()) {
wp_nav_menu( array( 'container_class' => 'interior_nav typeface-js', 'theme_location' => 'interior' ) );
}
?>