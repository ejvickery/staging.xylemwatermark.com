<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query. 
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>
<div id="content">

			<div class="left_col">
			<h3>Recent Blog Posts</h3>
			<div class="divider"></div>
			<?php
			/* Run the loop to output the posts.
			 * If you want to overload this in a child theme then include a file
			 * called loop-index.php and that will be used instead.
			 */
			 get_template_part( 'loop', 'index' );
			?>
			</div>

<?php get_sidebar(); ?>
</div>
<div class="connect_footer">
<div class="connect_tagline">
CONNECT WITH US
</div>
<ul>
	<li><a href="http://www.facebook.com/xylemwatermark" target="_blank"><img alt="facebook" src="<?php bloginfo('template_url'); ?>/images/fb.png" width="27" height="27"></a></li>
	<li><a href="http://twitter.com/#!/xylemwatermark" target="_blank"><img alt="twitter" src="<?php bloginfo('template_url'); ?>/images/twitter.png" width="27" height="27"></a></li>
</ul>
</div>
<?php get_footer(); ?>