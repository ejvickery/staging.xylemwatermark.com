<?php
/*
Template Name: Logged-In Users Page
*/
?>
<?php
if(is_user_logged_in()):
get_header();
?>

<div id="content">
<div class="left_col">
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

					<?php if ( is_front_page() ) { ?>
						<!-- <h2><?php the_title(); ?></h2> -->
					<?php } else { ?>	
						<!-- <h1><?php the_title(); ?></h1> -->
					<?php } ?>				

						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'twentyten' ), 'after' => '' ) ); ?>


<?php endwhile; ?>
</div>


<?php get_sidebar(); ?>
</div>
<div class="connect_footer">
<div class="connect_tagline">
CONNECT WITH US
</div>
<ul>
	<li><a href="http://www.facebook.com/xylemwatermark" target="_blank"><img alt="facebook" src="<?php bloginfo('template_url'); ?>/images/fb.png" width="27" height="27"></a></li>
	<li><a href="http://twitter.com/#!/xylemwatermark" target="_blank"><img alt="twitter" src="<?php bloginfo('template_url'); ?>/images/twitter.png" width="27" height="27"></a></li>
</ul>
</div>
<?php get_footer(); ?>
<?php else:
wp_die('<h3 style="text-align: center; font-size: 20px; line-height: 32px;">Sorry, you must first <a href="/wp-login.php?redirect_to=/make-your-mark/volunteer-form/">log in</a> to fill out the volunteer application. <br /><span style="font-weight: normal;">If you are a Xylem employee, you can <a href="/wp-login.php?action=register">register here</a>.</span></h3>');
endif;?>