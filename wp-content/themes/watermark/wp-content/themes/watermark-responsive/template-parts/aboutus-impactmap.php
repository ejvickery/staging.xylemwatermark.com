<!-- aboutus-impactmap.php -->

	<?php $themeLink = get_bloginfo('template_url'); ?>

  		<div id="section-map">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h1>Xylem Watermark is making a <strong>DIFFERENCE</strong></h1>
						<p>Explore the Xylem Watermark social impact map to see how we are helping to solve water around the world</p>
						<a href="#" class="btn-map">Explore Map</a>
					</div>
				</div>
			</div>
		</div>


<!-- End aboutus-impactmap.php -->