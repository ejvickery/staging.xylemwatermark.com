<!-- mission-hero.php -->

	<?php $themeLink = get_bloginfo('template_url'); ?>

  		<div id="about-section2">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h1><strong>OUR MISSION</strong> is to provide and protect safe water resources for communities in need around the world.</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 centerup sideborders">
						<img src="<?php print $themeLink; ?>/images/about-community.png">
					</div>
					<div class="col-md-4 centerup">
						<img src="<?php print $themeLink; ?>/images/about-community.png">
					</div>
					<div class="col-md-4 centerup sideborders">
						<img src="<?php print $themeLink; ?>/images/about-community.png">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p>At Xylem, we are united in our focus to be the leading global provider of efficient and sustainable water technologies and applications. We will meet the most critical water challenges head on by engineering products that create efficient systems and sustainable solutions. Put simply, Xylem is working to solve water.</p>
						<a href="#" class="btn-vid">Watch the Video</a>
					</div>
				</div>
			</div>
		</div>


<!-- End mission-hero.php -->