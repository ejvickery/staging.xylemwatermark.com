<!-- aboutus-commitment.php -->

	<?php $themeLink = get_bloginfo('template_url'); ?>

    <div id="about-section4">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h1>Our <strong>COMMITMENT</strong></h1>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 timeline-box">
						<div class="timeline-primary">
							<h1>MORE THAN <span>2.3 MILLION</span> PEOPLE REACHED.</h1>
							<h2><span>5</span> partners.</h2>
							<h2><span>6</span> years.</h2>
							<h2><span>25</span> countries.</h2>
							<h2>More than <span>600</span> local water projects for communities in need.</h2>
						</div>
						<div class="timeline-year">
							<h1>2013</h1>
							<p><strong>DECEMBER:</strong> Watermark completes its second 3-year commitment by reaching more than 1.8 million people since 2010.</p>
							<p><strong>MARCH:</strong> Xylem Watermark innovates reporting process with launch of interactive social impact map.</p>
						</div>
						<div class="timeline-year">
							<h1>2012</h1>
							<p><strong>DECEMBER:</strong> Xylem and Mercy Corps complete Distaster Rish Reduction Initiative - Water, reaching more than 950,000 people with water-related distaster preparation and training.</p>
							<p><strong>JUNE:</strong> Xylem Watermark recognized with President’s Award for Excellence in Corporate Philanthropy from the Commitee Encouraging Corporate Philanthropy.</p>
						</div>
					</div>
				</div>
			</div>
		</div>


<!-- End aboutus-commitment.php -->