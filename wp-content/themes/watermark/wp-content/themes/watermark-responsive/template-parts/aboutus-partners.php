<!-- aboutus-partners.php -->

	<?php $themeLink = get_bloginfo('template_url'); ?>

  		<div id="about-section3">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h1>Our Nonprofit <strong>PARTNERS</strong></h1>
						<p>Through Xylem Watermark, Xylem works with best-in-class nonprofits to address the full spectrum of water challenges by providing financial support, water technology, sanitation equipment and hygiene education. Xylem Watermark solutions are truly sustainable, combining community-based interventions with regular monitoring to ensure projects meet local water needs for years to come.</p>
					</div>
				</div>

				<div class="row bord-bottom">
					<div class="col-md-6 bord-right">
						<div class="partner-box-main">
							box
						</div>
					</div>
					<div class="col-md-6">
						<div class="partner-box">
							<table>
								<tr>
									<td class="partner-logo">
										<img src="<?php print $themeLink; ?>/images/about-partner-WFP.png">
									</td>
									<td class="partner-cnt">
										Providing clean water and WASH education to schools and communities in India and Peru
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="row bord-bottom">
					<div class="col-md-6 bord-right">
						<div class="partner-box">
							<table>
								<tr>
									<td class="partner-logo">
										<img src="<?php print $themeLink; ?>/images/about-partner-PLANET.png">
									</td>
									<td class="partner-cnt">
										Building innovative water towers to provide clean water and WASH education to schools and communities in South East Asia
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="col-md-6">
						<div class="partner-box">
							<table>
								<tr>
									<td class="partner-logo">
										<img src="<?php print $themeLink; ?>/images/about-partner-MERCY.png">
									</td>
									<td class="partner-cnt">
										Protecting clean water through disaster risk reduction and responding to water-related emergencies around the world
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 bord-right">
						<div class="partner-box">
							<table>
								<tr>
									<td class="partner-logo">
										<img src="<?php print $themeLink; ?>/images/about-partner-AVINA.png">
									</td>
									<td class="partner-cnt">
										Building water cisterns to provide clean water and WASH education to schools and communities in Brazil’s Semiarid
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="col-md-6">
						<div class="partner-box">
							<table>
								<tr>
									<td class="partner-logo">
										<img src="<?php print $themeLink; ?>/images/about-partner-CHINA.png">
									</td>
									<td class="partner-cnt">
										Providing schools throughout China with improved sanitation and clean water sources
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- END about section 3 -->
<!-- End aboutus-partners.php -->