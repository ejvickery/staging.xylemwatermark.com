<?php
/*
Template Name: posts
*/
?>
<?php while ( have_posts() ) : the_post() ?>
<div id="post-<?php the_ID() ?>" class="post">
<?php the_content(); ?>
<?php endforeach; endif;?>
</div><!-- .post -->
<?php comments_template() ?>
<?php endwhile; ?>