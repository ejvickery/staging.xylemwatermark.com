<?php
/**
 * Template Name: Impact Map
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0//get_template_part('template-parts/map', 'header');?>
 */
get_header();?>
<div class="outerlimits">
	<?php get_template_part('template-parts/page', 'nav'); ?>
	<?php get_template_part('template-parts/map', 'map'); ?>
  <?php get_template_part('template-parts/page', 'contact'); ?>
<?php get_footer(); ?>