<?php
/**
 * The template for displaying Home page.
 *
 * Template Name: makeyourmark
 *
 * @package WordPress
 * @subpackage Xylem Watermark
 * @since Xylem Watermark 2.0
 */
get_header();?>
<div class="outerlimits">
<?php get_template_part('template-parts/page', 'nav'); ?>
<?php get_template_part('template-parts/makeyourmark', 'hero'); ?>
<?php get_template_part('template-parts/makeyourmark', 'join'); ?>
<?php get_template_part('template-parts/makeyourmark', 'donation'); ?>
<?php get_template_part('template-parts/makeyourmark', 'trip'); ?>
<?php get_template_part('template-parts/makeyourmark', 'spreadtheword'); ?>


	<?php if (have_posts()) : while (have_posts()) : the_post();?>

	<?php the_content(); ?>

	<?php endwhile; endif; ?>

<?php //get_sidebar(); ?>
<?php get_template_part('template-parts/makeyourmark', 'runningwater'); ?>
<?php get_template_part('template-parts/page', 'contact'); ?>
<?php get_footer(); ?>