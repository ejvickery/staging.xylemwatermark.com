<?php
 
if ( function_exists('register_sidebar') )
register_sidebar(array(
'before_widget' => '',
'after_widget' => '',
'before_title' => '<h2>',
'after_title' => '</h2>',
));
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'custom-header' );


function remove_more_link_scroll( $link ) {
	$link = preg_replace( '|#more-[0-9]+|', '', $link );
	return $link;
}
add_filter( 'the_content_more_link', 'remove_more_link_scroll' );


// Retuns first word in string as all lower case
function cs_trimTitle($fulltitle){

  $titleParts = explode("-", $fulltitle);

  return $trimedTitle = strtolower(trim($titleParts[0]));

}

/**************************************
Get Embed Video for post format video
**************************************/
function the_featured_video( &$content ) {
 $url = trim( array_shift( explode( "\n", $content ) ) );
 $w = get_option( 'embed_size_w' );
 if ( !is_single() )
 $url = str_replace( '448', $w, $url );
if ( 0 === strpos( $url, 'http://' ) ) {
 echo apply_filters( 'the_content', $url );
 $content = trim( str_replace( $url, '', $content ) ); 
 } else if ( preg_match ( '#^<(script|iframe|embed|object)#i', $url ) ) {
 $h = get_option( 'embed_size_h' );
 if ( !empty( $h ) ) {
 if ( $w === $h ) $h = ceil( $w * 0.75 );
$url = preg_replace( 
 array( '#height="[0-9]+?"#i', '#height=[0-9]+?#i' ), 
 array( sprintf( 'height="%d"', $h ), sprintf( 'height=%d', $h ) ), 
 $url 
 );
 }
echo $url;
 $content = trim( str_replace( $url, '', $content ) ); 
 }
}


function custom_pagination($numpages = '', $pagerange = '', $paged='') {
 
  if (empty($pagerange)) {
    $pagerange = 2;
  }
 
  /**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   * 
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }


  /** 
   * We construct the pagination arguments to enter into our paginate_links
   * function. 
   */
  $pagination_args = array(
    'base'            => get_pagenum_link(1) . '%_%',
    'format'          => 'page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('&laquo;'),
    'next_text'       => __('&raquo;'),
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);
  
  if ($paginate_links) {
	  echo "<div class='pagination-wrap'>";
    echo "<nav class='custom-pagination'>";
      echo $paginate_links;
    echo "</nav>";
	echo "</div>";
  }
 
}


function get_columns_array($totalCount, $columnSize) {
	$columns = array();
	$totalCount = (int) max(0, $totalCount);
	if (!$count)
		return $columns;	
	$columnSize = (int) max(0, $columnSize);
	if (!$columnSize)
		return $columns;
	($floor = (int) ($totalCount / $columnSize))
                && $columns = array_fill(0, $floor, $columnSize)
                ;
	($remainder = $totalCount % $columnSize)
		&& $columns[] = $remainder
		;
	return $columns;
}
 
/**
 * WP_Query_Columns
 *
 * Columns for the loop. 
 *
 * Copyright (c) 2011 hakre <http://hakre.wordpress.com/>, some rights reserved 
 * 
 * @author hakre <http://hakre.wordpress.com/>
 * @see http://wordpress.stackexchange.com/q/9308/178
 */
class WP_Query_Columns implements Countable, IteratorAggregate {
	/**
	 * column size
	 * @var int
	 */
	private $size;
	private $index = 0;
	private $query;
	public function __construct(WP_Query $query, $size = 10) {
		$this->query = $query;
		$this->size = (int) max(0, $size);
	}
	/**
	 * @return WP_Query
	 */
	public function query() {
		return $this->query;
	}
	private function fragmentCount($fragmentSize, $totalSize) {
		$total = (int) $totalSize;
		$size = (int) max(0, $fragmentSize);
		if (!$total || !$size)
			return 0;
		$count = (int) ($total / $size);
		$count * $size != $total && $count++;				
		return $count;
	}
	private function fragmentSize($index, $fragmentSize, $totalSize) {
		$index = (int) max(0, $index);
		if (!$index)
			return 0;
		$count = $this->fragmentCount($fragmentSize, $totalSize);
		if ($index > $count)
			return 0;
		return $index === $count ? ($totalSize - ($count-1) * $fragmentSize) : $fragmentSize;			
	}
	public function columnSize($index) {
		return $this->fragmentSize($index, $this->size, $this->query->post_count);
	}
	/**
	 * @see Countable::count()
	 * @return int number of columns
	 */
	public function count() {
		return $this->fragmentCount($this->size, $this->query->post_count);
	}
	/**
	 * @return array
	 */
	public function columns() {
		$count = $this->count();
		$array = $count ? range(1, $count) : array();
		return array_map(array($this, 'columnSize'), $array);
	}
	/**
	 * @see IteratorAggregate::getIterator()
	 * @return traversable columns
	 */
	public function getIterator() {
		return new ArrayIterator($this->columns());
	}
}


function get_excerpt_by_id($post_id){
    $the_post = get_post($post_id); //Gets post ID
    $the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
    $excerpt_length = 55; //Sets excerpt length by word count
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);

    if(count($words) > $excerpt_length) :
        array_pop($words);
        array_push($words, ' ...');
        $the_excerpt = implode(' ', $words);
    endif;

    $the_excerpt = '<p>' . $the_excerpt . '</p>';

    return $the_excerpt;
}


function checkLeftOrRight($isLeftOrRight){

  if ($isLeftOrRight == true){

    $isLeftOrRight = false;
  }else{

    $isLeftOrRight = true;
  }

  return $isLeftOrRight;

}

function checkIfFirst($isfirst){

  if ($isfirst == true){

    $postClasses = '<div class="blog_post first_post">';

  }else{

    $postClasses = '<div class="blog_post">';
  }
  return $postClasses;

}





function getPostType($postType){

  if($postType == ''){

    $postType = "text_type";
  }


  $sendPostType = '<div class="clear"></div>
                   <div class="post_type '.$postType.'"></div>';

  return $sendPostType;


}


function getPostTypeTeaser($postTypeTeaser){

  if($postTypeTeaser == ''){

    $postTypeTeaser = "news-title";
  }

  return $postTypeTeaser;

}

function getPostColClass($classIndex){

  if($classIndex == 0){

    $thePostColClass = "col-sm-4";
  }else{

    $thePostColClass = "col-sm-4  hidden-xs";
  }

  return $thePostColClass;

}



function cleanThemePageName($themePageFullName) {
	//$themePageName = str_replace('.php', '',$themePageName);

	$themePageName = preg_replace("/\\.[^.\\s]{3,4}$/", "", $themePageFullName);
	//echo '<!-- '.$themePageName.' -->';
	return $themePageName;
}

function setThemePageNav($themePageName) {

  switch (cleanThemePageName($themePageName)){
  //switch ($themePageName){

    case 'front-page':
      $themePagenav = "front";
      break;
    case 'crisis':
      $themePagenav = "crisis";
      break;
    case 'challenge':
      $themePagenav = "challenge";
      break;
    case 'aboutus':
      $themePagenav = "aboutus";
      break;
    case 'makeyourmark':
      $themePagenav = "makeyourmark";
      break;
    case 'waternews':
      $themePagenav = "waternews";
      break;
    case 'runningwater':
      $themePagenav = "runningwater";
      break;
    default:
      $themePagenav = "page";
  }

	return $themePagenav;
}

function addCycle($themePageName) {

  switch (cleanThemePageName($themePageName)){
  //switch ($themePageName){

    case 'front-page':
    case 'crisis':
    case 'challenge':
      $themeLink = get_template_directory_uri();

      print "<!-- Cylcle2 -->
    <script type='text/javascript' src='".$themeLink."/js/jquery.cycle2.js'></script>
    <script type='text/javascript' src='".$themeLink."/js/jquery.cycle2.center.js'></script>
    <script type='text/javascript' src='".$themeLink."/js/jquery.cycle2.caption2.js'></script>
    <script type='text/javascript' src='".$themeLink."/js/jquery.cycle2.carousel.js'></script>
    <script type='text/javascript' src='".$themeLink."/js/jquery.cycle2.swipe.js'></script>
    <script type='text/javascript' src='".$themeLink."/js/jquery.cycle2.ie-fade.js'></script>
    ";
      break;

    default:

  }

}


function addForPosts($themePageName) {

  switch (cleanThemePageName($themePageName)){
  //switch ($themePageName){

    case 'runningwater':
    case 'waternews':
      $themeLink = get_template_directory_uri();

      print "<!-- Posts -->
    <link href='".$themeLink."/css/plugins.css' rel='stylesheet'>
    <link href='".$themeLink."/css/animate.css' rel='stylesheet'>
    <link href='".$themeLink."/css/bstyle.css' rel='stylesheet'>
    <link href='".$themeLink."/css/responsive.css' rel='stylesheet'>
    <link href='".$themeLink."/css/jplayer/jplayer.css' rel='stylesheet'>


    <script type='text/javascript' src='".$themeLink."/js/jquery.isotope.min.js'></script>
    <script type='text/javascript' src='".$themeLink."/js/animate.js'></script>
    <script type='text/javascript' src='".$themeLink."/js/sorting.js'></script>
    <script type='text/javascript' src='".$themeLink."/js/modernizr.custom.js'></script>
    <script type='text/javascript' src='".$themeLink."/js/current.js'></script>
    <script type='text/javascript' src='".$themeLink."/js/jquery.cycle2.ie-fade.js'></script>
    ";
      break;

    default:
$themeLink = get_template_directory_uri();
          print "<!-- Posts -->
    <link href='".$themeLink."/css/plugins.css' rel='stylesheet'>
    <link href='".$themeLink."/css/animate.css' rel='stylesheet'>
    <link href='".$themeLink."/css/responsive.css' rel='stylesheet'>
    <link href='".$themeLink."/css/jplayer/jplayer.css' rel='stylesheet'>


    <script type='text/javascript' src='".$themeLink."/js/jquery.isotope.min.js'></script>
    <script type='text/javascript' src='".$themeLink."/js/animate.js'></script>
    <script type='text/javascript' src='".$themeLink."/js/sorting.js'></script>
    <script type='text/javascript' src='".$themeLink."/js/modernizr.custom.js'></script>
    <script type='text/javascript' src='".$themeLink."/js/current.js'></script>
    <script type='text/javascript' src='".$themeLink."/js/jquery.cycle2.ie-fade.js'></script>
    ";


  }


}

function getPaginationPage($postOffset, $postOrNews){

   $urlpage = site_url()."/".$postOrNews;


   switch ($postOffset) {

    case '0':

      $paginationPage = "<li><span>1</span></li>
                         <li><a href='".$urlpage."/?posts=8'>2</a></li>
                         <li><a href='".$urlpage."/?posts=16'>3</a></li>
                         <li><a href='".$urlpage."/?posts=24'>4</a></li>
                         <li><a href='".$urlpage."/?posts=32'>5</a></li>";
      break;

    case '8':

      $paginationPage = "<li><a href='".$urlpage."/?posts=0'>1</a></li>
                         <li><span>2</span></li>
                         <li><a href='".$urlpage."/?posts=16'>3</a></li>
                         <li><a href='".$urlpage."/?posts=24'>4</a></li>
                         <li><a href='".$urlpage."/?posts=32'>5</a></li>";
      break;

    case '16':

      $paginationPage = "<li><a href='".$urlpage."/?posts=0'>1</a></li>
                         <li><a href='".$urlpage."/?posts=8'>2</a></li>
                         <li><span>3</span></li>
                         <li><a href='".$urlpage."/?posts=24'>4</a></li>
                         <li><a href='".$urlpage."/?posts=32'>5</a></li>";
      break;

    case '24':

      $paginationPage = "<li><a href='".$urlpage."/?posts=0'>1</a></li>
                         <li><a href='".$urlpage."/?posts=8'>2</a></li>
                         <li><a href='".$urlpage."/?posts=16'>3</a></li>
                         <li><span>4</span></li>
                         <li><a href='".$urlpage."/?posts=32'>5</a></li>";
      break;

    case '32':

      $paginationPage = "<li><a href='".$urlpage."/?posts=0'>1</a></li>
                         <li><a href='".$urlpage."/?posts=8'>2</a></li>
                         <li><a href='".$urlpage."/?posts=16'>3</a></li>
                         <li><a href='".$urlpage."/?posts=24'>4</a></li>
                         <li><span>5</span></li>";
      break;

    /*case '40':

      $paginationPage = "<li><a href='".$urlpage."/?posts=0'>1</a></li>
                         <li><a href='".$urlpage."/?posts=8'>2</a></li>
                         <li><a href='".$urlpage."/?posts=16'>3</a></li>
                         <li><a href='".$urlpage."/?posts=24'>4</a></li>
                         <li><a href='".$urlpage."/?posts=32'>5</a></li>
                         <li><span>6</span></li>
                         <li><a href='".$urlpage."/?posts=48'>7</a></li>
                         <li><a href='".$urlpage."/?posts=56'>8</a></li>";
      break;

    case '48':

      $paginationPage = "<li><a href='".$urlpage."/?posts=0'>1</a></li>
                         <li><a href='".$urlpage."/?posts=8'>2</a></li>
                         <li><a href='".$urlpage."/?posts=16'>3</a></li>
                         <li><a href='".$urlpage."/?posts=24'>4</a></li>
                         <li><a href='".$urlpage."/?posts=32'>5</a></li>
                         <li><a href='".$urlpage."/?posts=40'>6</a></li>
                         <li><span>7</span></li>
                         <li><a href='".$urlpage."/?posts=56'>8</a></li>";
      break;

     case '56':

      $paginationPage = "<li><a href='".$urlpage."/?posts=0'>1</a></li>
                         <li><a href='".$urlpage."/?posts=8'>2</a></li>
                         <li><a href='".$urlpage."/?posts=16'>3</a></li>
                         <li><a href='".$urlpage."/?posts=24'>4</a></li>
                         <li><a href='".$urlpage."/?posts=32'>5</a></li>
                         <li><a href='".$urlpage."/?posts=40'>6</a></li>
                         <li><a href='".$urlpage."/?posts=48'>7</a></li>
                         <li><span>8</span></li>";
      break; */

      default:
      $paginationPage = "<li><a href='".$urlpage."/?posts=0'>1</a></li>
                         <li><a href='".$urlpage."/?posts=8'>2</a></li>
                         <li><a href='".$urlpage."/?posts=16'>3</a></li>
                         <li><a href='".$urlpage."/?posts=24'>4</a></li>
                         <li><a href='".$urlpage."/?posts=32'>5</a></li>
                         ";

    }



  return $paginationPage;

  }



add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );
