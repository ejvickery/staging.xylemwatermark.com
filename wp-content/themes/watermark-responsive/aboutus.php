<?php
/**
 * The template for displaying Home page.
 *
 * Template Name: aboutus
 *
 * @package WordPress
 * @subpackage Xylem Watermark
 * @since Xylem Watermark 2.0
 */
get_header();?>
<?php get_template_part('template-parts/page', 'nav'); ?>
<div class="outerlimits">
<?php get_template_part('template-parts/aboutus', 'hero'); ?>
<?php get_template_part('template-parts/aboutus', 'mission'); ?>
<?php get_template_part('template-parts/aboutus', 'partners'); ?>
<?php get_template_part('template-parts/section', 'impactmap'); ?>
<?php get_template_part('template-parts/aboutus', 'commitment'); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post();?>

	<?php the_content(); ?>

	<?php endwhile; endif; ?>

<?php //get_sidebar(); ?>
<?php get_template_part('template-parts/page', 'contact'); ?>
<?php get_footer(); ?>