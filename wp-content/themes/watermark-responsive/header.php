<?php
/**
 * The Header for our theme.
 *
 * @package WordPress
 * @subpackage Xylem Watermark
 * @since Xylem Watermark 2.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Xylem Watermark <?php wp_title('|'); ?></title>

    <?php
    $themeLink = get_template_directory_uri();
    $templateName = get_page_template_slug();
    $thisThemePageNav = setThemePageNav($templateName)
    ?>

    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="shortcut icon" href="<?php print $themeLink ?>/images/favicon.ico" type="image/vnd.microsoft.icon"/>
    <link rel="icon" href="<?php print $themeLink ?>/images/favicon.png" type="image/x-ico"/>

    <link href="<?php print $themeLink ?>/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php print $themeLink ?>/css/owl.theme.css" rel="stylesheet">
    <link href="<?php print $themeLink ?>/css/magnific-popup.css" rel="stylesheet">
    <link href="<?php print $themeLink ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php print $themeLink ?>/css/styles.css" rel="stylesheet">
    <link href="<?php print $themeLink ?>/style.css" rel="stylesheet">

    <?php wp_head(); ?>
    <script type="text/javascript" src="<?php print $themeLink ?>/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php print $themeLink ?>/js/jquery-scrollto.js"></script>
    <script type="text/javascript" src="<?php print $themeLink ?>/js/skrollr.min.js"></script>
    <script type="text/javascript" src="<?php print $themeLink ?>/js/jquery.magnific-popup.js"></script>
    <script type="text/javascript" src="<?php print $themeLink ?>/js/jwplayer.js"></script>
	<script type="text/javascript">jwplayer.key="nSzIUWYJK6EN6G/5ofMmAB1BEEL+44I2jjOTRltXWC0=";</script>
	  
	  <?php addCycle($templateName); ?>
	  <?php addForPosts($templateName); ?>

    <link href="<?php print $themeLink ?>/js/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php print $themeLink ?>/js/jqvmap/jquery.vmap.js"></script>
    <script type="text/javascript" src="<?php print $themeLink ?>/js/jqvmap/maps/jquery.vmap.world.js"></script>
    <script type="text/javascript" src="<?php print $themeLink ?>/js/jqvmap/data/jquery.vmap.sampledata.js"></script>
    <script type="text/javascript" src="<?php print $themeLink ?>/js/owl.carousel.js"></script>
    <script type="text/javascript" src="<?php print $themeLink ?>/js/watermark.js"></script>
		<script type="text/javascript" src="http://fast.fonts.net/jsapi/ed20b8ca-d7b0-420e-b131-341974e2f41c.js"></script>
    <!-- <script type="text/javascript" src="<?php print $themeLink ?>/js/<?php print $thisThemePageNav ?>scroll.js"></script> -->

   <!-- <script type="text/javascript" src="<?php print $themeLink ?>/js/testbox.js"></script> -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <script type="text/javascript" src="<?php print $themeLink ?>/js/skrollr.ie.min.js"></script>
    <![endif]-->


  </head>

  <body>

  <!-- End header.php -->
