<?php $themeLink = get_template_directory_uri(); ?>

<?php
/**
 * The template for displaying Category pages.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header();?>

<?php get_template_part('template-parts/page', 'nav'); ?>

<style>

.flexthumb, .flexslider {
display: inline;
width: 200px!important;
height: 150px!important;
padding-right: 2%;
padding-left: 0!important;
overflow:hidden;
/* margin: 0!important; */
/* padding: 0!important; */
}

.flexthumb iframe  {
	display: inline;
width: 187px!important;
height: 125px!important;
}

</style>

<div class="outerlimits">
	<div>
		<img src="<?php print $themeLink ?>/images/GENERIC_header.jpg" alt="" class="img-responsive"/>
	</div>

	<div id="content" class="category_content_section">
				
		<div class="container">

		<div class="row">
			<div class="col-md-7">

				<h1 class="category_title">Tag: <strong><?php printf( __( '%s', 'twentyten' ), '' . single_cat_title( '', false ) . '' ); ?></strong></h1>
            				
<?php if (have_posts()) : while (have_posts()) : the_post();?>

				<div class="individual-archive">
					<?php if ( get_post_meta(get_the_ID(), 'video', true)) { ?>
     
     
     <div class="flexthumb col-sm-12 col-xs-12 col-md-5 col-lg-5 col-xl-5">
        <?php echo get_post_meta( get_the_ID(), 'video', true ); ?>
       </div> 
       
       
       <?php
} elseif ( get_post_meta(get_the_ID(), 'gallery', true)) {
    ?>
            
            <?php 

$images = get_field('gallery');

if( $images ): ?>
    <div id="slider" class="flexslider col-sm-12 col-xs-12 col-md-5 col-lg-5 col-xl-5">
        <ul class="slides">
            <?php foreach( $images as $image ): ?>
                <li>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <p><?php echo $image['caption']; ?></p>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>
            

       <?php
} else {
    ?>
    
            
      <div class="flexthumb col-sm-12 col-xs-12 col-md-5 col-lg-5 col-xl-5"> 
              <a href="<?php the_permalink(); ?>"><?php get_the_image( array('size' => 'medium-16x9', 'thumbnail_id_save' => true, 'link_to_post' => false, width => '100%', 'image_scan' => true, 'order_of_image' => 1)); ?></a>
        </div>
    

            
        <?php } ?>

<div class="row">
					<div class="post-descrip col-sm-12 col-xs-12 col-md-7 col-lg-7 col-xl-7">		
								<div class="individual-archive-title">
									<a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
								</div>
								<?php the_excerpt(); ?>
				
			   </div>
                </div>
                </div>
								 
				<?php endwhile; endif; ?>

			</div><!-- /.col-xs-7 -->

			<div class="col-md-4 col-md-offset-1">
				<div class="single-post-cats individual-archive-tag-spacer">
					<h5>Categories</h5>
   <?php
$args = array(
  'orderby' => 'name',
  'order' => 'ASC'
  );
$categories = get_categories($args);
  foreach($categories as $category) {
    echo '<a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a>';
    }
?>
			</div><!-- END categories -->
			<div class="clearfix"></div>

			<div class="single-post-tags">
				<h5>Tags</h5>
   <?php
$tags = get_tags();
$categories = get_categories($args);
  foreach($tags as $tag) {
    echo '<a href="' . get_tag_link( $tag->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $tag->name ) . '" ' . '>' . $tag->name.'</a> </span> ';
    }
?>
			</div><!-- END tags -->
			 <br class="clear-post-float" />

			</div><!-- /.col-sm-4 .col-sm-offset-1 -->
		</div> <!-- END row -->

		</div><!-- /.container -->



<?php get_template_part('template-parts/page', 'contact'); ?>
<?php get_footer(); ?>