<?php get_header(); ?>

<?php get_template_part('template-parts/page', 'nav'); ?>
<div class="outerlimits">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<?php
if (in_category('News')){

	$section = 'waternews';
	$postTeaser = 'newsfeatured';

} else {

	$section = 'runningwater';
	$postTeaser = 'postsfeatured';


}

//echo '<!-- This Post is: '.$section. ' -->';

 if($section == ''){
   $section = 'waternews';
 }
get_template_part("template-parts/$section", "hero");

?>

<div class="container" style="margin-bottom: 60px;">
	<div class="row">
		<div class="col-sm-7">
			<div class="post-content">

			<?php //if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div <?php  post_class() ?> id="post-<?php the_ID(); ?>">
			<?php
									$postType = get_field('post_type');

									 if($postType == "image_type"){

											$featureImageLink = get_field('feature_image_link');

											print '<div class="post-featured-image"><img src="'.$featureImageLink.'"/></div>';

									 }
			              if($postType == "video_type"){

                        $featureVideoLink = get_field('video');

                          print '<div id="post-list-video"></div>';

                         print '<script>
                                 jwplayer("post-list-video").setup({
                                   file: "'.$featureVideoLink.'",
                                   width: "100%",
                                   aspectratio: "16:9"
                                 });
                                </script>';

             }

            if($postType == "photoset_type"):

             $images = get_field('gallery');

             if( $images ): ?>
             <div id="slider" class="flexslider">
               <ul class="slides">
            <?php foreach( $images as $image ): ?>
                <li>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <p><?php echo $image['caption']; ?></p>
                </li>
            <?php endforeach; ?>
            </ul>
            </div>

            <?php endif; ?>

            <?php endif; ?>

						<div id="post-title">
							<div class="post-title-text"><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></div>
							<div class="post-social-btns"><?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { ADDTOANY_SHARE_SAVE_KIT(); } ?></div>
							<div class="clear-post-float"></div>
						</div>

						<br class="clear-post-float" />

						<div class="postbody">
							<?php the_content(); ?>

							<?php //wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
						</div>

						<div id="postmeta"><?php echo get_the_date(); ?> / by <?php the_author_posts_link(); ?> in <?php echo the_category(', '); ?> / tags: <?php echo the_tags(' '); ?></div>

						<div id="postnavbar" class="postnav">
							<div id="postprev"><?php previous_post_link('%link', 'Previous Post '.$postnavlinkname, TRUE); ?></div>
							<div id="postnext"><?php next_post_link('%link', 'Next Post'.$postnavlinkname, TRUE); ?></div>
						</div>


					</div>
			 </div><!-- END post-content -->
		</div>



		<div class="col-sm-4 col-sm-offset-1">
			<div class="post-authorbar">
				<h5>Author</h5>
				<div class="author typeface-js">
					<div class="author_image"><?php the_author_image(); ?></div>
					<div class="author_name"><?php the_author_posts_link(); ?></div>
					<div class="author_title"><?php the_author_meta('job_title'); ?></div>
				</div>
			<div class="clearfix"></div>
			</div><!-- END post-authorbar -->

			<div class="single-post-cats">
				<h5>Categories</h5>
   <?php
$args = array(
  'orderby' => 'name',
  'order' => 'ASC'
  );
$categories = get_categories($args);
  foreach($categories as $category) {
    echo '<a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a>';
    }
?>
			</div>
			<div class="clearfix"></div>
			<div class="single-post-tags">
				<h5>Tags</h5>
   <?php
$tags = get_tags();
$categories = get_categories($args);
  foreach($tags as $tag) {
    echo '<a href="' . get_tag_link( $tag->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $tag->name ) . '" ' . '>' . $tag->name.'</a> </span> ';
    }
?>
			</div>
		</div>

 <br class="clear-post-float" />

	<?php //comments_template(); ?>

	<?php endwhile; endif; ?>

 </div><!-- END of row div -->
</div><!-- END of container div -->

<?php
 if($postTeaser == ''){
   $postTeaser = 'newsfeatured';
 }


get_template_part('template-parts/posts', $postTeaser); ?>
<?php get_template_part('template-parts/page', 'contact'); ?>
<?php get_footer(); ?>