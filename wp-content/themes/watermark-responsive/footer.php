<!-- footer.php -->

	<?php $themeLink = get_bloginfo('template_url');
  	    $url = site_url();
	?>

		<div id="footer">
			<div class="container">
				<div class="row hidden-xs">
					<div class="col-sm-3 centerup rightborder">
						<a href="<?php print $url ?>/the-challenge/" class="footerlink1">The Challenge</a><br>
						<a rel="m_PageScroll2id" href="<?php print $url ?>/the-challenge/#crisis-section2">Know The Facts</a><br>
						<a rel="m_PageScroll2id" href="<?php print $url ?>/the-challenge/#crisis-section3">The Global Water Crisis</a><br>
						.<br>.
					</div>
					<div class="col-sm-3 centerup rightborder">
						<a href="<?php print $url ?>/about-us/" class="footerlink1">About Us</a><br>
						<a rel="m_PageScroll2id" href="<?php print $url ?>/about-us/#about-section2">Our Mission</a><br>
						<a rel="m_PageScroll2id" href="<?php print $url ?>/about-us/#about-section3">Our Partners</a><br>
						<a rel="m_PageScroll2id" href="<?php print $url ?>/about-us/#section-map">Our Impact</a><br>
						<a rel="m_PageScroll2id" href="<?php print $url ?>/about-us/#about-section4">Our Commitment</a>
					</div>
					<div class="col-sm-3 centerup rightborder">
						<a href="<?php print $url ?>/make-your-mark/" class="footerlink1">Make Your Mark</a><br>
						<a rel="m_PageScroll2id" href="<?php print $url ?>/make-your-mark/#section-wtrmrk-community">Join The Watermark Community</a><br>
						<a rel="m_PageScroll2id" href="<?php print $url ?>/make-your-mark/#section-make-donation">Make A Donation</a><br>
						<a rel="m_PageScroll2id" href="<?php print $url ?>/make-your-mark/#section-take-trip">Take A Trip</a><br>
						<a rel="m_PageScroll2id" href="<?php print $url ?>/make-your-mark/#section-spread-the-word">Spread The Word</a><br>
						.
					</div>
					<div class="col-sm-3 centerup">
						<a href="<?php print $url ?>/water-news/" class="footerlink1">Water News</a><br>
						<a href="<?php print $url ?>/running-water/" class="footerlink1">Running Water</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 footerdots">
						<div class="terms">
							<a href="http://www.xyleminc.com/en-us/pages/privacy-policy.aspx" target="_blank">Privacy Policy</a>
							<a href="http://www.xyleminc.com/en-us/pages/terms-and-conditions.aspx" target="_blank">Terms and Conditions</a>
							<a href="http://www.xyleminc.com/" target="_blank">Xylem Inc.</a>
						</div>
						<div class="copyright">&copy; <?php print date("Y");?> Xylem, Inc.</div>
					</div>
				</div>
			</div>
    </div>
    </div>
	</div><!-- END outerlimits -->

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-XXXXX-X']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


<?php wp_footer(); ?>
  </body>
</html>


