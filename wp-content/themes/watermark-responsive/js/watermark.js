/*!
 * JQuery xylem-watermark-script - Version: 20140409.01.0
 * Copyright (c) 2014 Andrew Hubbell
 * Requires: JQuery v1.11.0 or later
 */
$(document).ready(function(){

$('.menu-item-has-children', '.navbar').hover(
  function() {
    $(this).css('background', 'url(/wp-content/themes/watermark-responsive/images/menu-carret.19x11.png) no-repeat center bottom');
    $(this).children(':first').css('color', '#fff');
    $('.sub-menu', this).slideDown(200);
  }, function() {
    $('.sub-menu', this).slideUp(200);
    $(this).css('background', 'none');
    $(this).children(':first').css('color', '#a6e5f7');
  }
);

$('.mobile-menu').click(function(){
  $('.navbar-mobile').slideToggle();
  return false;
});

$('.mobile-menu-close').click(function(){
  $('.navbar-mobile').slideToggle();
  return false;
});

function navReset() {
  $('.navbar-mobile').slideUp();     
}

navReset()
$(window).resize(navReset)

//var s = skrollr.init();

if(!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)){
    var s = skrollr.init({
        forceHeight: false
    });
}

    // Popups


   $('#factoverlay01btn').magnificPopup({
        type: 'inline',

          fixedContentPos: false,
          fixedBgPos: true,

          overflowY: 'auto',

          closeBtnInside: true,
          preloader: false,

          midClick: true,
    });


   $('#vid-btn-cont').magnificPopup({
        delegate: 'a',
        type: 'iframe',
        iframe: {
  markup: '<div class="mfp-iframe-scaler video-modal">'+
            '<div class="mfp-close"></div>'+
            '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
          '</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button
          }

    });

   $('.btn-vid').magnificPopup({
        type: 'iframe',
        iframe: {
  markup: '<div class="mfp-iframe-scaler video-modal">'+
            '<div class="mfp-close"></div>'+
            '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
          '</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button
          }

    });
    


    $('#impactmapbtn').magnificPopup({
        type: 'inline',
          fixedContentPos: false,
          fixedBgPos: true,
          overflowY: 'auto',
          closeBtnInside: true,
          preloader: false,
          midClick: true,
    });

// Challenge links

   $('.challenge-popup').magnificPopup({
        type: 'ajax',

          fixedContentPos: 'auto',
          fixedBgPos: true,
          closeOnContentClick: false,
          showCloseBtn: true,
          overflowY: 'auto',
          closeBtnInside: true,
          preloader: false,



    });

// Mission links

   $('.mission-box').magnificPopup({
        delegate: 'a',
        type: 'ajax',

          fixedContentPos: 'auto',
          fixedBgPos: true,
          closeOnContentClick: false,
          showCloseBtn: true,

          overflowY: 'auto',

          closeBtnInside: true,
          preloader: false,
    });



// Partner links

   $('a.partner-link').magnificPopup({
        type: 'ajax',

          fixedContentPos: 'auto',
          fixedBgPos: true,
          closeOnContentClick: false,
          showCloseBtn: true,
          overflowY: 'auto',
          closeBtnInside: true,
          preloader: false,
    });

// Partner links

   $('.current-trip-popup').magnificPopup({
        delegate: 'a',
        type: 'ajax',

          fixedContentPos: 'auto',
          fixedBgPos: true,
          closeOnContentClick: false,
          showCloseBtn: true,
          overflowY: 'auto',
          closeBtnInside: true,
          preloader: false,
    });


// Make your mark browse all links

   $('.browse-all-popup').magnificPopup({
        type: 'ajax',

          fixedContentPos: 'auto',
          fixedBgPos: true,
          closeOnContentClick: false,
          showCloseBtn: true,
          overflowY: 'auto',
          closeBtnInside: true,
          preloader: false,
    });

$('.refugee-day-open-popup').magnificPopup({
  type:'inline',
  midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
});


// owl carousels
$("#challenge-facts").owlCarousel({
    items : 3,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [979,3],
      navigation:true,
    navigationText: [
          "<img src='../wp-content/themes/watermark-responsive/images/left-arrow.20x34.png'/>",
      "<img src='../wp-content/themes/watermark-responsive/images/right-arrow.20x34.png'/>"
      ],
     // Responsive
     responsive: true,
     responsiveRefreshRate : 200,
     responsiveBaseWidth: window,
  });

$("#resource-carousel").owlCarousel({
    items:3,
      itemsCustom : false,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [980,3],
    itemsTablet: [768,2],
    itemsTabletSmall: false,
    itemsMobile : [479,1],
    singleItem : false,
    itemsScaleUp : false,
      pagination : false,
      navigation:true,
    navigationText: [
          "<img src='../wp-content/themes/watermark-responsive/images/btn-carousel-L.png'/>",
      "<img src='../wp-content/themes/watermark-responsive/images/btn-carousel-R.png'/>"
      ],
      // Responsive
      responsive: true,
      responsiveRefreshRate : 200,
      responsiveBaseWidth: window,
  });

$("#section-spread-the-word .owl-theme .owl-controls").append('<span class="resource-carousel-instruction">Scroll to view full listing</span>');

$("#commitment-carousel").owlCarousel({
    items:3,
      itemsCustom : false,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [980,3],
    itemsTablet: [768,2],
    itemsTabletSmall: false,
    itemsMobile : [479,1],
    singleItem : false,
    itemsScaleUp : false,
      pagination : false,
      navigation:true,
    navigationText: [
          "<img src='../wp-content/themes/watermark-responsive/images/btn-carousel-L.png'/>",
      "<img src='../wp-content/themes/watermark-responsive/images/btn-carousel-R.png'/>"
      ],
      // Responsive
      responsive: true,
      responsiveRefreshRate : 200,
      responsiveBaseWidth: window,
  });

$("#about-section4 .owl-theme .owl-controls").append('<span class="resource-carousel-instruction">Scroll to view full timeline</span>');

    // Map

    $('#vmap').vectorMap({
      mmap: 'world_en',
			backgroundColor: null,
			color: '#5466a6',
			hoverColor: "#12A37A",
			selectedColor: '#12A37A',
		    values: sample_data,
		    scaleColors: ['#5466a6', '#9ea3cb'],
			enableZoom: true,
			showTooltip: true,
			normalizeFunction: 'polynomial',
			onRegionClick: function(element, code, region)
      {
        if (code != "br" && code != "co" && code != "in" && code != "gt" && code != "ht" && code != "hn" && code != "pe" && code != "kh" && code != "id" && code != "cn" && code != "jp" && code != "jo" && code != "np" && code != "pk" && code != "ph" && code != "lk" && code != "tj" && code != "ye" && code != "cd" && code != "et" && code != "ke" && code != "ne" && code != "us") {
        event.preventDefault();
        clearSelectedRegions();
        }
        else {
        var region = region.toLowerCase().replace(/ /g,"-");
        //$('.worldPop img').attr('src', 'http://staging.xylemwatermark.com/wp-content/themes/watermark/images/countries/'+region+'.png');
        //$('.worldPop img').attr('alt', region);
        $('#overlay').delay(800).fadeIn(200);
        $('.worldPop').fadeOut(200);
        $('.worldPop img').remove();
        $('.worldPop').append('<img width="850" height="369" alt="'+region+'" src="http://staging.xylemwatermark.com/wp-content/themes/watermark/images/countries/'+region+'.png" id="country_info" />');
        switch(region)
        {
        case "brazil":
          $('.worldPop a').attr('href', 'http://www.avina.net/eng/acciones-por-pais/brasil/');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "cambodia":
          $('.worldPop a').attr('href', 'http://www.planet-water.org/project.php?id=341');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "china":
          $('.worldPop a').attr('href', 'http://www.mothercellar.cn/html/mothercellar/report/145205-0.htm');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "colombia":
          $('.worldPop a').attr('href', '2013/03/05/drr-colombia/');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "congo":
          $('.worldPop a').attr('href', '../about-us/#about-section3');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "ethiopia":
          $('.worldPop a').attr('href', '2012/11/28/story-of-impact-after-change-of-heart-community-leader-takes-action/');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "guatemala":
          $('.worldPop a').attr('href', '2013/03/05/bringing-positive-change-for-students-in-las-tunas/');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "haiti":
          $('.worldPop a').attr('href', 'http://www.mercycorps.org/dansadowsky/blog/18988');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "honduras":
          $('.worldPop a').attr('href', '2014/05/15/health-and-education-in-vallecito-2/');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "india":
          $('.worldPop a').attr('href', '2012/09/14/driving-real-change/');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "indonesia":
          $('.worldPop a').attr('href', '2013/03/05/stories-of-impact-neglected-springs-could-save-lives/');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "japan":
          $('.worldPop a').attr('href', 'http://www.mercycorps.org/countries/japan/26556');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "jordan":
          $('.worldPop a').attr('href', '2013/01/02/refugees-in-jordan/');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "kenya":
          $('.worldPop a').attr('href', 'http://www.mercycorps.org/rogerburks/blog/25564');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "nepal":
          $('.worldPop a').attr('href', '2012/08/27/story-of-impact-community-womens-groups-in-nepal-champion-sanitation-and-hygiene-improvement/');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "niger":
          $('.worldPop a').attr('href', '2013/03/07/mercy-corps-niger-2009/');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "pakistan":
          $('.worldPop a').attr('href', 'http://www.mercycorps.org/jeffreyshannon/blog/25996');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "peru":
          $('.worldPop a').attr('href', 'http://www.youtube.com/watch?v=Ta8Ia1FoXrs&list=UUd7w_pTEorFS9aXvqxyG6KA&index=1');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "philippines":
          $('.worldPop a').attr('href', 'http://www.planet-water.org/project.php?id=23');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "sri-lanka":
          $('.worldPop a').attr('href', 'http://www.mercycorps.org/paularmour/blog/15841');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "tajikistan":
          $('.worldPop a').attr('href', '2013/03/07/mercy-corps-tajikistan/');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "yemen":
          $('.worldPop a').attr('href', 'http://www.mercycorps.org/victoriastanski/blog/26314');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        case "united-states-of-america":
          $('.worldPop a').attr('href', 'http://www.godwinpumps.com/index.php/about-us/press-releases/xylem-still-sending-relief-and-donations-to-areas-hit-by-hurricane-sandy');
          $('.worldPop #map_donate').attr('href', 'make-your-mark/#section-make-donation');
          break;
        default:
          $('.worldPop a').attr('href', '#');
        }
        $('.worldPop').delay(800).fadeIn(400);
        }
      },
      onRegionOver: function (event, code, region) {
       if (code != "br" && code != "co" && code != "in" && code != "gt" && code != "ht" && code != "hn" && code != "pe" && code != "kh" && code != "id" && code != "cn" && code != "jp" && code != "jo" && code != "np" && code != "pk" && code != "ph" && code != "lk" && code != "tj" && code != "ye" && code != "cd" && code != "et" && code != "ke" && code != "ne" && code != "us") {
        event.preventDefault();
       }
      },
      onLabelShow: function(event, label, code) {
       if (code != "br" && code != "co" && code != "in" && code != "gt" && code != "ht" && code != "hn" && code != "pe" && code != "kh" && code != "id" && code != "cn" && code != "jp" && code != "jo" && code != "np" && code != "pk" && code != "ph" && code != "lk" && code != "tj" && code != "ye" && code != "cd" && code != "et" && code != "ke" && code != "ne" && code != "us") {
        event.preventDefault();
        $('.jvectormap-region').css( 'cursor', 'default' );
       }
       else {
        $('.jvectormap-region').css( 'cursor', 'pointer' );
       }
      }
    });


      $('.worldPop .close').click(function(event){
        event.preventDefault();
        $('#overlay').fadeOut(200);
        $('.worldPop').fadeOut(200);
        });



/* END Document.ready */ });
