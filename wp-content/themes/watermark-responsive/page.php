<?php
/**
 * The template for displaying page.
 *
 *
 * @package WordPress
 * @subpackage Xylem Watermark
 * @since Xylem Watermark 2.0
 */
get_header();?>

<?php get_template_part('template-parts/page', 'nav'); ?>

<div class="outerlimits">

<?php get_template_part('template-parts/page', 'hero'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<div class="dotted">

<?php the_content(); ?>

</div>

<?php endwhile; endif; ?>

<?php //get_sidebar(); ?>
<?php get_template_part('template-parts/page', 'makingadifference'); ?>
<?php get_template_part('template-parts/page', 'contact'); ?>



<?php get_footer(); ?>


