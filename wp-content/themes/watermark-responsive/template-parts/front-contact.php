<!-- front-contact.php -->

		<div id="contact">
			<div class="container">
  <?php

    $pageSectionID = get_page_by_title( 'Contact section' );
    $pageSection = get_post($pageSectionID->ID, ARRAY_A);
    print $pageSection['post_content'];

  ?>
 </div>
 </div>

<?php print '<!-- End front-contact - pageID: '.$pageSectionID->ID.' -->'; ?>