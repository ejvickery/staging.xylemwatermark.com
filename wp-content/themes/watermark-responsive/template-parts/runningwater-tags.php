<!-- waternews-press.php -->

	<?php $themeLink = get_bloginfo('template_url'); ?>

		<div id="section-news-feature-tags">
			 <div class="container">

<div class="col-ms-2">
<div><h4>Blog Categories</h4></div>

<div class="row">
<?php
              $args = array(
               'orderby' => 'name',
               'order' => 'ASC'
              );
              $html = '';
              $categories = get_categories($args);

               foreach($categories as $category) {

                $category_link = get_category_link($category->term_id);

                $html .= "<a href='{$category_link}' title='{$category->name}'>";
                $html .= "{$category->name}</a>";
                }
                $html .= '';
                echo $html;
             ?>
</div><!-- /.row -->
</div> <!-- /.col-ms-2 -->


<br/>
<br/>
<br/>



<div class="col-ms-2">
<div><h4>Blog Tags</h4></div>
<div class="row">
<?php
                $tags = get_tags();
                $html = '';
                foreach ( $tags as $tag ) {
              	  $tag_link = get_tag_link($tag->term_id);

                  $html .= "<a href='{$tag_link}' title='{$tag->name} Tag'>";
                  $html .= "{$tag->name}</a>";
                  }
                  $html .= '';
                  echo $html;
             ?>
</div><!-- /.row -->
</div> <!-- /.col-ms-2 -->


			 </div> <!-- /.container -->
		</div><!-- #section-news-feature-tags -->

<!-- End waternews-press.php -->