<!-- waternews-hero.php -->

	<?php $themeLink = get_bloginfo('template_url'); ?>

  <div class="news-hero">
  		<div class="hero-text-container">
			<p class="hero-main">Water News</p>
			<p class="hero-sub">Insights, trends, updates, and announcements that are making headlines and making an impact on global water issues.</p>
		</div>
	</div>

<!-- End waternews-hero.php -->