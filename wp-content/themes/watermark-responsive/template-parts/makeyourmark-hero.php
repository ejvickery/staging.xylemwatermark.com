<!-- makeyourmark-hero.php -->

  <?php

    $pageSectionID = get_page_by_title( 'Make your mark hero section' );
    $pageSection = get_post($pageSectionID->ID, ARRAY_A);
    print $pageSection['post_content'];

  ?>

<?php print '<!-- End makeyourmark-hero.php - pageID: '.$pageSectionID->ID.' -->'; ?>
