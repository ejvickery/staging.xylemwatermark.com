<!-- makeyourmark-join.php -->
	<?php $themeLink = get_bloginfo('template_url');
  	    $url = site_url();
	?>
		<div id="section-wtrmrk-community">
			<div class="container">
  <?php

    //$pageSectionID = get_page_by_title( 'Make your mark join section' );
    //$pageSection = get_post($pageSectionID->ID, ARRAY_A);
    //print $pageSection['post_content'];

  ?>

  <div class="row">
<div class="col-xs-12">
<h1>Join the <strong>WATERMARK COMMUNITY</strong></h1>
</div>
</div>
<div class="row">
<div class="col-sm-7" style="text-align: left;">
<p class="subhead"><em>My Watermark</em> is Xylem's online community that enables Xylem employees to support Xylem Watermark and our nonprofit partners.</p>

<!--<img class="img-responsive col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6" src="<?php print $themeLink ?>/images/givethegiftlogo.png" alt="" />
<p class="givethegift col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">

This holiday season bring the gift of clean water to communities, schools and families in need. Login to <a href="https://mywatermark.benevity.org/campaigns" target="_blank"><span style="font-family: 'Avenir Next LT W01 Demi', Arial, sans-serif;font-weight:bold;">My Watermark</span></a> to make a contribution today!
</p>

<br/><br/>-->

<!--<p style="border-top: 1px dotted #ccc;
display: inline-block;
margin-top: 3%;
padding-top: 3%;">-->

<p>It's a place where employees from around the world can come together to see how individually, and together, they can make every drop count in solving critical water challenges.</p>

<p>Through <em>My Watermark</em>, employees can:</p>
<ul>
	<li><strong>Connect</strong> with colleagues to see how Xylem locations are supporting Xylem Watermark</li>
	<li><strong>Donate</strong> to support Xylem Watermark's nonprofit partners</li>
	<li><strong>Find</strong> volunteer opportunities to support water-related projects in their local communities</li>
	<li><strong>Track</strong> their personal impact on the global water crisis</li>
</ul>
</div>
<div class="col-sm-5 centerup"><img src="<?php print $themeLink ?>/images/community-img1.png" alt="" /></div>
</div>
<div class="row">
<div class="col-xs-12 centerup"><a class="btn-login" href="https://mywatermark.benevity.org/" target="_blank"></a></div>
<p>For questions about My Watermark, contact Michael Fields, Director of Corporate Citizenship, at <a href="mailto:michaelc.fields@xyleminc.com">michaelc.fields@xyleminc.com</a>.</p>
</div>




			</div>
		</div>

<?php //print '<!-- End makeyourmark-join.php - pageID: '.$pageSectionID->ID.' -->'; ?>

<!-- End makeyourmark-join.php -->