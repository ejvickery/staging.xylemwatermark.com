<!-- makeyourmark-donation.php -->
	<?php $themeLink = get_bloginfo('template_url');
  	    $url = site_url();
	?>
		<div id="section-make-donation">
			<div class="wave-BG">
				<div class="container">
  <?php

   // $pageSectionID = get_page_by_title( 'Make your mark donation section' );
   // $pageSection = get_post($pageSectionID->ID, ARRAY_A);
   // print $pageSection['post_content'];

  ?>

  <div class="row">
<div class="col-sm-12">
<h1>Make a <strong>DONATION</strong></h1>
<h4>Support the Xylem Watermark Fund and our five nonprofit partners</h4>
</div>
</div>
<div class="row">
<div class="col-xs-12" style="padding-bottom: 40px;">
<img src="<?php print $themeLink ?>/images/btn-partner-water4people.png">
<img src="<?php print $themeLink ?>/images/btn-partner-mercycorps.png">
<img src="<?php print $themeLink ?>/images/btn-partner-CWDF.png">
<img src="<?php print $themeLink ?>/images/btn-partner-planetwater.png">
<img src="<?php print $themeLink ?>/images/btn-partner-avina.png">
</div>
</div>
<div class="row">
<div class="col-xs-12">
<p>Through Xylem's Employee Matching Gifts Program, any Xylem employee is eligible to donate to our five nonprofit partners, and Xylem will match contributions up to $5,000 USD per employee, per year.</p>
<p>In recent years, employees have supported local water projects from India to Peru and the Philippines to Honduras. In addition, Xylem frequently offers a double match of employee contributions to support emergency response efforts of water-related disasters. Our employees have rallied to support small business owners in Japan, families in Haiti, and our neighbors in New York City - just a few miles from Xylem headquarters - among many others.</p>
</div>
</div>
<div class="row">
<div class="col-xs-12 donation-separator">
<p><strong>ALREADY REGISTERED on <em>My Watermark</em>?</strong> - <a href="https://mywatermark.benevity.org/" target="_blank">Click here to log in</a> and make your mark.</p>
<p><strong>Need to register on <em>My Watermark</em>?</strong> - Contact Michael Fields, Director of Corporate Citizenship, Xylem Inc. at <a href="mailto:michaelc.fields@xyleminc.com">michaelc.fields@xyleminc.com</a>.</p>
<p class="small-text">At this time, Xylem Watermark only accepts contributions from Xylem employees. To learn more about how you can support the program and our nonprofit partners if you are not an employee, visit <a rel="m_PageScroll2id" href="<?php print $url ?>/about-us/#about-section3">Our Nonprofit Partners</a> or <a href="<?php print $url ?>/about-us/">About Us</a>, or email Michael Fields, Director of Corporate Citizenship, Xylem Inc. at <a href="mailto:michaelc.fields@xyleminc.com">michaelc.fields@xyleminc.com</a>.</p>
</div>
<div class="row">
<div class="col-xs-12">
<a href="<?php print $url ?>/wp-content/uploads/2013/07/Matching-Gifts-Check-Donation-Form_UPDATED_031513.pdf" class="btn-donate" target="_blank">How to Donate by Check</a>
</div>
</div>
</div>





       </div>
			</div>
		</div>

<?php //print '<!-- End makeyourmark-donation.php - pageID: '.$pageSectionID->ID.' -->'; ?>

<!-- End makeyourmark-donation.php -->