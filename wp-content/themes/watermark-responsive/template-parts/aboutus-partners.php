<!-- aboutus-partners.php -->
	<?php $themeLink = get_bloginfo('template_url');
  	    $url = site_url();
	?>

     <div id="about-section3">
			<div class="container">
			 <div class="row">
        <div class="col-xs-12">
  <?php

    $pageSectionID = get_page_by_title( 'Our partners section' );
    $pageSection = get_post($pageSectionID->ID, ARRAY_A);
    print $pageSection['post_content'];

  ?>

    </div>
    </div>
      <div class="row bord-bottom">
          <div class="col-md-6 bord-right">
              <div class="partner-box-main">
                  <table>
                  	<tr>
                  		<td>SEE HOW XYLEM WATERMARK WORKS WITH EACH OF OUR NONPROFIT PARTNERS</td>
                  	</tr>
                  </table>
              </div>
          </div>

          <div class="col-md-6">
              <div class="partner-box">
                  <a class="partner-link" href="<?php print $url ?>/partner-water-for-people/"></a>

                  <div class="clearfix"></div>

                  <table>
                      <tr>
                          <td class="partner-logo"><a href="https://www.waterforpeople.org/" target="_blank"><img src="<?php print $themeLink ?>/images/about-partner-WFP.png" class="partner-grab"></a></td>

                          <td class="partner-cnt">Providing clean water and WASH education to schools and communities in India and Peru.</td>
                      </tr>
                  </table>
              </div>
          </div>
      </div>

      <div class="row bord-bottom">
          <div class="col-md-6 bord-right">
              <div class="partner-box">
                  <a class="partner-link" href="<?php print $url ?>/partner-planet-water/"></a>

                  <table>
                      <tr>
                          <td class="partner-logo"><a href="http://planet-water.org/" target="_blank"><img src="<?php print $themeLink ?>/images/about-partner-PLANET.png"></a></td>

                          <td class="partner-cnt">Building innovative water towers to provide clean water and WASH education to schools and communities in South East Asia.</td>
                      </tr>
                  </table>
              </div>
          </div>

          <div class="col-md-6">
              <div class="partner-box">
                  <a class="partner-link" href="<?php print $url ?>/partner-mercy-corps/"></a>

                  <table>
                      <tr>
                          <td class="partner-logo"><a href="http://www.mercycorps.org/" target="_blank"><img src="<?php print $themeLink ?>/images/about-partner-MERCY.png"></a></td>

                          <td class="partner-cnt">Protecting clean water through disaster risk reduction and responding to water-related emergencies around the world.</td>
                      </tr>
                  </table>
              </div>
          </div>
      </div>

      <div class="row">
          <div class="col-md-6 bord-right">
              <div class="partner-box">
                  <a class="partner-link" href="<?php print $url ?>/partner-avina/"></a>

                  <table>
                      <tr>
                          <td class="partner-logo"><a href="http://www.avina.net/eng/" target="_blank"><img src="<?php print $themeLink ?>/images/about-partner-AVINA.png"></a></td>

                          <td class="partner-cnt">Building water cisterns to provide clean water and WASH education to schools and communities in Brazil’s Semiarid region.</td>
                      </tr>
                  </table>
              </div>
          </div>

          <div class="col-md-6">
              <div class="partner-box">
                  <a class="partner-link" href="<?php print $url ?>/partner-china-womens-development-foundation/"></a>

                  <table>
                      <tr>
                          <td class="partner-logo"><a href="http://www.cwdf.org.cn/engilsh/index.asp" target="_blank"><img src="<?php print $themeLink ?>/images/about-partner-china.png"></a></td>

                          <td class="partner-cnt">Providing schools throughout China with improved sanitation and clean water sources.</td>
                      </tr>
                  </table>
              </div>
          </div>
      </div>

    </div>
   </div>

<?php print '<!-- End aboutus-partners.php - pageID: '.$pageSectionID->ID.' -->'; ?>