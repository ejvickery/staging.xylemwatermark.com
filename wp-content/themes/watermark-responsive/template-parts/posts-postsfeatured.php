<!-- posts-featured.php -->
	<?php $themeLink = get_bloginfo('template_url');
  	    $url = site_url();
	?>
		<div class="featured-news-blog">

				<div class="row">
						<div class="col-xs-12 centerup">
								<h1>Featured <strong>POSTS</strong></h1>
						</div>
				</div>

				<div class="row">
					<div class="col-xs-10 col-xs-offset-1">
							<div class="row">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
								<?php
									$postOffset = $_REQUEST['posts'];

									if($postOffset == ''){
										$postOffset = 0;
									}

									$classIndex = 0;
                  $videoFPID = 1;
									$postlist = get_posts("category_name=running-water&posts_per_page=3&offset=$postOffset");
									if ($postlist):

									foreach ($postlist as $post) :

									setup_postdata($post);



									$thePostColClass = getPostColClass($classIndex);
									$classIndex = $classIndex  = 1;
								?>

								<div class="<?php echo $thePostColClass ?>">
								 <div class="featured-posts">
				<?php
								 $postType = get_field('post_type');


						     if($postType == ''){
                   $postType = 'text_type';
                 }

								 if($postType == 'image_type'){

										$featureImageLink = get_field('feature_image_link');

										 print '<img src="'.$featureImageLink.'"/><br>';

								 }

             if($postType == "video_type"){

                $featureVideoLink = get_field('video');

                print '<div id="post-list-video'.$videoFPID.'"></div>';

                print '<script>
                      jwplayer("post-list-video'.$videoFPID.'").setup({
                          file: "'.$featureVideoLink.'",
                          width: "100%",
                          aspectratio: "16:9"
                        });
                    </script>';


                    $videoFPID += 1;


             }



            if($postType == "photoset_type"):

             $images = get_field('gallery');

             if( $images ): ?>
             <div id="slider" class="flexslider">
               <ul class="slides">
            <?php foreach( $images as $image ): ?>
                <li>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <p><?php echo $image['caption']; ?></p>
                </li>
            <?php endforeach; ?>
            </ul>
            </div>

            <?php endif; ?>

            <?php endif; ?>

						<?php $thePostTypeTeaser = getPostTypeTeaser($postType);?>

								<a href="<?php echo get_permalink(); ?>" class="<?php echo $thePostTypeTeaser ?>"><?php echo get_the_title(); ?></a>
								<?php
								 print  the_excerpt(); ?>

								 <br />
								 <p class="news-more"><?php echo get_the_date(); ?> / <a href="<?php echo get_permalink(); ?>"> Read More...</a></p>

										</div>
									</div>
				<?php

				endforeach;

				endif;
				 ?>


				 <?php endwhile; else: ?>
         <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
         <?php endif; ?>



							</div>
					</div>

					<?php

						 // $pageSectionID = get_page_by_title( 'Crisis waternews section' );
						 // $pageSection = get_post($pageSectionID->ID, ARRAY_A);
						 // print $pageSection['post_content'];

						?>
			</div>
			<div class="centerup"><a href="<?php print $url ?>/running-water">Read more Running Water Posts</a></div>
		</div>

    <!-- End posts-featured.php -->
