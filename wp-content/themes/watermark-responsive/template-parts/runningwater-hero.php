<!-- runningwater-hero.php -->

	<?php $themeLink = get_bloginfo('template_url'); ?>

  <div class="runningwater-hero">
		<div class="hero-text-container">
			<p class="hero-main">Running Water</p>
			<p class="hero-sub">Thoughts on the global water crisis and updates from Xylem and its partners in the field.</p>
		</div>
  </div>


<!-- End runningwater-hero.php -->