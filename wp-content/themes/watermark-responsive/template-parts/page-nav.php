<!-- front-nav.php -->
<?php $themeLink = get_bloginfo('template_url');
	    $url = site_url();
?>
<!-- Fixed navbar -->

<div id="header">
  <div id="header-inner">
    <a class="logo" href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo-header.png"></a>
    <div class="navbar" role="navigation">
      <?php	
        wp_nav_menu( array( 'container_class' => '', 'theme_location' => 'primary_nav' ) );
      ?>
    </div>
    <div class="mobile-menu"></div>
    <div class="social-nav">
      <a href="http://www.facebook.com/xylemwatermark" id="social-FB" target="_blank" >Facebook</a>
      <a href="https://twitter.com/xylemwatermark" id="social-TW" target="_blank">Twitter</a>
      <a href="https://www.youtube.com/user/XylemWatermark" id="social-YT" target="_blank">YouTube</a>
    </div>
  </div>
  <div class="arc" data-0="top:-3px;" data-112="top:-3px;" data-550="top:-145px;" data-smooth-scrolling="on">
    <img src="<?php bloginfo('template_url'); ?>/images/spacer.png" width="100%" height="256" />
  </div>
  <div class="navbar-mobile" role="navigation">
      <?php 
        wp_nav_menu( array( 'container_class' => '', 'theme_location' => 'primary_nav', 'depth' => '1' ) );
      ?>
      <div class="social-nav-mobile">
      <a href="http://www.facebook.com/xylemwatermark" class="social-FB" target="_blank" >Facebook</a>
      <a href="https://twitter.com/xylemwatermark" class="social-TW" target="_blank">Twitter</a>
      <a href="https://www.youtube.com/user/XylemWatermark" class="social-YT" target="_blank">YouTube</a>
    </div>
    <a href="#" class="mobile-menu-close">Close</a>
  </div>
</div>

<div id="skrollr-body"></div>

<!-- End front-nav -->