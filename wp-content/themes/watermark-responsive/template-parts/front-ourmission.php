<!-- front-ourmission.php -->
		<div id="section2" class="container">

  <?php

    $pageSectionID = get_page_by_title( 'Front Our Mission section' );
    $pageSection = get_post($pageSectionID->ID, ARRAY_A);
    print $pageSection['post_content'];

  ?>
		</div>

<?php print '<!-- End front-ourmission.php - pageID: '.$pageSectionID->ID.' -->'; ?>

