<!-- makeyourmark-spreadtheword.php -->

  		<div id="section-spread-the-word">
			<div class="container">
  <?php

    $pageSectionID = get_page_by_title( 'Make your mark spreadtheword section' );
    $pageSection = get_post($pageSectionID->ID, ARRAY_A);
    print $pageSection['post_content'];

  ?>

			</div>
		</div>

<?php print '<!-- End makeyourmark-spreadtheword.php - pageID: '.$pageSectionID->ID.' -->'; ?>