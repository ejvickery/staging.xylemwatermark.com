<!-- section-impactmap.php -->

 <div id="section-map">
 <div class="container">
  <?php

    $pageSectionID = get_page_by_title( 'Impact map section' );
    $pageSection = get_post($pageSectionID->ID, ARRAY_A);
    print $pageSection['post_content'];

  ?>
 </div>


<div id="map-overlay" class="impactmap-popup mfp-hide">
<div class="map-modal-title">Xylem Watermark <strong>IMPACT MAP</strong></div>
  <div id="vmap" style="width: 950px; height: 475px;">

  <p class="zoom_text">Zoom Map</p>

  <div class="worldPop"><a class="close" href="#">Close</a>
  <a href="#" id="learn_more" target="_blank">Learn More</a>
  <a href="#" id="map_donate" target="_blank">Learn More</a>
  </div>
</div>
<div id="map_photo_credit"><em>All images courtesy Xylem Watermark nonprofit partners and Reuters/Nicky Loh - courtesy <a href="http://www.alertnet.org" target="_blank">www.alertnet.org</a> (Indonesia).</em></div>

 </div>

 </div>
<?php print '<!-- End section-impactmap - pageID: '.$pageSectionID->ID.' -->'; ?>