<!-- crisis-challenge.php -->

	<?php $themeLink = get_bloginfo('template_url');
	  	    $url = site_url();
	?>
		<div id="crisis-section3">
			<div class="wave-BG">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<h1>The Global Water <strong>CRISIS</strong></h1>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<h3>The world is facing unprecedented water challenges brought on by population growth, urbanization and shrinking fresh water supplies, meaning women can spend up to six hours per day fetching water for their families.</h3>
							<h2>WATER SCARCITY</h2>
							<p>The photo at right was taken by a Xylem employee who traveled to West Bengal, India as part of a Xylem Watermark global volunteer trip. It demonstrates the power of water, sanitation and hygiene to change lives.</p>
						</div>
						<div class="col-sm-6 centerup">
							<a href="<?php print $url ?>/crisis-scarcity/" class='challenge-popup'><img src="<?php print $themeLink ?>/images/crisis-infographic1.png"></a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 col-sm-push-6">
							<h2>WATER-RELATED DISASTERS</h2>
							<p>This image was taken in Jakarta, Indonesia after a massive flood, capturing the devastating effects of water-related disasters. Disaster risk reduction offers a new approach to solving the global water crisis by proactively securing water resources before natural disasters happen.</p>
						</div>
						<div class="col-sm-6 col-sm-pull-6 centerup">
							<a href="<?php print $url ?>/crisis-disaster/" class='challenge-popup'><img src="<?php print $themeLink ?>/images/crisis-infographic2.png"></a>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<h3>As the world’s largest provider of pumps and equipment that treats and transports water, <a href="http://www.xyleminc.com/" target="_blank" style="text-decoration: underline;">Xylem Inc.</a> is committed to addressing this critical need.</h3>
						</div>
					</div>
				</div>
			</div>
		</div>

<!-- End crisis-challenge.php -->