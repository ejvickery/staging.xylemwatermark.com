<!-- makeyourmark-trip.php -->
	<?php $themeLink = get_bloginfo('template_url');
  	    $url = site_url();
	?>
		<div id="section-take-trip">
  <?php

    //$pageSectionID = get_page_by_title( 'Make your mark trip section' );
    //$pageSection = get_post($pageSectionID->ID, ARRAY_A);
    //print $pageSection['post_content'];

  ?>

  <div class="row">
<div class="col-xs-12">
<h1>Take a <strong>TRIP</strong></h1>
</div>
</div>
<div class="row">
<div class="image-strip"></div>
</div>
<div class="container">
<div class="row">
<div class="col-xs-12">
<p>Annually, Xylem offers 10-day global volunteer trips for employees with our nonprofit partner Water For People. Employees travel into the field to monitor local water sources, survey local residents, and help assess and prepare the community for future water projects. For more details on current trips, see below.</p>
<p>Xylem Watermark also offers regional volunteer opportunities in the field throughout the year with Planet Water Foundation, China Women’s Development Foundation, and Fundación Avina. These trips do not require an application, and participants are managed by local Xylem offices.</p>
<p>If you have any questions, please email Michael Fields, Director of Corporate Citizenship, at <a href="mailto:michaelc.fields@xyleminc.com">michaelc.fields@xyleminc.com</a>.</p>
</div>
</div>

<div class="row">
<div class="col-md-5 col-md-offset-1 current-trip-popup">  <!-- NOTE: This open div is used when both buttons are displayed.
<div class="col-md-5 col-md-offset-3 current-trip-popup">-->
<a href="./make-your-mark/take-a-trip/" class="btn-browse"></a>
</div>

<div class="col-md-5">
<a href="./volunteer-form" class="btn-submit-app">Submit Your Application</a>
</div>

</div>

</div>


		</div>
        

<?php //print '<!-- End makeyourmark-trip.php - pageID: '.$pageSectionID->ID.' -->'; ?>


<!-- End makeyourmark-trip.php -->