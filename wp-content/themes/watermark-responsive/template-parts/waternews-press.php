<!-- waternews-press.php -->

	<?php $themeLink = get_bloginfo('template_url'); ?>

  		<div id="press-releases">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h1>Xylem <strong>PRESS RELEASES</strong></h1>
						<p>Here you will find current and archived coverage of Xylem in the news and press releases.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 press-box">
						<a href="http://www.xyleminc.com/en-us/news-and-events/press-release-archive/Pages/Xylem-showcases-new-innovative-water-technologies-at-IFAT-2014.aspx">
Xylem showcases new innovative water technologies at IFAT 2014 <span>(5/5/2014)</span></a>
						<a href="http://www.xyleminc.com/en-us/news-and-events/press-release-archive/Pages/Xylem-Inc.-reports-improved-first-quarter-earnings;-maintains-full-year-outlook-.aspx">Xylem Inc. reports improved first quarter earnings; maintains full-year outlook<span>(4/29/2014)</span></a>
						<a href="http://www.xyleminc.com/en-us/news-and-events/press-release-archive/Pages/Xylem-recognized-by-Business-Roundtable-for-sustainable-business-practices.aspx">Xylem recognized by Business Roundtable for sustainable business practices <span>(4/22/2014)</span></a>
					</div>
				</div>
				<div class="row">
					<a href="http://xyleminc.com" target="_blank" class="btn-more-news">More news on XylemInc.com</a>
				</div>
			</div>
		</div>


<!-- End waternews-press.php -->