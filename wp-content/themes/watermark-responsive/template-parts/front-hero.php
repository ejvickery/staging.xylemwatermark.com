<!-- front-hero.php -->

  <?php $themeLink = get_template_directory_uri(); ?>

  <?php

   // $pageSectionID = get_page_by_title( 'Home hero' );
   // $pageSection = get_post($pageSectionID->ID, ARRAY_A);
   // print $pageSection['post_content'];

  ?>

<div class="home-hero">

  <div id="cycle" class="cycle-slideshow"
    data-cycle-loader="wait"
    data-cycle-fx="fadeout"
    data-cycle-speed="700"
    data-cycle-timeout="7000"
    data-cycle-center-horz="true"
    data-cycle-slides="> div"
    >



		<div class="slide-content hero-slide-1">
			<div class="slide-text">
				<h1><strong>780 million people</strong> lack access to an improved water source.</h1>
				<h4>SOURCE: UNICEF/WHO</h4>
			</div>
		</div><!-- close page01-A-hero -->


		<div class="slide-content hero-slide-2">
			<div class="slide-text">
				<h1>More people on Earth have a mobile phone <strong>than a toilet.</strong></h1>
				<h4>SOURCE: UNICEF/WHO</h4>
			</div>
		</div><!-- close page01-B-hero -->

		<div class="slide-content hero-slide-3">
			<div class="slide-text">
				<h1>Since 2008, Xylem Watermark has been <strong>making a difference</strong></h1>
			</div>
		</div><!-- close page01-C-hero -->

		<div class="slide-content hero-slide-4">
			<div class="slide-text">
				<h1><strong>How do you</strong> make a difference?</h1>
			</div>
			<img src="<?php print $themeLink ?>/images/slideshow-imgs/slideshow-spacer.png" alt="XYLEM IS MAKING A DIFFERENCE" id="h-04" class="first" width="100%"/>
		</div><!-- close page01-D-hero -->

  </div><!-- close cycle -->

<!--<div class="blog-btn-temp">
		<a href="http://staging.xylemwatermark.com/make-your-mark#section-wtrmrk-community" class="btn"></a>
	</div>-->
    
<!--<div class="blog-btn-temp">
		<a href="/running-water/"><img src="<?php print $themeLink ?>/images/slideshow-imgs/Blog_button.png" width="416" height="84" alt="Learn more on Running Water"/></a>
	</div>-->
    
    <div class="blog-btn-temp">
		<a href="http://www.youtube.com/watch?v=5illnEKRy1g" title="Xylem Watermark Two Million and Counting" class="btn-vid"></a>
	</div>

	<!--div id="vid-btn-cont">  This is the piece to go back to after the banner "0" expires.
		<a href="http://www.youtube.com/watch?v=5illnEKRy1g" title="Xylem Watermark Two Million and Counting"><img src="<?php print $themeLink ?>/images/slideshow-imgs/Video-button.png" width="302" height="81" alt="Play the Video"/></a>
		<a href="#refuge-day" class="refugee-day-open-popup" title="Xylem Watermark Two Million and Counting"><img src="<?php print $themeLink ?>/images/WRD_button.png" class="WRD_button" width="475" height="84" alt="Play the Video"/></a>
	</div-->

</div><!-- close home-hero -->




<div id="refuge-day" class="partner-modal mfp-hide">
 <div class="refuge-day-image"><img src="<?php print $themeLink ?>/images/WRD_Modal_image-1.jpg" /></div>
 <div class="refuge-day-text"><p class="refugee-header">"When we left Damascus, my father told us
we would only be gone for a month. But 
after we were here for 6 months, I knew we
were not going to be going home soon. I 
was devastated. I thought I had lost my life,
my future."</p>

<p>Imagine having to pick up and leave your home, your country, and your life behind in the blink of an eye. That is exactly what Nour and more than 2.8 million Syrians have had to do to escape the civil war and violence that began more than three years ago in their country.</p>

<p>Nour, along with her parents, four siblings, grandmother, and cousins, fled Syria when their neighborhood was bombed. Their house was destroyed just days after they left. Today, Nour and her family are among the more than 100,000 Syrian refugees who have called the Zaatari camp in Jordan – the second largest refugee camp in the world – home for more than two years.</p>

<p>As part of Xylem Watermark, Xylem’s corporate citizenship and social investment program, Xylem, Mercy Corps and UNICEF are working together to help ensure that clean water is nearby, and that Nour’s future remains vibrant. The partners constructed deep-water wells to bring clean water to Nour, her family, and refugees throughout the Zaatari camp.</p>

<p><a href="http://www.mercycorps.org/photoessays/jordan-syria/life-refugee-spend-day-nour" target="_blank">Read more of Nour’s story from Mercy Corps</a></p>
</div>
<div class="reset"></div>
</div>
<!-- End front-hero.php -->

<?php //print '<!-- End front-hero.php - pageID: '.$pageSectionID->ID.' -->'; ?>
