<!-- runningwater-posts.php -->
<style>
.pagination-wrap {width:100%;position:relative;text-align:center;height:50px;display:inline-block;}

.thumbnail {border:none!important;}

.iframe {height:360px;}

.blog_container article {min-height:700px;padding-bottom:2%;}

.custom-pagination span.page-num
{display:block;}

.custom-pagination span, .custom-pagination a {
display: inline-block;
padding: 15px 29px 15px 23px;
width: 34px;
float: left;
margin: 3px;
font-size: 12px;
}

.post-featured-image, .flexslider {max-height:350px;}
.post-featured-video iframe {height:350px;}



@media (max-width: 1200px) and (min-width:1000px) { 
.post-featured-image, .flexslider {
max-height: 300px;
}	
.post-featured-video iframe {height:300px;}
	
}

@media (max-width:999px) { 
.post-featured-image, .flexslider {
max-height: 220px;
}	
.post-featured-video iframe {height:220px;}	
}

@media (max-width:767px) { 
.blog_container article {min-height: none; }

}



nav.custom-pagination {
display: inline-block;
margin-top: 0;
background: #fff!important;
padding-top: 5%;
margin-left: 5%;
}

.custom-pagination a {
  background-color: #fff;
  color: #0088b0;
  border: 1px solid #0088b0;
}
.custom-pagination a:hover {
  background-color: #0088b0;
  color: #fff;
  text-decoration:none;
    border: 1px solid #0088b0;
}
.custom-pagination span.page-num {
  margin-right: 10px;
  padding: 0;
}
.custom-pagination span.dots {
  padding: 10px 0 0 0;
  color: #b2b2b2;
}
.custom-pagination span.current {
  background-color: #0088b0;
  color: #fff;
    border: 1px solid #0088b0;
}
</style>


<?php $themeLink = get_bloginfo('template_url');
  	    $url = site_url();
	?>

          <section class="page_section" id="blog">

            <!-- section content -->
            <div class="content_section container">
            <div class="blog_title centerup"><h1>Featured <strong>POSTS</strong></h1></div>
            	<!--Blog-->
                <div class="blog_container">

                <?php 

  $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
  
  $custom_args = array(
      'post_type' => 'post',
	  'category_name' => 'news',
      'posts_per_page' => 8,
      'paged' => $paged,
	  'order' => 'DESC',
    );

  $custom_query = new WP_Query( $custom_args ); ?>
 
  <?php if ( $custom_query->have_posts() ) : ?>
  
   
    <!-- the loop -->
    <?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
    
      <article class=" col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 loop">
 
        
        

	 
	 <?php if ( get_post_meta(get_the_ID(), 'video', true)) { ?>
     
     
     <div class="post-featured-video">
        <?php echo get_post_meta( get_the_ID(), 'video', true ); ?>
       </div> 
       
       
       <?php
} elseif ( get_post_meta(get_the_ID(), 'gallery', true)) {
    ?>
            
            <?php 

$images = get_field('gallery');

if( $images ): ?>
     <a href="<?php the_permalink(); ?>"><div id="slider" class="flexslider">
        <ul class="slides">
            <?php foreach( $images as $image ): ?>
                <li>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <p><?php echo $image['caption']; ?></p>
                </li>
            <?php endforeach; ?>
        </ul>
    </div></a>
<?php endif; ?>
            

       <?php
} else {
    ?>
    
            
      <div class="post-featured-image"> 
              <a href="<?php the_permalink(); ?>"><?php get_the_image( array('size' => 'medium-16x9', 'thumbnail_id_save' => true, 'link_to_post' => false, width => '100%', 'image_scan' => true, 'order_of_image' => 1)); ?></a>
        </div>
    

            
        <?php } ?>



	 
        <h3 id="post-<?php the_ID(); ?>">
        
        
        
        
        

       <a href="<?php the_permalink(); ?>">
	   
	   
       <!--beginning of icon next to title--
	   
	   
	   
       
        <?php if ( get_post_meta(get_the_ID(), 'video', true)) { ?>
     
     
     <img src="http://staging.xylemwatermark.com/wp-content/themes/watermark-responsive/images/sm-video-icon.png" />
       
       
       <?php
} elseif ( get_post_meta(get_the_ID(), 'gallery', true)) {
    ?>
            
              <img src="http://staging.xylemwatermark.com/wp-content/themes/watermark-responsive/images/sm-gallery-icon.png" />

            

       <?php
} else {
    ?>
    
            
      <div class="post-featured-image"> 
             <img src="http://staging.xylemwatermark.com/wp-content/themes/watermark-responsive/images/sm-image-icon.png" />


            
        <?php } ?>

	   
	   
	   
       --end of icon next to title-->
       
       
	   
	   <?php the_title(); ?></a></h3>
        <div class="content">
          <?php the_excerpt(); ?>
        </div>
        <?php echo get_the_date(); ?> / <?php the_author_posts_link(); ?>
            <br /><div>Categories: <?php echo the_category(', '); ?> / Tags: <?php echo the_tags(' '); ?></div>
        <br/>
        <div class="post_socials">
                <?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { ADDTOANY_SHARE_SAVE_KIT(); } ?>
            </div>
        
      </article>
    <?php endwhile; ?>
    <!-- end of the loop -->
 
    <!-- pagination here -->
    <?php
      if (function_exists(custom_pagination)) {
        custom_pagination($custom_query->max_num_pages,"",$paged);
      }
    ?>
 
  <?php wp_reset_postdata(); ?>
 
  <?php else:  ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
  <?php endif; ?>

                    <div class="clear"></div>
                </div>
                <!--pagerblock -->

     </section>
            <!--//Blog-->

<!-- End runningwater-posts.php -->