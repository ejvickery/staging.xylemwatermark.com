<!-- crisis-facts.php -->

	<?php $themeLink = get_bloginfo('template_url'); ?>

  		<div id="crisis-section2" class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1>Know the <strong>FACTS</strong></h1>
				</div>
			</div>

<div class="row">
<div class="col-sm-12">

<div id="challenge-facts">
          
  <div class="item"><a href="http://www.unicef.org/media/files/JMPreport2012.pdf"><img src="<?php print $themeLink ?>/images/fact-box-01.png" alt="fact-box-01"></a></div>
  <div class="item"><a href="http://www.unicef.org/media/files/JMPreport2012.pdf"><img src="<?php print $themeLink ?>/images/fact-box-02.png" alt="fact-box-02"></a></div>
  <div class="item"><a href="http://www.wssinfo.org/fileadmin/user_upload/resources/1278061137-JMP_report_2010_en.pdf"><img src="<?php print $themeLink ?>/images/fact-box-03.png" alt="fact-box-03"></a></div>
  <div class="item"><a href="http://www.wssinfo.org/fileadmin/user_upload/resources/1278061137-JMP_report_2010_en.pdf"><img src="<?php print $themeLink ?>/images/fact-box-04.png" alt="fact-box-04"></a></div>
  <div class="item"><a href="http://www.unwater.org/downloads/181793E.pdf"><img src="<?php print $themeLink ?>/images/fact-box-05.png" alt="fact-box-05"></a></div>
  <div class="item"><a href="http://www.unwater.org/downloads/181793E.pdf"><img src="<?php print $themeLink ?>/images/fact-box-06.png" alt="fact-box-06"></a></div>
  <div class="item"><a href="http://hdr.undp.org/en/media/HDR06-complete.pdf"><img src="<?php print $themeLink ?>/images/fact-box-07.png" alt="fact-box-07"></a></div>
  <div class="item"><a href="http://whqlibdoc.who.int/publications/2009/9789241598415_eng.pdf"><img src="<?php print $themeLink ?>/images/fact-box-08.png" alt="fact-box-08"></a></div>
 
</div>

				</div>
			</div>

<!-- /#crisis-section2 --></div>
<!-- End crisis-facts.php -->