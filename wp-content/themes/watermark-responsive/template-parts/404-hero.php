<!-- 404-hero.php -->

  <div id="content-page-content">

    <div id="page-404-text">
      <h2>404 Error</h2>
    </div>

  <div id="404" class="content-text">

We're sorry, the page you are looking for could not be found (404 error).

  </div>

  </div>

<!-- 404-hero end -->