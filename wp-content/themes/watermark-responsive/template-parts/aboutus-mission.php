<!-- aboutus-mission.php -->

	<?php $themeLink = get_bloginfo('template_url');
	  	    $url = site_url();
	?>


  		<div id="about-section2">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1><strong>OUR MISSION</strong> is to provide and protect safe water resources for communities in need around the world.</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 centerup sideborders mission-box">
						<a href="<?php print $url ?>/crisis-scarcity/" class='challenge-popup'><img src="<?php print $themeLink; ?>/images/mission-box-1.png"></a>
					</div>
					<div class="col-lg-4 centerup mission-box">
						<a href="<?php print $url ?>/crisis-scarcity/" class='challenge-popup'><img src="<?php print $themeLink; ?>/images/mission-box-2.png"></a>
					</div>
					<div class="col-lg-4 centerup sideborders mission-box">
						<a href="<?php print $url ?>/crisis-disaster/" class='challenge-popup'><img src="<?php print $themeLink; ?>/images/mission-box-3.png"></a>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p>As a leading global technology company, Xylem is committed to finding solutions to the world's most critical water challenges. Put simply, Xylem is working to <em>solve water.</em></p>
						<a href="http://www.youtube.com/watch?v=5illnEKRy1g" title="Xylem Watermark Two Million and Counting" class="btn-vid"></a>
					</div>
				</div>
			</div>
		</div>

<!-- End aboutus-mission.php -->