<!-- waternews-press.php -->

	<?php $themeLink = get_bloginfo('template_url'); ?>

		<div id="section-news-feature-tags">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h4>News Feature Tags</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 centerup">
          <?php
            $tags = get_tags();
            $html = '';
            foreach ( $tags as $tag ) {
          	  $tag_link = get_tag_link( $tag->term_id );

              $html .= "<a href='{$tag_link}' title='{$tag->name} Tag'>";
              $html .= "{$tag->name}</a>";
              }
              $html .= '';
              echo $html;
          ?>
					</div>
				</div>
			</div>
		</div>

<!-- End waternews-press.php -->