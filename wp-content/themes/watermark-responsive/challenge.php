<?php
/**
 * The template for displaying Challenge page.
 *
 * Template Name: challenge
 *
 * @package WordPress
 * @subpackage Xylem Watermark
 * @since Xylem Watermark 2.0
 */
get_header();?>
<?php get_template_part('template-parts/page', 'nav'); ?>
<div class="outerlimits">
<?php get_template_part('template-parts/challenge', 'hero'); ?>
<?php get_template_part('template-parts/challenge', 'facts'); ?>
<?php get_template_part('template-parts/challenge', 'challenge'); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post();?>

	<?php the_content(); ?>

	<?php endwhile; endif; ?>

<?php //get_sidebar(); ?>
<?php get_template_part('template-parts/challenge', 'waternews'); ?>
<?php get_template_part('template-parts/page', 'contact'); ?>
<?php get_footer(); ?>