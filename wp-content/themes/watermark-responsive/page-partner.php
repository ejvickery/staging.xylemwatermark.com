<?php
/*
Template Name: Xylem Partner
*/
//get_header();?>

<?php //get_template_part('template-parts/page', 'nav'); ?>

<?php //get_template_part('template-parts/page', 'hero'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<?php the_content(); ?>

<?php endwhile; endif; ?>

<?php //get_sidebar(); ?>
<?php //get_template_part('template-parts/page', 'makingadifference'); ?>
<?php //get_template_part('template-parts/page', 'contact'); ?>
<?php //get_footer(); ?>
