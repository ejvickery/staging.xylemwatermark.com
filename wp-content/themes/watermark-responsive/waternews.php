<?php
/**
 * The template for displaying Water news posts page.
 *
 * Template Name: waternews
 *
 * @package WordPress
 * @subpackage Xylem Watermark
 * @since Xylem Watermark 2.0
 */
get_header();?>
<?php get_template_part('template-parts/page', 'nav'); ?>
<div class="outerlimits">
<?php get_template_part('template-parts/waternews', 'hero'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

 <?php get_template_part('template-parts/waternews', 'posts'); ?>

<?php endwhile; else: ?>

 <p>Sorry, no posts matched your criteria.</p>

<?php endif; ?>

<?php get_template_part('template-parts/waternews', 'tags'); ?>
<?php get_template_part('template-parts/waternews', 'press'); ?>
<?php get_template_part('template-parts/page', 'contact'); ?>
<?php get_footer(); ?>